package org.aichallenge.ants;

public class ResultadoAccion {
	boolean bAcabado;
	int iNuevaPrioridad;
	/**
	 * @param objBAcabado
	 * @param objNuevaPrioridad
	 */
	public ResultadoAccion(boolean bAcabado, int iNuevaPrioridad) {
		this.bAcabado = bAcabado;
		this.iNuevaPrioridad = iNuevaPrioridad;
	}
	public boolean isAcabado() {
		return bAcabado;
	}
	public int getNuevaPrioridad() {
		return iNuevaPrioridad;
	}
	@Override
	public String toString() {
		return String.format("ResultadoAccion [bAcabado=%s, iNuevaPrioridad=%s]", bAcabado, iNuevaPrioridad);
	}
	
}
