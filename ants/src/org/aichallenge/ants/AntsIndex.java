package org.aichallenge.ants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import kdtree.KdTree;



public class AntsIndex {
	private KdTree<Tile> kdTree=new KdTree.SqrEuclid<Tile>(2,null);
	private Ants ants;
	/**
	 * @param objAnts
	 */
	public AntsIndex(Ants objAnts) {
		ants = objAnts;
	}
	
	
	public void addTile(Tile tile){
		double[] location=new double[2];
		location[0]=tile.getRow();
		location[1]=tile.getCol();
		this.kdTree.addPoint(location, tile);
		EnumSet<Aim> objLocationTile=EnumSet.noneOf(Aim.class);
		int row=tile.getRow();
		if (row<ants.getRows()-row){
			objLocationTile.add(Aim.NORTH);
		}else{
			objLocationTile.add(Aim.SOUTH);
		}
		int col=tile.getCol();
		if (col<ants.getCols()-col){
			objLocationTile.add(Aim.WEST);
		}else{
			objLocationTile.add(Aim.EAST);
		}
		
		
		// NORTH-WEST tile
		if (objLocationTile.contains(Aim.SOUTH)&& objLocationTile.contains(Aim.EAST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()-ants.getRows();
			virtualLocation[1]=tile.getCol()-ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		
		// NORTH tile
		if (objLocationTile.contains(Aim.SOUTH)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()-ants.getRows();
			virtualLocation[1]=tile.getCol();
			this.kdTree.addPoint(virtualLocation, tile);	
		}

		// NORTH-EAST tile
		if (objLocationTile.contains(Aim.SOUTH)&& objLocationTile.contains(Aim.WEST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()-ants.getRows();
			virtualLocation[1]=tile.getCol()+ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		
		// EAST tile
		if (objLocationTile.contains(Aim.WEST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow();
			virtualLocation[1]=tile.getCol()+ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		
		// SOUTH-EAST tile
		if (objLocationTile.contains(Aim.NORTH)&& objLocationTile.contains(Aim.WEST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()+ants.getRows();
			virtualLocation[1]=tile.getCol()+ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		// SOUTH tile
		if (objLocationTile.contains(Aim.NORTH)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()+ants.getRows();
			virtualLocation[1]=tile.getCol();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		// SOUTH-WEST tile
		if (objLocationTile.contains(Aim.NORTH)&& objLocationTile.contains(Aim.EAST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow()+ants.getRows();
			virtualLocation[1]=tile.getCol()-ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}
		// WEST tile
		if (objLocationTile.contains(Aim.EAST)){
			double[] virtualLocation=new double[2];
			virtualLocation[0]=tile.getRow();
			virtualLocation[1]=tile.getCol()-ants.getCols();
			this.kdTree.addPoint(virtualLocation, tile);	
		}

		
		
	}
	
	public List<Tile> queryTiles(Tile center,int iLimit){
		Set<Tile> objResult=new LinkedHashSet<Tile>();
		double[] location=new double[2];
		location[0]=center.getRow();
		location[1]=center.getCol();
		List<KdTree.Entry<Tile>> objPartialResult=this.kdTree.nearestNeighbor(location, iLimit, true);
		for (KdTree.Entry<Tile> objEntry : objPartialResult) {
			objResult.add(objEntry.value);
		}
		List <Tile> objCompleteResult=new ArrayList<Tile>(objResult);
		Collections.sort(objCompleteResult,new SqrDistanceComparator(center, ants));
		return objCompleteResult;

	}
	
	
	
	public static void main(String[] args) {
		Ants ants=new Ants(0,0,60,90,900,77,5,1,42);
		AntsIndex index=new AntsIndex(ants);
		
		
				Tile[] aTest=new Tile[]{
						new Tile(0,0),
						new Tile(1,0),
						new Tile(2,0),
						new Tile(3,0),
						new Tile(4,0),
						new Tile(5,0),
						new Tile(6,1),
						new Tile(6,2),
						new Tile(6,3),
						new Tile(5,3),
						new Tile(4,2),
						new Tile(3,2),
						new Tile(2,2),
						new Tile(1,2)
				};

		for (Tile objTile : aTest) {
			index.addTile(objTile);
		}
		
		Tile[] aTest2=new Tile[]{
				new Tile(59,89),
				new Tile(6,0),
				new Tile(0,0),
				new Tile (7,4),
				new Tile(5,2)
				
		};
		
		for (Tile objTile : aTest2) {
			List<Tile> objResult=index.queryTiles(objTile, 5);
			StringBuilder sb=new StringBuilder();
			sb.append("Tile:");
			sb.append(objTile.toString());
			sb.append(":");
			sb.append(objResult);
			System.out.println(sb.toString());
		}
	
		long lInicio=System.currentTimeMillis();
		List<Tile> objResult=null;
		for (int i=0;i<1000;i++){
			 objResult=index.queryTiles(new Tile(5,5), 5);
		}
		long lFin=System.currentTimeMillis();
		System.out.println(objResult);
		System.out.println((lFin-lInicio)+" ms");
	}
}
class ManhattanDistanceComparator implements Comparator<Tile>{
	private Tile objCenter;
	private Ants ants;
	
	/**
	 * @param objCenter
	 */
	public ManhattanDistanceComparator(Tile objCenter,Ants ants) {
		this.objCenter = objCenter;
		this.ants=ants;
	}


	@Override
	public int compare(Tile objO1, Tile objO2) {
		int iDistance1=ants.getManhattanDistance(objCenter, objO1);
		int iDistance2=ants.getManhattanDistance(objCenter, objO2);
		if (iDistance1<iDistance2){
			return -1;
		}else if (iDistance1>iDistance2){
			return 1;
		}else{
			return 0;
		}
		
	}
	
}

class SqrDistanceComparator implements Comparator<Tile>{
	private Tile objCenter;
	private Ants ants;
	
	/**
	 * @param objCenter
	 */
	public SqrDistanceComparator(Tile objCenter,Ants ants) {
		this.objCenter = objCenter;
		this.ants=ants;
	}


	@Override
	public int compare(Tile objO1, Tile objO2) {
		int iDistance1=ants.getDistance(objCenter, objO1);
		int iDistance2=ants.getDistance(objCenter, objO2);
		if (iDistance1<iDistance2){
			return -1;
		}else if (iDistance1>iDistance2){
			return 1;
		}else{
			return 0;
		}
		
	}
	
}