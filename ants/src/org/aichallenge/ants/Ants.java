package org.aichallenge.ants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import es.cccf.ants.battleplan.BattlePlan;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.BattlePlanGeneral;
import es.cccf.ants.spacepartitioning.AntsExploration;
import es.cccf.ants.spacepartitioning.AntsSpacePartitioning;
import es.cccf.ants.spacepartitioning.Path;
import es.cccf.ants.spacepartitioning.Room;
import es.cccf.ants.spacepartitioning.RoomInfoPartition;
import es.cccf.ants.spacepartitioning.RoomInstanceNode;
import es.cccf.ants.spacepartitioning.TileAimKey;


/**
 * Holds all game data and current game state.
 */
public class Ants{
	public static final int PLAYER_UNKNOWN=-1;
	
    /** Maximum map size. */
    public static final int MAX_MAP_SIZE = 256;
    
    private final int loadTime;
    
    private final int turnTime;
    
    private final int rows;
    
    private final int cols;
    
    private final int turns;
    
    private final int viewRadius2;
    
    private final int attackRadius2;
    
    private final int spawnRadius2;
    
    private long turnStartTime;
    
    private final TileInformation map[][];
    
    private final Set<Tile> myAnts = new HashSet<Tile>();
    
    private final Set<Tile> enemyAnts = new HashSet<Tile>();
    
    private final Set<Tile> myHills = new HashSet<Tile>();
    
    private final Set<Tile> enemyHills = new HashSet<Tile>();
    
    private final Set<Tile> foodTiles = new HashSet<Tile>();
    
    private final Set<Order> orders = new HashSet<Order>();
    
    private final Set<Tile> visibleTiles=new HashSet<Tile>();
    
    private final Set<Tile> originOrders=new HashSet<Tile>();
    
    private final Set<TileAimKey> originOrdersAim=new HashSet<TileAimKey>();
    
    private final Set<Tile> destOrders=new HashSet<Tile>();
    
    private TileTemplate attackTemplate;
    
    private TileTemplate viewTemplate;
    
    private TileTemplate spawnTemplate;
    
    private int iCurrentTurn;
    
    private AntsIndex myAntsIndex;
    
    private AntsSpacePartitioning objPathFinder;
    
    private Random objRandom;
    
    private BattlePlanGeneral objGeneral;
    
    private AntsExploration objAntsExploration;
    
    /**
     * Creates new {@link Ants} object.
     * 
     * @param loadTime timeout for initializing and setting up the bot on turn 0
     * @param turnTime timeout for a single game turn, starting with turn 1
     * @param rows game map height
     * @param cols game map width
     * @param turns maximum number of turns the game will be played
     * @param viewRadius2 squared view radius of each ant
     * @param attackRadius2 squared attack radius of each ant
     * @param spawnRadius2 squared spawn radius of each ant
     * @param lPlayerSeed 
     */
    public Ants(int loadTime, int turnTime, int rows, int cols, int turns, int viewRadius2,
            int attackRadius2, int spawnRadius2, long lPlayerSeed) {
        this.loadTime = loadTime;
        this.turnTime = turnTime;
        this.rows = rows;
        this.cols = cols;
        this.turns = turns;
        this.viewRadius2 = viewRadius2;
        this.attackRadius2 = attackRadius2;
        this.spawnRadius2 = spawnRadius2;
        this.objGeneral=new BattlePlanGeneral(this);
        map = new TileInformation[rows][cols];
        for (int row=0;row<this.rows;row++){
        	for (int col=0;col<this.cols;col++){
        		this.map[row][col]=new TileInformation(new Tile(row,col));
        	}
        }
        iCurrentTurn=0;
        attackTemplate=new TileTemplate(attackRadius2);
        viewTemplate=new TileTemplate(viewRadius2);
        spawnTemplate=new TileTemplate(spawnRadius2);
        this.objPathFinder=new AntsSpacePartitioning(this);
        objRandom=new Random(lPlayerSeed);
        objAntsExploration=new AntsExploration(this);
    }
    
    
    public boolean isTileDestOrder(Tile objTileDest){
    	return this.destOrders.contains(objTileDest);
    }
    
    /**
     * Returns timeout for initializing and setting up the bot on turn 0.
     * 
     * @return timeout for initializing and setting up the bot on turn 0
     */
    public int getLoadTime() {
        return loadTime;
    }
    
    /**
     * Returns timeout for a single game turn, starting with turn 1.
     * 
     * @return timeout for a single game turn, starting with turn 1
     */
    public int getTurnTime() {
        return turnTime;
    }
    
    /**
     * Returns game map height.
     * 
     * @return game map height
     */
    public int getRows() {
        return rows;
    }
    
    /**
     * Returns game map width.
     * 
     * @return game map width
     */
    public int getCols() {
        return cols;
    }
    
    /**
     * Returns maximum number of turns the game will be played.
     * 
     * @return maximum number of turns the game will be played
     */
    public int getTurns() {
        return turns;
    }
    
    /**
     * Returns squared view radius of each ant.
     * 
     * @return squared view radius of each ant
     */
    public int getViewRadius2() {
        return viewRadius2;
    }
    
    /**
     * Returns squared attack radius of each ant.
     * 
     * @return squared attack radius of each ant
     */
    public int getAttackRadius2() {
        return attackRadius2;
    }
    
    /**
     * Returns squared spawn radius of each ant.
     * 
     * @return squared spawn radius of each ant
     */
    public int getSpawnRadius2() {
        return spawnRadius2;
    }
    
    /**
     * Sets turn start time.
     * 
     * @param turnStartTime turn start time
     */
    public void setTurnStartTime(long turnStartTime) {
        this.turnStartTime = turnStartTime;
        iCurrentTurn++;
    }
    
    /**
     * Returns how much time the bot has still has to take its turn before timing out.
     * 
     * @return how much time the bot has still has to take its turn before timing out
     */
    public int getTimeRemaining() {
        return turnTime - (int)(System.currentTimeMillis() - turnStartTime);
    }
    
    /**
     * Returns ilk at the specified location.
     * 
     * @param tile location on the game map
     * 
     * @return ilk at the <cod>tile</code>
     */
    public Ilk getIlk(Tile tile) {
        return map[tile.getRow()][tile.getCol()].getType();
    }
    
    /**
     * Sets ilk at the specified location.
     * 
     * @param tile location on the game map
     * @param ilk ilk to be set at <code>tile</code>
     */
    public void setIlk(Tile tile, Ilk ilk) {
        map[tile.getRow()][tile.getCol()].setType(ilk);
    }
    
    /**
     * Returns ilk at the location in the specified direction from the specified location.
     * 
     * @param tile location on the game map
     * @param direction direction to look up
     * 
     * @return ilk at the location in <code>direction</code> from <cod>tile</code>
     */
    public Ilk getIlk(Tile tile, Aim direction) {
        Tile newTile = getTile(tile, direction);
        return map[newTile.getRow()][newTile.getCol()].getType();
    }
    
    /**
     * Returns location in the specified direction from the specified location.
     * 
     * @param tile location on the game map
     * @param direction direction to look up
     * 
     * @return location in <code>direction</code> from <cod>tile</code>
     */
    public Tile getTile(Tile tile, Aim direction) {
        int row = (tile.getRow() + direction.getRowDelta()) % rows;
        if (row < 0) {
            row += rows;
        }
        int col = (tile.getCol() + direction.getColDelta()) % cols;
        if (col < 0) {
            col += cols;
        }
        return new Tile(row, col);
    }
    
    /**
     * Returns canonical location
     * @param tile location on the game map
     * @return
     */
    public Tile getCanonicalTile(Tile tile){
    	int row=tile.getRow();
    	int col=tile.getCol();
    	boolean cambiado=false;
    	if (row<0 || row>=this.rows){
    		cambiado=true;
    		row=row%rows;
            if (row < 0) {
                row += rows;
            }
    	}
    	
    	if (col<0 || col>=this.cols){
    		cambiado=true;
    		col=col%cols;
            if (col < 0) {
                col += cols;
            }
    	}

    	if (cambiado){
    		return new Tile(row,col);
    	}else{
    		return tile;
    	}
    	
    }
    
    
    /**
     * Returns a set containing all my ants locations.
     * 
     * @return a set containing all my ants locations
     */
    public Set<Tile> getMyAnts() {
        return myAnts;
    }
    
    /**
     * Returns a set containing all enemy ants locations.
     * 
     * @return a set containing all enemy ants locations
     */
    public Set<Tile> getEnemyAnts() {
        return enemyAnts;
    }
    
    /**
     * Returns a set containing all my hills locations.
     * 
     * @return a set containing all my hills locations
     */
    public Set<Tile> getMyHills() {
        return myHills;
    }
    
    /**
     * Returns a set containing all enemy hills locations.
     * 
     * @return a set containing all enemy hills locations
     */
    public Set<Tile> getEnemyHills() {
        return enemyHills;
    }
    
    /**
     * Returns a set containing all food locations.
     * 
     * @return a set containing all food locations
     */
    public Set<Tile> getFoodTiles() {
        return foodTiles;
    }
    
    /**
     * Returns all orders sent so far.
     * 
     * @return all orders sent so far
     */
    public Set<Order> getOrders() {
        return orders;
    }
    
    /**
     * Calculates distance between two locations on the game map.
     * 
     * @param t1 one location on the game map
     * @param t2 another location on the game map
     * 
     * @return distance between <code>t1</code> and <code>t2</code>
     */
    public int getDistance(Tile t1, Tile t2) {
        int rowDelta = Math.abs(t1.getRow() - t2.getRow());
        int colDelta = Math.abs(t1.getCol() - t2.getCol());
        rowDelta = Math.min(rowDelta, rows - rowDelta);
        colDelta = Math.min(colDelta, cols - colDelta);
        return rowDelta * rowDelta + colDelta * colDelta;
    }
    
    /**
     * Calculates distance between two locations on the game map.
     * 
     * @param t1 one location on the game map
     * @param t2 another location on the game map
     * 
     * @return distance between <code>t1</code> and <code>t2</code>
     */
    public int getManhattanDistance(Tile t1, Tile t2) {
        int rowDelta = Math.abs(t1.getRow() - t2.getRow());
        int colDelta = Math.abs(t1.getCol() - t2.getCol());
        rowDelta = Math.min(rowDelta, rows - rowDelta);
        colDelta = Math.min(colDelta, cols - colDelta);
        return rowDelta  + colDelta ;
    }

    
    /**
     * Returns one or two orthogonal directions from one location to the another.
     * 
     * @param t1 one location on the game map
     * @param t2 another location on the game map
     * 
     * @return orthogonal directions from <code>t1</code> to <code>t2</code>
     */
    public List<Aim> getDirections(Tile t1, Tile t2) {
        List<Aim> directions = new ArrayList<Aim>();
        if (t1.getRow() < t2.getRow()) {
            if (t2.getRow() - t1.getRow() >= rows / 2) {
                directions.add(Aim.NORTH);
            } else {
                directions.add(Aim.SOUTH);
            }
        } else if (t1.getRow() > t2.getRow()) {
            if (t1.getRow() - t2.getRow() >= rows / 2) {
                directions.add(Aim.SOUTH);
            } else {
                directions.add(Aim.NORTH);
            }
        }
        if (t1.getCol() < t2.getCol()) {
            if (t2.getCol() - t1.getCol() >= cols / 2) {
                directions.add(Aim.WEST);
            } else {
                directions.add(Aim.EAST);
            }
        } else if (t1.getCol() > t2.getCol()) {
            if (t1.getCol() - t2.getCol() >= cols / 2) {
                directions.add(Aim.EAST);
            } else {
                directions.add(Aim.WEST);
            }
        }
        return directions;
    }
    
    /**
     * Clears game state information about my ants locations.
     */
    public void clearMyAnts() {
        myAnts.clear();
        this.myAntsIndex=new AntsIndex(this);
    }
    
    public void clearOrders(){
    	this.orders.clear();
    	this.originOrders.clear();
    	this.originOrdersAim.clear();
    	this.destOrders.clear();
    }
    
    /**
     * Clears game state information about enemy ants locations.
     */
    public void clearEnemyAnts() {
        enemyAnts.clear();
    }
    
    /**
     * Clears game state information about my hills locations.
     */
    public void clearMyHills() {
        myHills.clear();
    }
    
    /**
     * Clears game state information about enemy hills locations.
     */
    public void clearEnemyHills() {
        enemyHills.clear();
    }

    /**
     * Clears game map state information
     */
    public void clearMap() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (map[row][col].getType() != Ilk.WATER &&
                	map[row][col].getType() != Ilk.FOOD) {
                    map[row][col].setType(Ilk.LAND);
                }
                map[row][col].setEnemyAdjacent(false);
            }
        }
    }
    
    /**
     * Updates game state information about new ants and food locations.
     * 
     * @param ilk ilk to be updated
     * @param tile location on the game map to be updated
     * @param iPlayer solo si es relevante
     */
    public void update(Ilk ilk, Tile tile,int iPlayer) {
    	if (tile.getRow()==105 && tile.getCol()==69){
    		System.err.println("EST");
    	}
    	TileInformation objTileInformation=getTileInformation(tile);
    	Ilk oldType=objTileInformation.getType();
    	
    	

        switch (ilk) {
        	case WATER:
        		objPathFinder.addOccupiedBlock(tile);
        		objTileInformation.setType(ilk);
        		objAntsExploration.addWaterTile(tile);
        		break;
            case FOOD:
                foodTiles.add(tile);
                objTileInformation.setLastTurnSeen(iCurrentTurn);
                objTileInformation.setType(ilk);
            break;
            case MY_ANT:
                myAnts.add(tile);
                map[tile.getRow()][tile.getCol()].setPlayerEnemyAnt(iPlayer);
                map[tile.getRow()][tile.getCol()].setLastTurnPassedMyAnt(this.iCurrentTurn);
                Set<Tile> currentVisibleTiles=viewTemplate.getTilesInRange(tile, this);
                this.myAntsIndex.addTile(tile);
                this.visibleTiles.addAll(currentVisibleTiles);
                if (oldType==Ilk.FOOD){
                	this.foodTiles.remove(tile);
                }
                objTileInformation.setType(ilk);
                objAntsExploration.addAnt(tile);
                
                
            break;
            case ENEMY_ANT:
                enemyAnts.add(tile);
                map[tile.getRow()][tile.getCol()].setPlayerEnemyAnt(iPlayer);
                if (oldType==Ilk.FOOD){
                	this.foodTiles.remove(tile);
                }
                for (Aim aim : Aim.values()) {
					Tile objTileAdjacent=this.getTile(tile, aim);
					map[objTileAdjacent.getRow()][objTileAdjacent.getCol()].setEnemyAdjacent(true);
				}
                objTileInformation.setType(ilk);
               

            break;
        }
    }
    
    
    public TileAimKey getTileAimKey(Tile objTile,Aim aim){
    	Tile objTileDest;
    	if (aim!=null){
    		objTileDest=getTile(objTile, aim);
    	}else{
    		objTileDest=objTile;
    	}
    	return new TileAimKey(objTile, aim, objTileDest);
    }
    
    public void updateVisibleTiles(){
    	for (Tile objTile : visibleTiles) {
			TileInformation objTileInformation=getTileInformation(objTile);
			if (objTileInformation.isHill()){
//				if (iCurrentTurn==30){
//					System.err.println("TEST");
//				}
				if (objTileInformation.getLastTurnSeen()!=this.iCurrentTurn){
					objTileInformation.setHill(false);
					if (objTileInformation.getPlayerHill()==0){
						this.myHills.remove(objTile);
					}else{
						this.enemyHills.remove(objTile);
					}
					objTileInformation.setPlayerHill(-1);
				}
			}
			
			if (objTileInformation.getType()==Ilk.FOOD){
				if (objTileInformation.getLastTurnSeen()!=this.iCurrentTurn){
					objTileInformation.setType(Ilk.LAND);
					this.foodTiles.remove(objTile);
				}
			}
			
			objTileInformation.setLastTurnSeen(iCurrentTurn);
		}
    }
    
    
    public TileInformation getTileInformation(Tile objTile){
    	return map[objTile.getRow()][objTile.getCol()];
    }
    
    /**
     * Updates game state information about hills locations.
     *
     * @param owner owner of hill
     * @param tile location on the game map to be updated
     */
    public void updateHills(int owner, Tile tile) {
 
    	
        if (owner > 0)
            enemyHills.add(tile);
        else
            myHills.add(tile);
        
        TileInformation objTileInformation=getTileInformation(tile);
        objTileInformation.setHill(true);
        objTileInformation.setPlayerHill(owner);
        objTileInformation.setLastTurnSeen(iCurrentTurn);
   }
    
    /**
     * Issues an order by sending it to the system output.
     * 
     * @param myAnt map tile with my ant
     * @param direction direction in which to move my ant
     */
    private void issueOrder(Tile myAnt, Aim direction) {
    	if (this.iCurrentTurn==1&& myAnt.getRow()==25&&myAnt.getCol()==101){
    		System.err.println("TEST");
    	}
    	if (this.iCurrentTurn==114&& myAnt.getRow()==3&&myAnt.getCol()==16){
    		System.err.println("TEST");
    	}
    	TileAimKey objTileAimKey=this.getTileAimKey(myAnt, direction);
   		this.originOrders.add(myAnt);
   		this.originOrdersAim.add(objTileAimKey);
   		this.destOrders.add(objTileAimKey.getTileDest());
   		if (direction!=null){
       		Order order = new Order(myAnt, direction);
       		orders.add(order);
   			System.out.println(order);
   		}
    	
    }
    
    
    public ResultMove issueMove(Tile objTileOrigin,Tile objTileDest,BattlePlanComparator objComparator){
    	return issueMove(objTileOrigin, this.getDirections(objTileOrigin, objTileDest),  objComparator);
    }
    
    public ResultMove issueMove(Tile objTileOrigin,List<Aim> objListOfAim,  BattlePlanComparator objComparator){
    	return issueMove(objTileOrigin, objListOfAim,false, objComparator);
    }
    
    public ResultMove issueMove(Tile objTileOrigin,List<Aim> objListOfAim,boolean bTryNullMove, BattlePlanComparator objComparator){
    	if (this.iCurrentTurn==1&& objTileOrigin.getRow()==82&&objTileOrigin.getCol()==102 ){
    		System.err.println("TEST");
    	}
    	if (this.iCurrentTurn==104&& objTileOrigin.getRow()==1&&objTileOrigin.getCol()==24){
    		System.err.println("TEST");
    	}

    	if (this.iCurrentTurn==79&& objTileOrigin.getRow()==60&&objTileOrigin.getCol()==59){
    		System.err.println("TEST");
    	}

    	List<TileAimKey> objFreeMoves=new ArrayList<TileAimKey>();
    	List<TileAimKey> objPushMoves=new ArrayList<TileAimKey>();
    	List<TileAimKey> objBattlePlanMoves=new ArrayList<TileAimKey>();
    	ResultMove objResultMove=new ResultMove();
    	if (objListOfAim!=null){
	    	for (Aim aim : objListOfAim){
				TileAimKey objTileAimKey=this.getTileAimKey(objTileOrigin, aim);
				if (isCurrentlyPassableStatic(objTileAimKey.getTileDest(), objResultMove)){
					if (objGeneral.isAffectedTile(objTileAimKey.getTileDest())){
						objBattlePlanMoves.add(objTileAimKey);
					}else if (!getTileInformation(objTileAimKey.getTileDest()).isPassable()){
						objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_WATER);
						
					}else if (this.getTileInformation(objTileAimKey.getTileDest()).getType()==Ilk.MY_ANT && !isAntIssuedOrder(objTileAimKey.getTileDest())){
						objPushMoves.add(objTileAimKey);
					}else if (this.isCurrentlyPassable(objTileAimKey.getTileDest(),objResultMove)){
						objFreeMoves.add(objTileAimKey);
					}
		    	}
	    	}
    	}
    	if (bTryNullMove){
			TileAimKey objTileAimKey=this.getTileAimKey(objTileOrigin, null);
			if (objGeneral.isAffectedTile(objTileAimKey.getTileDest())){
				objBattlePlanMoves.add(objTileAimKey);
			}else{
				objFreeMoves.add(objTileAimKey);
			}
    	}
    	if (!objFreeMoves.isEmpty()){
    		if (objFreeMoves.size()>1){
    			Collections.shuffle(objFreeMoves,this.objRandom);
    		}
    			TileAimKey objTileAimKey=objFreeMoves.get(0);
    			issueOrder(objTileAimKey.getTile(), objTileAimKey.getAim());
    			objResultMove.setAimMoved(objTileAimKey.getAim());
    			objResultMove.setIssuedOrder(true);
    			objResultMove.setNumAntsAssigned(1);
    	}else {
    		boolean bBattleMoveIssued=false;
    	
    		if (!objBattlePlanMoves.isEmpty()){
    	
    		List<BattlePlan> objListBattlePlan=new ArrayList<BattlePlan>();
    		for (TileAimKey objTileAimKey : objBattlePlanMoves) {
				objListBattlePlan.addAll(objGeneral.computeBattlePlanList(objTileAimKey, objComparator));
			}
    		Collections.sort(objListBattlePlan,objComparator);
    		
    		Iterator<BattlePlan> it=objListBattlePlan.iterator();
    		while(it.hasNext()&&!bBattleMoveIssued){
    			BattlePlan objBattlePlan=it.next();
    			if (objComparator.canAcceptBattlePlan(objBattlePlan)){
    				if (this.isBattlePlanCompatible(objBattlePlan)){
    					this.issuePlan(objBattlePlan);
    					objResultMove.setIssuedOrder(true);
    					objResultMove.setBattlePlanIssued(objBattlePlan);
    					objResultMove.setNumAntsAssigned(objBattlePlan.getActionsOfPlan().size());
    					objResultMove.setAimMoved(objBattlePlan.getTileAimForAnt(objTileOrigin).getAim());
    					bBattleMoveIssued=true;
   
    				}else{
    					objResultMove.addReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID);
    				}
    			}else{
    				if (this.isBattlePlanCompatible(objBattlePlan)){
    					objResultMove.addBattlePlan(objBattlePlan);
    				}
					objResultMove.addReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID);
    			}
    		}
    		if (!bBattleMoveIssued){
    			objResultMove.addReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID);
    		}
    		
    	} 
    	if (!objPushMoves.isEmpty()&&!bBattleMoveIssued){
    		if (objPushMoves.size()>1){
       			Collections.shuffle(objPushMoves,this.objRandom);
    		}
       			boolean bFinded=false;
       			Iterator<TileAimKey> it=objPushMoves.iterator();
       			while (it.hasNext()&&!bFinded){
       				TileAimKey objCurrent=it.next();
       				if (this.pushAnt(objCurrent)){
       					bFinded=true;
       					this.issueOrder(objCurrent.getTile(), objCurrent.getAim());
       					objResultMove.setIssuedOrder(true);
       					objResultMove.setNumAntsAssigned(1);
       					objResultMove.setAimMoved(objCurrent.getAim());
       				}else{
       					objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT);
       				}
       			}
    		
    	}
    	}
    	
    	
    	
    	return objResultMove;
    	
    }

	private boolean pushAnt(TileAimKey objCurrent) {
		Set<Tile> objPreviousTiles=new HashSet<Tile>();
		objPreviousTiles.add(objCurrent.getTile());
		
		return pushAnt(objCurrent.getTileDest(),objCurrent.getAim(),objPreviousTiles);
	}

	private boolean pushAnt(Tile objMyAnt,Aim directionOfPush, Set<Tile> objPreviousTiles) {
		List <Aim> objListAim=Aim.getPerpendicular(directionOfPush);
		Collections.shuffle(objListAim,this.objRandom);
		objListAim.add(directionOfPush);
		objPreviousTiles.add(objMyAnt);
		if (this.isAntIssuedOrder(objMyAnt)){
			return false;
		}
		boolean bFinded=false;
		Iterator<Aim> it=objListAim.iterator();
		List<Aim> objPushMoves=new ArrayList<Aim>();
		while(it.hasNext()&&!bFinded){
			Aim aim=it.next();
			Tile objDestTile=this.getTile(objMyAnt, aim);
			if (!objGeneral.isAffectedTile(objDestTile)){
			if (!objPreviousTiles.contains(objDestTile)){
					if (isCurrentlyPassable(objDestTile,null)){
						this.issueOrder(objMyAnt, aim);
						bFinded=true;
					}else{
						if (getTileInformation(objDestTile).getType()==Ilk.MY_ANT){
							objPushMoves.add(aim);
						}
					}
				}
			}
		}
		if (!bFinded){
			if (!objPushMoves.isEmpty()){
				it=objPushMoves.iterator();
				while(it.hasNext()&&!bFinded){
					Aim aim=it.next();
					Tile objDestTile=this.getTile(objMyAnt, aim);
					if (pushAnt(objDestTile,aim, objPreviousTiles)){
						this.issueOrder(objMyAnt, aim);
						bFinded=true;
					}
				}
			}
		}
		
		
		
		return bFinded;
	}

	public void issuePlan(BattlePlan objBattlePlan) {
		Set<TileAimKey> objActionsOfPlan=objBattlePlan.getActionsOfPlan();
		for (TileAimKey objAction : objActionsOfPlan) {
			if (!isAntIssuedOrder(objAction.getTile())){
				issueOrder(objAction.getTile(), objAction.getAim());
			}
		}
		
	}

	private boolean isBattlePlanCompatible(BattlePlan objBattlePlan) {
		boolean bCompatible=true;
		Set<TileAimKey> objActionsOfPlan=objBattlePlan.getActionsOfPlan();
		Iterator<TileAimKey> it=objActionsOfPlan.iterator();
		while(it.hasNext()&&bCompatible){
			TileAimKey objAction=it.next();
			if (isAntIssuedOrder(objAction.getTile())){
				if (!this.originOrdersAim.contains(objAction)){
					bCompatible=false;
				}
			}else{
				if (!this.isCurrentlyPassable(objAction.getTileDest(),null)){
					if (objBattlePlan.getTileAimForAnt(objAction.getTileDest()) !=null){
						bCompatible=true;
					}else{
						bCompatible=false;
					}
				}
			}
		}
		return bCompatible;
	}

	public boolean isAntIssuedThisOrder(TileAimKey objTileAimKey){
		return this.originOrdersAim.contains(objTileAimKey);
	}
	


	public int adjustCol(int col) {
    	if (col<0 || col>=this.cols){
    		
    		col=col%this.cols;
            if (col < 0) {
                col += this.cols;
            }
    	}
    	return col;

	}

	public int adjustRow(int row) {
    	if (row<0 || row>=this.rows){
    		
    		row=row%this.rows;
            if (row < 0) {
                row += this.rows;
            }
    	}
    	return row;
	}

	

	public int getCurrentTurn() {
		return iCurrentTurn;
	}
	
	public void clearVisibleTiles(){
		this.visibleTiles.clear();
	}

	public List<Tile> queryTilesMyAnts(Tile objCenter, int iLimit) {
		return myAntsIndex.queryTiles(objCenter, iLimit);
	}
	
	public boolean isCurrentlyPassableStatic(Tile objTile,ResultMove objResultMove){
		boolean bResult=true;
		

		if (!getTileInformation(objTile).getType().isPassable()){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_WATER);
			}

		}
	
	
		if (this.myHills.contains(objTile)){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_MY_HILL);
			}
		}
		if (this.destOrders.contains(objTile)){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT);
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_DEST_ORDER);
			}
		}
		
		if (this.getTileInformation(objTile).getType()==Ilk.ENEMY_ANT){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_ENEMY_ANT);
			}
		}
		if (this.getTileInformation(objTile).isEnemyAdjacent()){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_ENEMY_ANT);
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_ENEMY_ANT_ADJACENT);
			}
		}
		if (this.getTileInformation(objTile).getType()==Ilk.FOOD){
			bResult=false;
			if (objResultMove!=null){
				objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_FOOD);
			}
		}

		return bResult;
	}
	
	public boolean isCurrentlyPassable(Tile objTile,ResultMove objResultMove){
		
		boolean bResult=true;
		

		bResult=this.isCurrentlyPassableStatic(objTile, objResultMove);
		
		if(this.myAnts.contains(objTile)&&!this.originOrders.contains(objTile)){
				bResult=false;
				if (objResultMove!=null){
					objResultMove.addReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT);
				}

			}
		
		return bResult;
	}
	
	public boolean isAntIssuedOrder(Tile objMyTile){
		return this.originOrders.contains(objMyTile);
	}
	
	public Path getPath(Tile objTileInicio, Tile objTileFin){
		return this.getPath(objTileInicio, objTileFin, Integer.MAX_VALUE);
	}

	public Path getPath(Tile objTileInicio, Tile objTileFin,int iMaxLength){
		return this.objPathFinder.findPath(objTileInicio, objTileFin,iMaxLength,(int) (this.getTurnTime()*0.05));
	}

	public List<RoomInstanceNode> getRoomsAtCost(Tile objTileStart,int iMaxCost){
		return this.objPathFinder.getRoomsAtCost(objTileStart, iMaxCost, (int) (this.getTurnTime()*0.05));
	}
	
	public TileTemplate getAttackTemplate() {
		return attackTemplate;
	}

	public TileTemplate getViewTemplate() {
		return viewTemplate;
	}

	public TileTemplate getSpawnTemplate() {
		return spawnTemplate;
	}

	public void updateSpacePartitioning() {
		this.objPathFinder.processUpdate();
		for (Tile objMyAntTile : myAnts) {
			this.objPathFinder.addAntTile(objMyAntTile, false, this);
		}
		for (Tile objEnemyAntTile : enemyAnts) {
			this.objPathFinder.addAntTile(objEnemyAntTile, true, this);
		}

		for (Tile objMyHill : myHills) {
			this.objPathFinder.addHillTile(objMyHill, false, this);
		}
		
		for (Tile objEnemyHill : enemyHills) {
			this.objPathFinder.addHillTile(objEnemyHill, true, this);
		}
	}

	public Random getRandom() {
		return objRandom;
	}

	public void clearCacheBattlePlanGeneral() {
		objGeneral.clearInfo();
		
	}

	public void updateBattlePlanGeneral() {
		this.objGeneral.processBattleInfo();
		
	}

	public Room<RoomInfoPartition> getRoomAtTile(Tile objTile) {
		return objPathFinder.getRoomAtTile(objTile);
	}


	public boolean isAntInDanger(Tile objTile) {
		return objGeneral.isAffectedTile(objTile);
	}


	public BattlePlanGeneral getGeneral() {
		return objGeneral;
	}


	public AntsExploration getAntsExploration() {
		return objAntsExploration;
	}

	
	
	
	
}
