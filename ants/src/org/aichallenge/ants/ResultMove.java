package org.aichallenge.ants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.cccf.ants.battleplan.BattlePlan;

public class ResultMove {
	private Aim objAimMoved;
	private boolean bIssuedOrder;
	private Set<BlockedMovesReasons> objBlockedMovesReasons;
	private int iNumAntsAssigned;
	private List<BattlePlan> objListBattlePlan;
	private BattlePlan objBattlePlanIssued;
	public Aim getAimMoved() {
		return objAimMoved;
	}
	
	public ResultMove() {
		objAimMoved=null;
		bIssuedOrder=false;
		objBlockedMovesReasons=new HashSet<BlockedMovesReasons>();
		objListBattlePlan=new ArrayList<BattlePlan>();
	}
	
	public void setAimMoved(Aim objAimMoved) {
		this.objAimMoved = objAimMoved;
	}
	public boolean isIssuedOrder() {
		return bIssuedOrder;
	}
	public void setIssuedOrder(boolean objIssuedOrder) {
		bIssuedOrder = objIssuedOrder;
	}
	public Set<BlockedMovesReasons> getBlockedMovesReasons() {
		return objBlockedMovesReasons;
	}
	public boolean addReason(BlockedMovesReasons objE) {
		return objBlockedMovesReasons.add(objE);
	}
	public boolean containsReason(BlockedMovesReasons objO) {
		return objBlockedMovesReasons.contains(objO);
	}

	@Override
	public String toString() {
		return String.format("ResultMove [objAimMoved=%s, bIssuedOrder=%s, objBlockedMovesReasons=%s]", objAimMoved, bIssuedOrder,
				objBlockedMovesReasons);
	}

	public int getNumAntsAssigned() {
		return iNumAntsAssigned;
	}

	public void setNumAntsAssigned(int objNumAntsAssigned) {
		iNumAntsAssigned = objNumAntsAssigned;
	}

	public List<BattlePlan> getListBattlePlan() {
		return objListBattlePlan;
	}
	
	public void addBattlePlan(BattlePlan objNewBattlePlan){
		this.objListBattlePlan.add(objNewBattlePlan);
	}

	public BattlePlan getBattlePlanIssued() {
		return objBattlePlanIssued;
	}

	public void setBattlePlanIssued(BattlePlan objBattlePlanIssued) {
		this.objBattlePlanIssued = objBattlePlanIssued;
	}
	
}
