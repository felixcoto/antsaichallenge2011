package org.aichallenge.ants;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


public class TileTemplate {
	private int sqrsize;
	private Set<Tile> objTileSet;
	private Set<Tile> objTilesAdjacentByRow;
	private Set<Tile> objTilesAdjacentByCol;
	
	public TileTemplate(int objSqrsize) {
		sqrsize = objSqrsize;
		objTileSet=new LinkedHashSet<Tile>();
		objTilesAdjacentByRow=new LinkedHashSet<Tile>();
		objTilesAdjacentByCol=new LinkedHashSet<Tile>();
		computeTemplate();
	}
	
	private void computeTemplate(){
		
		int maxCoordinate=(int) Math.floor(Math.sqrt(sqrsize));
		for (int row=0;row<=maxCoordinate;row++){
			for (int col=0;col<=maxCoordinate;col++){
				int currentLength=row*row+col*col;
				if (currentLength<=sqrsize){
					this.objTileSet.add(new Tile(row,col));
				}
			}
		}
		// calculamos los adjacentes
		for (int row=0;row<=maxCoordinate+1;row++){
			for (int col=0;col<=maxCoordinate+1;col++){
				Tile objTile=new Tile(row,col);
				if (!this.objTileSet.contains(objTile)){
					Tile objRowAdjacent=new Tile(objTile.getRow()-1,objTile.getCol());
					if (this.objTileSet.contains(objRowAdjacent)){
						this.objTilesAdjacentByRow.add(objTile);
					}
					Tile objColAdjacent=new Tile(objTile.getRow(),objTile.getCol()-1);
					if (this.objTileSet.contains(objColAdjacent)){
						this.objTilesAdjacentByCol.add(objTile);
					}

				}
			}
		}
	}
	
	public boolean isTileInRange(Tile center,Tile dest,Ants ants){
		Tile testTile=getTileCanonical(center,dest, ants);
		boolean bIsInRange=this.objTileSet.contains(testTile);
		return bIsInRange;
	}
	
	public Set<Tile> getTilesInRange(Tile center,Ants ants){
		Set<Tile> tileSetResult=new HashSet<Tile>();
		for (Iterator<Tile> objIterator = this.objTileSet.iterator(); objIterator.hasNext();) {
			Tile objTile = objIterator.next();
			Tile objNewTile=new Tile(center.getRow()+objTile.getRow(),center.getCol()+objTile.getCol());
			objNewTile=ants.getCanonicalTile(objNewTile);
			tileSetResult.add(objNewTile);
			objNewTile=new Tile(center.getRow()-objTile.getRow(),center.getCol()+objTile.getCol());
			objNewTile=ants.getCanonicalTile(objNewTile);
			tileSetResult.add(objNewTile);
			objNewTile=new Tile(center.getRow()+objTile.getRow(),center.getCol()-objTile.getCol());
			objNewTile=ants.getCanonicalTile(objNewTile);
			tileSetResult.add(objNewTile);
			objNewTile=new Tile(center.getRow()-objTile.getRow(),center.getCol()-objTile.getCol());
			objNewTile=ants.getCanonicalTile(objNewTile);
			tileSetResult.add(objNewTile);
		}
		return tileSetResult;
	}
	
	public int getMahattanDistanceTemplate(Tile center,Tile dest,Ants ants){
		Tile objTileCanonical=getTileCanonical(center, dest, ants);
		int iDistance=Integer.MAX_VALUE;
		if (this.objTileSet.contains(objTileCanonical)){
			iDistance=0;
		}else{
			for (Tile objCurrentTile : this.objTileSet) {
				iDistance=Math.min(iDistance, ants.getManhattanDistance(objCurrentTile, objTileCanonical));
			}
		}
		return iDistance;
	}
	
	
    private Tile getTileCanonical(Tile t1, Tile t2,Ants ants) {
        int rowDelta = Math.abs(t1.getRow() - t2.getRow());
        int colDelta = Math.abs(t1.getCol() - t2.getCol());
        rowDelta = Math.min(rowDelta, ants.getRows() - rowDelta);
        colDelta = Math.min(colDelta, ants.getCols() - colDelta);
        return new Tile(rowDelta,colDelta);
    }
    
    
    
    public Set<Tile> getNewTilesByAim(Tile center,Aim aim,Ants ants){
    	Set<Tile> objResult=new LinkedHashSet<Tile>();
    	switch (aim){
    	case NORTH:
    	case SOUTH:
    		for (Tile objTile : this.objTilesAdjacentByRow) {
				Tile objNewTile=new Tile(center.getRow()+(objTile.getRow()*aim.getRowDelta()),center.getCol()+objTile.getCol());
				objNewTile=ants.getCanonicalTile(objNewTile);
				objResult.add(objNewTile);
				objNewTile=new Tile(center.getRow()+(objTile.getRow()*aim.getRowDelta()),center.getCol()-objTile.getCol());
				objNewTile=ants.getCanonicalTile(objNewTile);
				objResult.add(objNewTile);
			}
    		break;
    	case EAST:
    	case WEST:
    		for (Tile objTile : this.objTilesAdjacentByCol) {
				Tile objNewTile=new Tile(center.getRow()+objTile.getRow(),center.getCol()+(objTile.getCol()*aim.getColDelta()));
				objNewTile=ants.getCanonicalTile(objNewTile);
				objResult.add(objNewTile);
				objNewTile=new Tile(center.getRow()-objTile.getRow(),center.getCol()+(objTile.getCol()*aim.getColDelta()));
				objNewTile=ants.getCanonicalTile(objNewTile);
				objResult.add(objNewTile);
			}

    			
    	}
    	return objResult;
    }
    

	public static void main(String[] args) {
		int[] aList=new int[]{5,77};
		
		Ants ants=new Ants(0,0,60,90,900,77,5,1,42);
		for (int size : aList) {
			TileTemplate objTest=new TileTemplate(size);
			StringBuilder sb=new StringBuilder();
			sb.append(size);
			sb.append(" Template:");
			sb.append(objTest.objTileSet);
			sb.append("\n");
			sb.append(size);
			sb.append(" AdByRow");
			sb.append(objTest.objTilesAdjacentByRow);
			sb.append("\n");
			sb.append(size);
			sb.append(" AdByCol");
			sb.append(objTest.objTilesAdjacentByCol);
			Tile objCenter=new Tile(2,2);
			sb.append("\n");
			sb.append(size);
			sb.append( " NORTH TEST:");
			sb.append(objTest.getNewTilesByAim(objCenter, Aim.NORTH, ants));
			System.out.println(sb.toString());
		}
		
	}
}
