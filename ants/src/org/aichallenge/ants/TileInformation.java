package org.aichallenge.ants;

public class TileInformation {
	Tile objTile;
	Ilk objType;
	int iLastTurnSeen;
	int iPlayerEnemyAnt;
	boolean hill;
	int iPlayerHill;
	int iLastTurnPassedMyAnt;
	boolean bEnemyAdjacent;
	/**
	 * @param objTile
	 */
	public TileInformation(Tile objTile) {
		this.objTile = objTile;
		this.objType=Ilk.LAND;
		this.iLastTurnSeen=-30;
		this.iPlayerEnemyAnt=-1;
		this.hill=false;
		this.iPlayerHill=-1;
		this.iLastTurnPassedMyAnt=-1;
		this.bEnemyAdjacent=false;
	}
	
	public Tile getTile() {
		return objTile;
	}
	public void setTile(Tile objTile) {
		this.objTile = objTile;
	}
	public Ilk getType() {
		return objType;
	}
	public void setType(Ilk objType) {
		this.objType = objType;
	}
	public int getLastTurnSeen() {
		return iLastTurnSeen;
	}
	public void setLastTurnSeen(int objLastTurnSeen) {
		iLastTurnSeen = objLastTurnSeen;
	}
	public int getPlayerEnemyAnt() {
		return iPlayerEnemyAnt;
	}
	public void setPlayerEnemyAnt(int objPlayerEnemyAnt) {
		iPlayerEnemyAnt = objPlayerEnemyAnt;
	}
	public boolean isHill() {
		return hill;
	}
	public void setHill(boolean objHill) {
		hill = objHill;
	}
	public int getPlayerHill() {
		return iPlayerHill;
	}
	public void setPlayerHill(int objPlayerHill) {
		iPlayerHill = objPlayerHill;
	}

	public int getLastTurnPassedMyAnt() {
		return iLastTurnPassedMyAnt;
	}

	public void setLastTurnPassedMyAnt(int objLastTurnPassedMyAnt) {
		iLastTurnPassedMyAnt = objLastTurnPassedMyAnt;
	}

	public boolean isPassable() {
	
		return this.objType.isPassable();
	}

	public boolean isEnemyAdjacent() {
		return bEnemyAdjacent;
	}

	public void setEnemyAdjacent(boolean objEnemyAdjacent) {
		bEnemyAdjacent = objEnemyAdjacent;
	}
	
	
	
	
}
