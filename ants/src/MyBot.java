import java.io.IOException;

import es.cccf.ants.JacobStraussBot;


public class MyBot {

    /**
     * Main method executed by the game engine for starting the bot.
     * 
     * @param args command line arguments
     * 
     * @throws IOException if an I/O error occurs
     */
    public static void main(String[] args) throws IOException {
    	
        new JacobStraussBot().readSystemInput();
    }
    

}
