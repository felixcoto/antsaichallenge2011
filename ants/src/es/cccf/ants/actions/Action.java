package es.cccf.ants.actions;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;


public interface Action {
	public ResultadoAccion doAccion(Tile objTileObjetivo,int prioridad,int iSigPrioridad,Ants ants);
	public ResultadoAccion antIssuedOrder(Tile objMyAnt,int prioridad,int iSigPrioridad,Ants ants);

}
