package es.cccf.ants.actions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.BlockedMovesReasons;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;
import es.cccf.ants.spacepartitioning.Path;


public class SeekFoodAction implements Action {
	

	private static Map<Tile,RepeatTileStatus> objCurrentPathsTile=new HashMap<Tile, SeekFoodAction.RepeatTileStatus>();
	
	Map<Tile,TileStatus> objTileStatus;
	Tile objFoodTile;
	boolean bFinished;
	private int iAntsAsigned=0;

	/**
	 * @param objFoodTile TODO
	 * @param objMyAntsList
	 */
	public SeekFoodAction(Tile objFoodTile, List<Tile> objMyAntsList) {
		this.objTileStatus = new HashMap<Tile, SeekFoodAction.TileStatus>();
		for (Tile objTile : objMyAntsList) {
			this.objTileStatus.put(objTile, new TileStatus(objTile));
		}
		this.objFoodTile=objFoodTile;
	}


	@Override
	public ResultadoAccion doAccion(Tile objMyAntTile, int objPrioridad,int iSigPrioridad, Ants ants) {
		ResultadoAccion objResultado;
    	if (ants.getCurrentTurn()==11&& objMyAntTile.getRow()==49&&objMyAntTile.getCol()==36 && objFoodTile.getRow()==52 && objFoodTile.getCol()==43 ){
    		System.err.println("TEST");
    	}

		if (iAntsAsigned==0){
			TileStatus objStatus=this.objTileStatus.get(objMyAntTile);
			if (objStatus==null){
				objResultado=new ResultadoAccion(true, 0);
			}else{
				if (objStatus.getPath()!=null){
						Tile objCurrentTile=objStatus.getPath().getNextStep(objMyAntTile);
						BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objMyAntTile, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
						ResultMove objResultMove=ants.issueMove(objMyAntTile, objCurrentTile, objComparator);
						
						if (objResultMove.isIssuedOrder()){
							JacobStraussBot.log(String.format("Busqueda comida asignado a %1$s",objMyAntTile) , objFoodTile, LogLevel.INFO);
							objCurrentPathsTile.put(objMyAntTile, new RepeatTileStatus(objStatus.getPath()));
							iAntsAsigned++;
						}else{
							if (objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_FOOD)){
								objResultMove=ants.issueMove(objMyAntTile, null, true,  objComparator);
								
								if (objResultMove.isIssuedOrder()){
									JacobStraussBot.log(String.format("Busqueda comida asignado a %1$s, parado",objMyAntTile) , objFoodTile, LogLevel.INFO);
									
									objCurrentPathsTile.put(objMyAntTile, new RepeatTileStatus(objStatus.getPath()));
									iAntsAsigned++;
								}
								
							}else if (objResultMove.containsReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID)){
								if (ants.isAntInDanger(objMyAntTile)){
//									objComparator=new DeadSurviveRatioBattlePlanComparator(0.1,0.01);
									List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
									if (objListOfAim.size()==1){
										Aim aim=objListOfAim.get(0);
										objListOfAim=Aim.getPerpendicular(objListOfAim.get(0));
										objListOfAim.add(Aim.getOpposite(aim));
									}else{
										List<Aim> objNewAimList=new ArrayList<Aim>();
										for (Aim objAim : objListOfAim) {
											objNewAimList.add(Aim.getOpposite(objAim));
										}
										objListOfAim=objNewAimList;
									}
									objResultMove=ants.issueMove(objMyAntTile, objListOfAim,true,  objComparator);
									if (objResultMove.isIssuedOrder()){
										JacobStraussBot.log(String.format("Busqueda comida asignado a %1$s, huyendo",objMyAntTile) , objFoodTile, LogLevel.INFO);
									}
								}
							}
						}
					objResultado=new ResultadoAccion(true, 0);
				}else{
					int iDistancia=getDistancia(iSigPrioridad);
					Path objTempPath=ants.getPath(objMyAntTile, objFoodTile,iDistancia);
					if (objTempPath==null){
						objResultado=new ResultadoAccion(true, 0);
					}else{
						if (objTempPath.isComplete()){
							objStatus.setPath(objTempPath);
						}
						objResultado=new ResultadoAccion(false, getPriority(objTempPath.getCost()));
					}
				}
			}
		}else{
			objResultado=new ResultadoAccion(true, 0);
		}

			
		return objResultado;
	}

	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		objCurrentPathsTile.clear();
		List<AccionPrioridad> objResult=new ArrayList<AccionPrioridad>();
		for (Tile objFoodTile : ants.getFoodTiles()) {
			List<Tile> objMyAntsList=ants.queryTilesMyAnts(objFoodTile, JacobStraussBot.SEEK_FOOD_NUM_ANTS_SEARCH);
			SeekFoodAction objAccion=new SeekFoodAction(objFoodTile, objMyAntsList);
			for (Tile objAntTile : objMyAntsList) {
					int iPrioridad=getPriority(ants.getManhattanDistance(objAntTile, objFoodTile));
					objResult.add(new AccionPrioridad(iPrioridad, objAntTile, objAccion));
			}
			
		}
		return objResult;
	}
	
	private  static int getPriority(int iDistancia){
		int iPrioridad=JacobStraussBot.SEEK_FOOD_INITIAL_PRIORITY;
		iPrioridad+=iDistancia*JacobStraussBot.SEEK_FOOD_MODIFIER;
		return iPrioridad;
	}
	private static int getDistancia(int iPrioridad){
		int iDistancia=iPrioridad-JacobStraussBot.SEEK_FOOD_INITIAL_PRIORITY;
		iDistancia=iDistancia/JacobStraussBot.SEEK_FOOD_MODIFIER;
		return iDistancia+1;
	}

	private class TileStatus{
		Tile objTile;
		Path objPath=null;
		/**
		 * @param objTile
		 */
		public TileStatus(Tile objTile) {
			this.objTile = objTile;
		}
		public Path getPath() {
			return objPath;
		}
		public void setPath(Path objPath) {
			this.objPath = objPath;
		}
		public Tile getTile() {
			return objTile;
		}
		
	}
	
	
	private class RepeatTileStatus{
		List<Path> objPaths;
		int iTotalCost;
		Tile objLastTile;
		
		public RepeatTileStatus(Path objPath){
			objPaths=new ArrayList<Path>();
			this.addPath(objPath);
		}
		
		public void addPath(Path objPath){
			objPaths.add(objPath);
			iTotalCost+=objPath.getCost();
			objLastTile=objPath.getLast();
			
		}

		public int getTotalCost() {
			return iTotalCost;
		}

		public Tile getLastTile() {
			return objLastTile;
		}
		
	}

	@Override
	public ResultadoAccion antIssuedOrder(Tile objMyAntTile, int prioridad, int iSigPrioridad, Ants ants) {
		ResultadoAccion objResult;
    	if (ants.getCurrentTurn()==31&& objMyAntTile.getRow()==5&&objMyAntTile.getCol()==42 && objFoodTile.getRow()==12 && objFoodTile.getCol()==45 ){
    		System.err.println("TEST");
    	}

		if (iAntsAsigned == 0) {

			if (objCurrentPathsTile.containsKey(objMyAntTile)) {
				RepeatTileStatus objRepeatTileStatus = objCurrentPathsTile.get(objMyAntTile);
				Tile objCurrentTile = objRepeatTileStatus.getLastTile();
				int iDistancia = objRepeatTileStatus.getTotalCost();
				iDistancia += ants.getManhattanDistance(objCurrentTile, objFoodTile);
				int iCurrentPriority = getPriority(iDistancia);
				if (iCurrentPriority > iSigPrioridad) {
					objResult = new ResultadoAccion(false, iCurrentPriority);
				} else {
					int iMaxDistance = getDistancia(iSigPrioridad);
					iMaxDistance = iMaxDistance - objRepeatTileStatus.getTotalCost();
					Path objNewPath = ants.getPath(objCurrentTile, objFoodTile, iMaxDistance);
					if (objNewPath.isComplete()) {
						iAntsAsigned++;
						JacobStraussBot.log(String.format("Busqueda comida REasignado a %1$s", objMyAntTile), objFoodTile, LogLevel.INFO);
						objRepeatTileStatus.addPath(objNewPath);
						objResult = new ResultadoAccion(true, 0);
					} else {
						iDistancia = objNewPath.getCost()+objRepeatTileStatus.getTotalCost();
						iCurrentPriority =getPriority(iDistancia);
						objResult = new ResultadoAccion(false, iCurrentPriority);
					}
				}
			}else{
				objResult=new ResultadoAccion(true, 0);
			}

		} else {
			objResult = new ResultadoAccion(true, 0);
		}
		return objResult;
	}


	@Override
	public String toString() {
		return String.format("BuscarComidaAccion [objFoodTile=%s, bFinished=%s, iAntsAsigned=%s]", objFoodTile, bFinished, iAntsAsigned);
	}
}
