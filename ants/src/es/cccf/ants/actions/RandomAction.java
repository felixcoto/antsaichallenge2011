package es.cccf.ants.actions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;

public class RandomAction implements Action {

	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		List<AccionPrioridad> objResult=new LinkedList<AccionPrioridad>();
		RandomAction objAccion=new RandomAction();
		for (Tile objMyAnt : ants.getMyAnts()) {
			int iPrioridad=JacobStraussBot.RANDOM_ACCION_INITIAL_PRIORITY;
			for (Aim aim : Aim.values()) {
				Tile objTileObjetivo=ants.getTile(objMyAnt, aim);
				if (!ants.isCurrentlyPassable(objTileObjetivo,null)){
					iPrioridad+=JacobStraussBot.RANDOM_ACCION_MODIFIER;
				}
			}
			objResult.add(new AccionPrioridad(iPrioridad, objMyAnt, objAccion));
		}
		return objResult;
	}
	
	@Override
	public ResultadoAccion doAccion(Tile objMyAnt, int objPrioridad, int iSigPrioridad, Ants ants) {
		List<Aim> objAimLists=Arrays.asList(Aim.values());
		BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objMyAnt, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
		ResultMove objResultMove=ants.issueMove(objMyAnt, objAimLists,  objComparator);
		if (objResultMove.isIssuedOrder()){ 
			JacobStraussBot.log(String.format("RANDOM efectuado con %1$s",objMyAnt) , null, LogLevel.INFO);
		}
		
		return new ResultadoAccion(true, 0);
	}

	@Override
	public ResultadoAccion antIssuedOrder(Tile objMyAnt,int prioridad,int iSigPrioridad,Ants ants){
		// NADA
		return new ResultadoAccion(true, 0);
	}

}
