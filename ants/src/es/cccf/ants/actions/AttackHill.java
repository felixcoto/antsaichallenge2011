package es.cccf.ants.actions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.BlockedMovesReasons;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;
import es.cccf.ants.battleplan.BattlePlan;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;
import es.cccf.ants.spacepartitioning.Path;


public class AttackHill implements Action {
	


	

	Tile objHillTile;
	Map<Tile,TileStatus> objTileStatusMap;

	int iAntsAsigned;
	int iMaxAntsToAsign;

	/**
	 * @param objUnknownTile TODO
	 * @param objMyAntsList
	 */
	public AttackHill(Tile objHillTile, List<Tile> objMyAntsList,int iMaxAntsToAsign,Ants ants) {
		this.objHillTile=objHillTile;
		this.objTileStatusMap=new HashMap<Tile, AttackHill.TileStatus>();
		for (Tile objTile : objMyAntsList) {
			TileStatus objTileStatus= new TileStatus(objTile);
			objTileStatusMap.put(objTile,objTileStatus);
			objTileStatus.setEstimatedDistance(ants.getManhattanDistance(objTile, objHillTile));
		}
		this.iMaxAntsToAsign=iMaxAntsToAsign;
	}


	@Override
	public ResultadoAccion doAccion(Tile objMyAntTile, int objPrioridad,int iSigPrioridad, Ants ants) {
		ResultadoAccion objResultado;

		if (iAntsAsigned<iMaxAntsToAsign){
			TileStatus objStatus=this.objTileStatusMap.get(objMyAntTile);
			if (objStatus==null){
				objResultado=new ResultadoAccion(true, 0);
			}else{
				int iCurrentPriority=getPriority(objStatus.getEstimatedDistance());
				if (iCurrentPriority>iSigPrioridad){
					objResultado=new ResultadoAccion(false, iCurrentPriority);
				}else{
				if (objStatus.getPath()!=null){
					if (ants.getCurrentTurn()==1 && objMyAntTile.getRow()==31&&objMyAntTile.getCol()==83){
						System.err.println("TEST");
					}
					Path objPath=objStatus.getPath();
						Tile objCurrentTile=objPath.getNextStep(objMyAntTile);
						BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objMyAntTile, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
					ResultMove objResultMove=ants.issueMove(objMyAntTile, objCurrentTile, objComparator);
					List<BattlePlan> objPartialBattlePlanList=new ArrayList<BattlePlan>();
					if (objResultMove.isIssuedOrder()){
						JacobStraussBot.log(String.format("Atacar hill asignado a %1$s",objMyAntTile) , objHillTile, LogLevel.INFO);
						
						iAntsAsigned+=objResultMove.getNumAntsAssigned();
					}else{
						if (objResultMove.containsReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID)){
							objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
							objResultMove=ants.issueMove(objMyAntTile, null, true,  objComparator);
							if (objResultMove.isIssuedOrder()){
								JacobStraussBot.log(String.format("Atacar hill asignado a %1$s, parado",objMyAntTile) , objHillTile, LogLevel.INFO);

								iAntsAsigned+=objResultMove.getNumAntsAssigned();

							}else if (ants.isAntInDanger(objMyAntTile)){
								objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
								List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
								if (objListOfAim.size()==1){
									Aim aim=objListOfAim.get(0);
									objListOfAim=Aim.getPerpendicular(objListOfAim.get(0));
									objListOfAim.add(Aim.getOpposite(aim));
								}else{
									List<Aim> objNewAimList=new ArrayList<Aim>();
									for (Aim objAim : objListOfAim) {
										objNewAimList.add(Aim.getOpposite(objAim));
									}
									objListOfAim=objNewAimList;
								}
								objResultMove=ants.issueMove(objMyAntTile, objListOfAim,  objComparator);
								if (objResultMove.isIssuedOrder()){
									JacobStraussBot.log(String.format("Atacar hill asignado a %1$s, huyendo",objMyAntTile) , objHillTile, LogLevel.INFO);
								}else{
									objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
									// En este punto como se esta en peligro y no se puede hacer nada para evitarlo, utilizamos el mejor movimiento de forma unilateral
									if (!objPartialBattlePlanList.isEmpty()){
										Collections.sort(objPartialBattlePlanList,objComparator);
										if (objComparator.canAcceptBattlePlan(objPartialBattlePlanList.get(0))){
											ants.issuePlan(objPartialBattlePlanList.get(0));
											JacobStraussBot.log(String.format("Atacar hill asignado a %1$s, realizando movimiento desesperado",objMyAntTile) , objHillTile, LogLevel.INFO);
										}else{
											JacobStraussBot.log(String.format("Atacar hill asignado a %1$s, movimiento desesperado descartado",objMyAntTile) , objHillTile, LogLevel.INFO);
										}
										
										
									}
								}
							}
						}else if (objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT)){
							List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
							if (objListOfAim.size()==1){
								Aim aim=objListOfAim.get(0);
								objListOfAim=Aim.getPerpendicular(objListOfAim.get(0));
								
							}else{
								List<Aim> objNewAimList=new ArrayList<Aim>();
								for (Aim objAim : objListOfAim) {
									objNewAimList.add(Aim.getOpposite(objAim));
								}
								objListOfAim=objNewAimList;
							}
							objResultMove=ants.issueMove(objMyAntTile, objListOfAim,  objComparator);

							
							if (objResultMove.isIssuedOrder()){
								JacobStraussBot.log(String.format("Atacar hill asignado a %1$s, esquivando hormigas atascadas",objMyAntTile) , objHillTile, LogLevel.INFO);

								iAntsAsigned+=objResultMove.getNumAntsAssigned();

							}
						}
					}
					objResultado=new ResultadoAccion(true, 0);
				}else{
					int iDistancia=getDistancia(iSigPrioridad);
					Path objTempPath=ants.getPath(objMyAntTile, objHillTile,iDistancia);
					if (objTempPath==null){
						objResultado=new ResultadoAccion(true, 0);
					}else{
						objStatus.setEstimatedDistance(objTempPath.getCost());
						if (objTempPath.isComplete()){
							objStatus.setPath(objTempPath);
						}
						objResultado=new ResultadoAccion(false, getPriority(objTempPath.getCost()));
					}
				}
			}
			}
		}else{
			objResultado=new ResultadoAccion(true, 0);
		}

			
		return objResultado;

		
	}

	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		List<AccionPrioridad> objResult=new ArrayList<AccionPrioridad>();
		for (Tile objFoodTile : ants.getEnemyHills()) {
			List<Tile> objMyAntsList=ants.queryTilesMyAnts(objFoodTile, JacobStraussBot.ATTACK_HILL_NUM_ANTS_SEARCH);
			AttackHill objAccion=new AttackHill(objFoodTile, objMyAntsList,(int) Math.ceil(ants.getMyAnts().size()/4),ants);
			for (Tile objAntTile : objMyAntsList) {
				int iPrioridad=objAccion.getPriority(ants.getManhattanDistance(objAntTile, objFoodTile));
				objResult.add(new AccionPrioridad(iPrioridad, objAntTile, objAccion));
			}
			
		}
		return objResult;
	}
	
	private  int getPriority(int iDistancia){
		int iPrioridad=JacobStraussBot.ATTACK_HILL_INITIAL_PRIORITY;
		iPrioridad+=iDistancia*JacobStraussBot.ATTACK_HILL_MODIFIER;
		iPrioridad+=iAntsAsigned*JacobStraussBot.ATTACK_HILL_MODIFIER_ANTS_ASSIGNED;
		return iPrioridad;
	}
	
	private  int getDistancia(int iPrioridad){
		int iPrioridadCalc=getPriority(0);
		iPrioridadCalc=iPrioridad-iPrioridadCalc;
		int iDistancia=iPrioridadCalc/JacobStraussBot.ATTACK_HILL_MODIFIER;
		return iDistancia+1;
	}
	private class TileStatus{
		Tile objTile;
		Path objPath=null;
		int iEstimatedDistance;
		/**
		 * @param objTile
		 */
		public TileStatus(Tile objTile) {
			this.objTile = objTile;
		}
		public Path getPath() {
			return objPath;
		}
		public void setPath(Path objPath) {
			this.objPath = objPath;
		}
		public Tile getTile() {
			return objTile;
		}
		public int getEstimatedDistance() {
			return iEstimatedDistance;
		}
		public void setEstimatedDistance(int objEstimatedDistance) {
			iEstimatedDistance = objEstimatedDistance;
		}
		
		
	}
	@Override
	public ResultadoAccion antIssuedOrder(Tile objMyAnt,int prioridad,int iSigPrioridad,Ants ants){
		this.objTileStatusMap.remove(objMyAnt);
		return new ResultadoAccion(true, 0);
		
	}
	
	private double adjustRisk(Ants ants){
		boolean bHasHills=!ants.getMyHills().isEmpty();
		int iNumOfAnts=ants.getMyAnts().size();
		double dMinRatio=1;
		if (!bHasHills){
			dMinRatio=1;
		}else{
			if (iNumOfAnts<=10){
				dMinRatio=1;
			}else if (iNumOfAnts<=30){
				dMinRatio=0.8;
			}else if (iNumOfAnts<=50){
				dMinRatio=0.7;
			}else if (iNumOfAnts<=80){
				dMinRatio=0.6;
			}else if (iNumOfAnts<=150){
				dMinRatio=0.5;
			}else{
				dMinRatio=0.5;
			}
		}

		return dMinRatio;
	}


	@Override
	public String toString() {
		return String.format("AtacarHill [objHillTile=%s, iAntsAsigned=%s]", objHillTile, iAntsAsigned);
	}
}
