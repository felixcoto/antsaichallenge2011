package es.cccf.ants.actions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.BlockedMovesReasons;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;
import es.cccf.ants.battleplan.BattlePlan;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;
import es.cccf.ants.spacepartitioning.Path;


public class DefendHill implements Action {
	


	

	Tile objEnemyAntTile;
	Tile objMyHillTile;
	Path objPathToHill;
	boolean bEnemyPathUnreachable=false;
	
	Map<Tile,TileStatus> objTileStatusMap;

	
	int iMaxAntsToAsign;
	private int iEstimatedDistEnemyToHill;
	private Ants ants;


	
	
	public DefendHill(Tile objEnemyAntTile, Tile objMyHillTile,List<Tile> objMyAntsList, int iMaxAntsToAsign,Ants ants) {
		this.objEnemyAntTile=objEnemyAntTile;
		this.objTileStatusMap=new HashMap<Tile, DefendHill.TileStatus>();
		this.objMyHillTile=objMyHillTile;
		for (Tile objTile : objMyAntsList) {
			objTileStatusMap.put(objTile, new TileStatus(objTile,ants));
		}
		this.iEstimatedDistEnemyToHill=ants.getManhattanDistance(objEnemyAntTile, objMyHillTile);
		this.iMaxAntsToAsign=iMaxAntsToAsign;
		this.ants=ants;
	}


	@Override
	public ResultadoAccion doAccion(Tile objMyAntTile, int objPrioridad,int iSigPrioridad, Ants ants) {
		ResultadoAccion objResultado;

		if (!CombatActions.isObjetiveComplete(objEnemyAntTile)  &&!bEnemyPathUnreachable&&iEstimatedDistEnemyToHill<=JacobStraussBot.DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL){
			TileStatus objStatus=this.objTileStatusMap.get(objMyAntTile);
			if (objStatus==null){
				objResultado=new ResultadoAccion(true, 0);
			}else{

				int iCurrentPriority=getPriority(iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy());
				if (iCurrentPriority<=iSigPrioridad){
					if (objPathToHill==null){
						int iDistanceEnemyToHill=getDistanciaEnemyToHill(iSigPrioridad, objStatus);
						iDistanceEnemyToHill=Math.min(JacobStraussBot.DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL, iDistanceEnemyToHill);
						Path objTempPath=ants.getPath(objEnemyAntTile, objMyHillTile,iDistanceEnemyToHill);
						if (objTempPath==null){
							objResultado=new ResultadoAccion(true, 0);
							bEnemyPathUnreachable=true;
						}else{
							if (objTempPath.isComplete()){
								objPathToHill=objTempPath;
							}
							iEstimatedDistEnemyToHill=objTempPath.getCost();
							if (iEstimatedDistEnemyToHill>JacobStraussBot.DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL){
								bEnemyPathUnreachable=true;
								objResultado=new ResultadoAccion(true, 0);
							}else{
								objResultado=new ResultadoAccion(false, getPriority(iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy()));
							}
						}
					}else{
						if (objStatus.getPathToHill()==null){
							int iDistanceMyAntToHill=getDistanciaMyAntToHill(iSigPrioridad, objStatus);
							Path objTempPath=ants.getPath(objMyAntTile, objMyHillTile,iDistanceMyAntToHill);
							if (objTempPath==null){
								objResultado=new ResultadoAccion(true, 0);
							}else{
								if (objTempPath.isComplete()){
									objStatus.setPathToHill(objTempPath);
								}
								objStatus.setEstimatedDistToHill(objTempPath.getCost());
								objResultado=new ResultadoAccion(false, getPriority(iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy()));
							}

						}else{
							if (objStatus.getPathToEnemy()==null){
								int iDistanceMyAntToEnemy=getDistanciaMyAntToEnemy(iSigPrioridad, objStatus);
								Path objTempPath=ants.getPath(objMyAntTile, objEnemyAntTile,iDistanceMyAntToEnemy);
								if (objTempPath==null){
									objResultado=new ResultadoAccion(true, 0);
								}else{
									if (objTempPath.isComplete()){
										objStatus.setPathToEnemy(objTempPath);
									}
									objStatus.setEstimatedDistToEnemy(objTempPath.getCost());
									objResultado=new ResultadoAccion(false, getPriority(iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy()));
								}

							}else{
								if (ants.getCurrentTurn()==1&&objMyAntTile.getRow()==79&&objMyAntTile.getCol()==15){
									System.err.println("TEST");
								}
								Path objPath=objStatus.getPathToEnemy();
								Tile objCurrentTile=objPath.getNextStep(objMyAntTile);
								BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objMyAntTile, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
								ResultMove objResultMove=ants.issueMove(objMyAntTile, objCurrentTile, objComparator);
								List<BattlePlan> objPartialBattlePlanList=new ArrayList<BattlePlan>();
								if (objResultMove.isIssuedOrder()){
									JacobStraussBot.log(String.format("DefenderHill asignado a %1$s",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
									BattlePlan objBattlePlan=objResultMove.getBattlePlanIssued();
									if (objBattlePlan!=null){
										CombatActions.computeBattlePlan(objBattlePlan);
									}else{
										CombatActions.addAntsAssignedToObjetive(objEnemyAntTile, objResultMove.getNumAntsAssigned());
									}
								}else{
									if (objResultMove.containsReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID)){
										objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
										Path objPathToHill=objStatus.getPathToHill();
										Tile objCurrentTileToHill=objPathToHill.getNextStep(objMyAntTile);
										objResultMove=ants.issueMove(objMyAntTile, objCurrentTileToHill,  objComparator);
										if (objResultMove.isIssuedOrder()){
											JacobStraussBot.log(String.format("DefenderHill asignado a %1$s, retrocediendo a Hill",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
											BattlePlan objBattlePlan=objResultMove.getBattlePlanIssued();
											if (objBattlePlan!=null){
												CombatActions.computeBattlePlan(objBattlePlan);
											}else{
												CombatActions.addAntsAssignedToObjetive(objEnemyAntTile, objResultMove.getNumAntsAssigned());
											}

										}else if (ants.isAntInDanger(objMyAntTile)){
											objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
											List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
											if (objListOfAim.size()==1){
												Aim aim=objListOfAim.get(0);
												objListOfAim=Aim.getPerpendicular(objListOfAim.get(0));
												objListOfAim.add(Aim.getOpposite(aim));
											}else{
												List<Aim> objNewAimList=new ArrayList<Aim>();
												for (Aim objAim : objListOfAim) {
													objNewAimList.add(Aim.getOpposite(objAim));
												}
												objListOfAim=objNewAimList;
											}
											objResultMove=ants.issueMove(objMyAntTile, objListOfAim,  objComparator);
											if (objResultMove.isIssuedOrder()){
												JacobStraussBot.log(String.format("DefenderHill asignado a %1$s, huyendo",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
											}else{
												objPartialBattlePlanList.addAll(objResultMove.getListBattlePlan());
												// En este punto como se esta en peligro y no se puede hacer nada para evitarlo, utilizamos el mejor movimiento de forma unilateral
												if (!objPartialBattlePlanList.isEmpty()){
													//objComparator=new DeadSurviveRatioBattlePlanComparator(0.1,0.01);
													Collections.sort(objPartialBattlePlanList,objComparator);
													if (objComparator.canAcceptBattlePlan(objPartialBattlePlanList.get(0))){
														ants.issuePlan(objPartialBattlePlanList.get(0));
														JacobStraussBot.log(String.format("DefenderHill asignado a %1$s, realizando movimiento desesperado",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
													}else{
														JacobStraussBot.log(String.format("DefenderHill asignado a %1$s, movimiento desesperado descartado",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
													}
													
													
												}
											}
				
										}
									}else if (objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT)){
										List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
										if (objListOfAim.size()==1){
											Aim aim=objListOfAim.get(0);
											objListOfAim=Aim.getPerpendicular(aim);
										}else{
											List<Aim> objNewAimList=new ArrayList<Aim>();
											for (Aim objAim : objListOfAim) {
												objNewAimList.add(Aim.getOpposite(objAim));
											}
											objListOfAim=objNewAimList;
										}
										objResultMove=ants.issueMove(objMyAntTile, objListOfAim,  objComparator);
										if (objResultMove.isIssuedOrder()){
											JacobStraussBot.log(String.format("DefenderHill asignado a %1$s, esquivando hormigas atascadas",objMyAntTile) , objEnemyAntTile, LogLevel.INFO);
											BattlePlan objBattlePlan=objResultMove.getBattlePlanIssued();
											if (objBattlePlan!=null){
												CombatActions.computeBattlePlan(objBattlePlan);
											}else{
												CombatActions.addAntsAssignedToObjetive(objEnemyAntTile, objResultMove.getNumAntsAssigned());
											}

										}
									}
								}
								objResultado=new ResultadoAccion(true, 0);

							}
						}
					}

				}else{
					objResultado=new ResultadoAccion(false, iCurrentPriority);
				}

			}
		}else{
			objResultado=new ResultadoAccion(true, 0);
		}


		return objResultado;


	}

	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		List<AccionPrioridad> objResult=new ArrayList<AccionPrioridad>();
		if (ants.getMyHills().size()>0){
		for (Tile objEnemyTile : ants.getEnemyAnts()) {
			
			for (Tile objMyHill : ants.getMyHills()) {
				int iDistanceToHill=ants.getManhattanDistance(objMyHill, objEnemyTile);
				if (iDistanceToHill<=JacobStraussBot.DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL){
					List<Tile> objMyAntsList=ants.queryTilesMyAnts(objEnemyTile, JacobStraussBot.DEFEND_HILL_NUM_ANTS_SEARCH);
					DefendHill objAccion=new DefendHill(objEnemyTile, objMyHill,objMyAntsList,JacobStraussBot.DEFEND_HILL_MAX_ANTS_ASSIGNED, ants);
					for (Entry<Tile, TileStatus> objEntry : objAccion.getTileStatusMap().entrySet()) {
						TileStatus objStatus=objEntry.getValue();
						Tile objMyAnt=objEntry.getKey();
						int iPrioridad=objAccion.getPriority(objAccion.iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy());
						objResult.add(new AccionPrioridad(iPrioridad, objMyAnt, objAccion));
					}
				}
						
			}
			
			
		}
		}
		return objResult;
	}
	
	protected   int getPriority(int iDistanciaEnemyToHill,int iDistanciaMyAntToHill,int iDistanciaMyAntToEnemy){
		int iPrioridad=JacobStraussBot.DEFEND_HILL_INITIAL_PRIORITY;
		iPrioridad+=iDistanciaEnemyToHill*JacobStraussBot.DEFEND_HILL_MODIFIER_ENEMY_DISTANCE;
		iPrioridad+=iDistanciaMyAntToHill*JacobStraussBot.DEFEND_HILL_MODIFIER_MYANT_TO_HILL_DISTANCE;
		iPrioridad+=iDistanciaMyAntToEnemy*JacobStraussBot.DEFEND_HILL_MODIFIER_MYANT_TO_ENEMY_DISTANCE;
		iPrioridad+=CombatActions.getNumAntsAssigned(objEnemyAntTile)*JacobStraussBot.DEFEND_HILL_MODIFIER_ANTS_ASSIGNED;
		return iPrioridad;
	}
	
	
	private  int getDistanciaEnemyToHill(int iPrioridad,TileStatus objStatus){
		int iDistancia=iPrioridad-getPriority(0, objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy());
		iDistancia=iDistancia/JacobStraussBot.DEFEND_HILL_MODIFIER_ENEMY_DISTANCE;
		return iDistancia+1;
	}
	
	private  int getDistanciaMyAntToHill(int iPrioridad,TileStatus objStatus){
		int iDistancia=iPrioridad-getPriority(iEstimatedDistEnemyToHill, 0, objStatus.getEstimatedDistToEnemy());
		iDistancia=iDistancia/JacobStraussBot.DEFEND_HILL_MODIFIER_MYANT_TO_HILL_DISTANCE;
		return iDistancia+1;
	}
	private  int getDistanciaMyAntToEnemy(int iPrioridad,TileStatus objStatus){
		int iDistancia=iPrioridad-getPriority(iEstimatedDistEnemyToHill, objStatus.getEstimatedDistToHill(), 0);
		iDistancia=iDistancia/JacobStraussBot.DEFEND_HILL_MODIFIER_MYANT_TO_ENEMY_DISTANCE;
		return iDistancia+1;
	}
	
	public class TileStatus{
		Tile objTile;
		Path objPathToHill=null;
		Path objPathToEnemy=null;
		int iEstimatedDistToEnemy;
		int iEstimatedDistToHill;
		/**
		 * @param objTile
		 */
		public TileStatus(Tile objTile,Ants ants) {
			this.objTile = objTile;
			iEstimatedDistToEnemy=ants.getManhattanDistance(objTile, objEnemyAntTile);
			iEstimatedDistToHill=ants.getManhattanDistance(objTile, objMyHillTile);
		}
		public Tile getTile() {
			return objTile;
		}
		public Path getPathToHill() {
			return objPathToHill;
		}
		public void setPathToHill(Path objPathToHill) {
			this.objPathToHill = objPathToHill;
		}
		public Path getPathToEnemy() {
			return objPathToEnemy;
		}
		public void setPathToEnemy(Path objPathToEnemy) {
			this.objPathToEnemy = objPathToEnemy;
		}
		public int getEstimatedDistToEnemy() {
			return iEstimatedDistToEnemy;
		}
		public void setEstimatedDistToEnemy(int objEstimatedDistToEnemy) {
			iEstimatedDistToEnemy = objEstimatedDistToEnemy;
		}
		public int getEstimatedDistToHill() {
			return iEstimatedDistToHill;
		}
		public void setEstimatedDistToHill(int objEstimatedDistToHill) {
			iEstimatedDistToHill = objEstimatedDistToHill;
		}
		
		
	}
	@Override
	public ResultadoAccion antIssuedOrder(Tile objMyAnt,int prioridad,int iSigPrioridad,Ants ants) {
		this.objTileStatusMap.remove(objMyAnt);
		return new ResultadoAccion(true, 0);
		
	}


	protected Map<Tile, TileStatus> getTileStatusMap() {
		return objTileStatusMap;
	}
	
	private double adjustRisk(Ants ants, int iEstimatedDistEnemyToHill){
		double dMinRatio=1;
		if (iEstimatedDistEnemyToHill<=5){
			double[] aMinRatio=new double[]{0,0.1,0.5,0.5,0.5,0.5};
			dMinRatio=aMinRatio[iEstimatedDistEnemyToHill];
		}else if (iEstimatedDistEnemyToHill<=10){
			dMinRatio=0.6;
		}else{
			boolean bHasHills=!ants.getMyHills().isEmpty();
			int iNumOfAnts=ants.getMyAnts().size();

			if (!bHasHills){
				dMinRatio=1;
			}else{
				if (iNumOfAnts<=10){
					dMinRatio=0.75;
				}else if (iNumOfAnts<=30){
					dMinRatio=0.75;
				}else if (iNumOfAnts<=50){
					dMinRatio=0.75;
				}else if (iNumOfAnts<=80){
					dMinRatio=0.75;
				}
			}
		}

		return dMinRatio;
	}


	@Override
	public String toString() {
		return String.format("DefenderHill [objEnemyAntTile=%s, objMyHillTile=%s, iAntsAsigned=%s]", objEnemyAntTile, objMyHillTile, CombatActions.getNumAntsAssigned(objEnemyAntTile));
	}


	public int getEstimatedDistEnemyToHill() {
		return iEstimatedDistEnemyToHill;
	}
}
