package es.cccf.ants.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;
import org.aichallenge.ants.TileTemplate;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.battleplan.BattlePlan;

public class CombatActions {
	private static Map<Tile, Integer> objCurrentAntsAssigned=new HashMap<Tile, Integer>();
	
	public static void addAntsAssignedToObjetive(Tile objObjetive,int iNumAntsAssigned){
		int iResult=iNumAntsAssigned;
		if (objCurrentAntsAssigned.containsKey(objObjetive)){
			iResult+=objCurrentAntsAssigned.get(objObjetive);
		}
		objCurrentAntsAssigned.put(objObjetive, iResult);
	}
	
	public static boolean isObjetiveComplete(Tile objObtetive){
		boolean bResult=false;
		if (objCurrentAntsAssigned.containsKey(objObtetive)){
			int iNumAntsAssigned=objCurrentAntsAssigned.get(objObtetive);
			if (iNumAntsAssigned>=3){
				bResult=true;
			}
		}
		return bResult;
	}
	
	public static int getNumAntsAssigned(Tile objObtetive){
		int iNumAntsAssigned=0;
		if (objCurrentAntsAssigned.containsKey(objObtetive)){
			 iNumAntsAssigned=objCurrentAntsAssigned.get(objObtetive);

		}
		return iNumAntsAssigned;
	}

	
	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		objCurrentAntsAssigned.clear();
		List<AccionPrioridad> objResult=new ArrayList<AccionPrioridad>();
		TileTemplate objAttackTemplate=ants.getAttackTemplate();
		for (Tile objEnemyTile : ants.getEnemyAnts()) {
			boolean bAddedForDefend=false;
			for (Tile objMyHill : ants.getMyHills()) {
				int iDistanceToHill=ants.getManhattanDistance(objMyHill, objEnemyTile);
				if (iDistanceToHill<=JacobStraussBot.DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL){
					bAddedForDefend=true;
					List<Tile> objMyAntsList=ants.queryTilesMyAnts(objEnemyTile, JacobStraussBot.DEFEND_HILL_NUM_ANTS_SEARCH);
					DefendHill objAccion=new DefendHill(objEnemyTile, objMyHill,objMyAntsList,JacobStraussBot.DEFEND_HILL_MAX_ANTS_ASSIGNED, ants);
					for (Entry<Tile, DefendHill.TileStatus> objEntry : objAccion.getTileStatusMap().entrySet()) {
						DefendHill.TileStatus objStatus=objEntry.getValue();
						Tile objMyAnt=objEntry.getKey();
						int iPrioridad=objAccion.getPriority(objAccion.getEstimatedDistEnemyToHill(), objStatus.getEstimatedDistToHill(), objStatus.getEstimatedDistToEnemy());
						objResult.add(new AccionPrioridad(iPrioridad, objMyAnt, objAccion));
					}
				}
						
			}
			
			if (!bAddedForDefend){
			List<Tile> objMyAntNearestList=ants.queryTilesMyAnts(objEnemyTile, JacobStraussBot.ATTACK_ANT_NUM_ANTS_SEARCH);

			if (!objMyAntNearestList.isEmpty()){
				Tile objClosestMyAnt=objMyAntNearestList.get(0);
				int iDistanceToCombat=objAttackTemplate.getMahattanDistanceTemplate(objEnemyTile, objClosestMyAnt, ants)-1;
				AttackAnt objAccion=new AttackAnt(objEnemyTile, objMyAntNearestList, iDistanceToCombat, ants);
				for (Entry<Tile, es.cccf.ants.actions.AttackAnt.TileStatus> objEntry : objAccion.getTileStatusMap().entrySet()) {
					AttackAnt.TileStatus objStatus=objEntry.getValue();
					Tile objMyAnt=objEntry.getKey();
					int iDistanceToEnemy=objStatus.getEstimatedDistance();
					int iPrioridad=objAccion.getPriority(iDistanceToEnemy);
					objResult.add(new AccionPrioridad(iPrioridad, objMyAnt, objAccion));
				}
				
				
			}
			}
			
		}
		
		return objResult;
	}

	public static void computeBattlePlan(BattlePlan objBattlePlan){
		Set<Tile> objEnemyTiles=objBattlePlan.getEnemyOriginTiles();
		int iNumEnemies=objEnemyTiles.size();
		int iNumAlliesAssigned=objBattlePlan.getActionsOfPlan().size();
		int iAntsAssignedForEnemy=(int) Math.ceil(((double) iNumAlliesAssigned)/((double)iNumEnemies));
		iAntsAssignedForEnemy=Math.max(1, iAntsAssignedForEnemy);
		for (Tile objEnemyTile : objEnemyTiles) {
			addAntsAssignedToObjetive(objEnemyTile, iAntsAssignedForEnemy);
		}
		
	}
	
}
