package es.cccf.ants.actions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.AntsIndex;
import org.aichallenge.ants.BlockedMovesReasons;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.AccionPrioridad;
import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;
import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;
import es.cccf.ants.spacepartitioning.AntsExploration;
import es.cccf.ants.spacepartitioning.AntsExplorationResult;
import es.cccf.ants.spacepartitioning.Path;


public class ExploreAction implements Action {
	

	Map<Tile,TileStatus> objTileStatus;
	Tile objUnknownTile;
	boolean bUnseen;
	int iAntsAsigned;
	
	/**
	 * @param objUnknownTile TODO
	 * @param objMyAntsList
	 * @param iMedianTurn TODO
	 */
	public ExploreAction(Tile objUnknownTile, List<Tile> objMyAntsList,boolean bUnseen,Ants ants) {
		this.objTileStatus = new HashMap<Tile, ExploreAction.TileStatus>();
		for (Tile objTile : objMyAntsList) {
			TileStatus objStatus=new TileStatus(objTile);
			objStatus.setEstimatedDistance(ants.getManhattanDistance(objTile, objUnknownTile));
			this.objTileStatus.put(objTile, new TileStatus(objTile));
		}
		this.objUnknownTile=objUnknownTile;
		this.iAntsAsigned=0;
		this.bUnseen=bUnseen;
	}


	@Override
	public ResultadoAccion doAccion(Tile objMyAntTile, int objPrioridad,int iSigPrioridad, Ants ants) {
		ResultadoAccion objResultado;




		TileStatus objStatus=this.objTileStatus.get(objMyAntTile);
		if (objStatus==null){
			objResultado=new ResultadoAccion(true, 0);
		}else{
			if (objStatus.getPath()!=null){

				
				Path objPath=objStatus.getPath();
				int iCurrentPriority=getPriority(objPath.getCost());
				if (iCurrentPriority<=iSigPrioridad){
					Tile objCurrentTile=objPath.getNextStep(objMyAntTile);
//					BattlePlanComparator objComparator=new DeadSurviveRatioBattlePlanComparator(0.1,0.1);
					BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objMyAntTile, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
					ResultMove objResultMove=ants.issueMove(objMyAntTile, objCurrentTile, objComparator);

					if (objResultMove.isIssuedOrder()){
						JacobStraussBot.log(String.format("Exploracion asignado a %1$s",objMyAntTile) , objUnknownTile, LogLevel.INFO);

						iAntsAsigned++;
					}else if (objResultMove.containsReason(BlockedMovesReasons.BATTLE_PLAN_NOT_VALID)){
						if (ants.isAntInDanger(objMyAntTile)){
							//objComparator=new DeadSurviveRatioBattlePlanComparator(0.1,0.01);
							List<Aim> objListOfAim=ants.getDirections(objMyAntTile, objCurrentTile);
							if (objListOfAim.size()==1){
								Aim aim=objListOfAim.get(0);
								objListOfAim=Aim.getPerpendicular(objListOfAim.get(0));
								objListOfAim.add(Aim.getOpposite(aim));
							}else{
								List<Aim> objNewAimList=new ArrayList<Aim>();
								for (Aim objAim : objListOfAim) {
									objNewAimList.add(Aim.getOpposite(objAim));
								}
								objListOfAim=objNewAimList;
							}
							objResultMove=ants.issueMove(objMyAntTile, objListOfAim,true,  objComparator);
							if (objResultMove.isIssuedOrder()){
								JacobStraussBot.log(String.format("Exploracion asignado a %1$s, huyendo",objMyAntTile) , objUnknownTile, LogLevel.INFO);
							}
						}
					}
					objResultado=new ResultadoAccion(true, 0);
				}else{
					objResultado=new ResultadoAccion(false,iCurrentPriority);
				}
			}else{
				int iCurrentPriority=getPriority(objStatus.getEstimatedDistance());
				if (iCurrentPriority<=iSigPrioridad){
					int iDistancia=getDistancia(iSigPrioridad);
					Path objTempPath=ants.getPath(objMyAntTile, objUnknownTile,iDistancia);
					if (objTempPath==null){
						objResultado=new ResultadoAccion(true, 0);
					}else{
						objStatus.setEstimatedDistance(objTempPath.getCost());
						if (objTempPath.isComplete()){
							objStatus.setPath(objTempPath);
						}
						objResultado=new ResultadoAccion(false, getPriority(objTempPath.getCost()));
					}
				}else{
					objResultado=new ResultadoAccion(false,iCurrentPriority);
				}
			}
		}


		return objResultado;
	}

	
	public static List<AccionPrioridad> getListaAcciones(Ants ants){
		
		if (ants.getCurrentTurn()==391){
			System.err.println("TEST");
		}
		
		
		List<AccionPrioridad> objResult=new ArrayList<AccionPrioridad>();
		AntsIndex objIndex=new AntsIndex(ants);
		AntsExploration objExploration=ants.getAntsExploration();
		AntsExplorationResult objExplorationResult=objExploration.processExploration();
		for (Tile objMyAnt : objExplorationResult.getListAntsInPopulatedArea()) {
			objIndex.addTile(objMyAnt);
		}
		for (Tile objDesertedTile : objExplorationResult.getListDestinationDeserted()) {
			List<Tile> objCandidateAnts=objIndex.queryTiles(objDesertedTile, JacobStraussBot.EXPLORE_NUM_ANTS_SEARCH);
			ExploreAction objAccion=new ExploreAction(objDesertedTile, objCandidateAnts, false,ants);
			for (Tile objMyAntTile : objCandidateAnts) {
				int iDistance=ants.getManhattanDistance(objDesertedTile, objMyAntTile);
				AccionPrioridad objAccionPrioridad=new AccionPrioridad(objAccion.getPriority(iDistance), objMyAntTile, objAccion);
				objResult.add(objAccionPrioridad);
			}
		}
		
		for (Tile objUnseenTile:objExplorationResult.getListDestinationUnseen()){
			List<Tile> objCandidateAnts=ants.queryTilesMyAnts(objUnseenTile, JacobStraussBot.EXPLORE_NUM_ANTS_SEARCH);
			ExploreAction objAccion=new ExploreAction(objUnseenTile, objCandidateAnts, true,ants);
			for (Tile objMyAntTile : objCandidateAnts) {
				int iDistance=ants.getManhattanDistance(objUnseenTile, objMyAntTile);
				AccionPrioridad objAccionPrioridad=new AccionPrioridad(objAccion.getPriority(iDistance), objMyAntTile, objAccion);
				objResult.add(objAccionPrioridad);
			}

		}
		
		
		
		
		return objResult;
	}
	
	private  int getPriority(int iDistancia){
		int iPrioridad=JacobStraussBot.EXPLORE_INITIAL_PRIORITY;
		iPrioridad+=iDistancia*JacobStraussBot.EXPLORE_MODIFIER;
		if (!bUnseen){
			iPrioridad+=JacobStraussBot.EXPLORE_NOT_UNSEEN_MODIFIER;
		}
		iPrioridad+=JacobStraussBot.EXPLORE_ASSIGNED_MODIFIER*iAntsAsigned;
		return iPrioridad;
	}
	private int getDistancia(int iPrioridad){
		int iPrioridadCero=getPriority(0);
		int iDistancia=iPrioridad-iPrioridadCero;
		iDistancia=iDistancia/JacobStraussBot.EXPLORE_MODIFIER;
		return iDistancia+1;
	}

	private class TileStatus{
		Tile objTile;
		Path objPath=null;
		int iEstimatedDistance;
		/**
		 * @param objTile
		 */
		public TileStatus(Tile objTile) {
			this.objTile = objTile;
		}
		public Path getPath() {
			return objPath;
		}
		public void setPath(Path objPath) {
			this.objPath = objPath;
		}
		public Tile getTile() {
			return objTile;
		}
		public int getEstimatedDistance() {
			return iEstimatedDistance;
		}
		public void setEstimatedDistance(int objEstimatedDistance) {
			iEstimatedDistance = objEstimatedDistance;
		}
		
	}
	@Override
	public ResultadoAccion antIssuedOrder(Tile objMyAnt,int prioridad,int iSigPrioridad,Ants ants){
		this.objTileStatus.remove(objMyAnt);
		return new ResultadoAccion(true, 0);
	}


	@Override
	public String toString() {
		return String.format("ExploracionAccion [objUnknownTile=%s, bUnseen=%s, iAntsAsigned=%s]", objUnknownTile, bUnseen, iAntsAsigned);
	}
}
