package es.cccf.ants.battleplan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Ilk;
import org.aichallenge.ants.Tile;
import org.aichallenge.ants.TileTemplate;

import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.spacepartitioning.TileAimKey;

public class BattlePlanGeneral {
	private Ants ants;

	
	private Map<Tile, TileThreatInfo> objMapThreatInfo;
	private Map<TileAimKey, CacheState> objCache;
	private Set<Tile> objAntProcesed;
	
	/**
	 * @param objAnts
	 */
	public BattlePlanGeneral(Ants objAnts) {
		ants = objAnts;
		objMapThreatInfo=new HashMap<Tile, TileThreatInfo>();
		objAntProcesed=new HashSet<Tile>();
		this.objCache=new HashMap<TileAimKey, CacheState>();
		
	}
	

	public void processBattleInfo(){
		if (ants.getCurrentTurn()==1){
			System.err.println("TEST");
		}
		TileTemplate objAttackTemplate=ants.getAttackTemplate();
		for (Tile objEnemyAnt : ants.getEnemyAnts()) {
			List<Tile> objMyAntNearestList=ants.queryTilesMyAnts(objEnemyAnt, 50);
			if (ants.getCurrentTurn()==1&&objEnemyAnt.getRow()==82&&objEnemyAnt.getCol()==16){
				System.err.println("TEST");
			}

			if (!objMyAntNearestList.isEmpty()){
				Tile objClosestMyAnt=objMyAntNearestList.get(0);
				if (objAttackTemplate.getMahattanDistanceTemplate(objEnemyAnt, objClosestMyAnt, ants)<=2){
					processBattleInfoForAnt(objEnemyAnt,true);
				}
				
				for (Tile objMyAnt : objMyAntNearestList) {
					if (objAttackTemplate.getMahattanDistanceTemplate(objEnemyAnt, objMyAnt, ants)<=2){
						processBattleInfoForAnt(objMyAnt, false);
					}
				}
			}
			
		}
	}

	
	public List<BattlePlan> computeBattlePlanList(TileAimKey objTileAimKey, BattlePlanComparator objComparator) {
		List<BattlePlan> objResult;
		List<BattlePlan> objQueueCurrentLevel;
		List<BattlePlan> objQueueNextLevel;
		Set<Set<TileAimKey>> objTileDestConfigExplored;

		CacheState objCacheState;
		if (objCache.containsKey(objTileAimKey)) {
			objCacheState = objCache.get(objTileAimKey);
			objResult = objCacheState.getResultMoves();
			objQueueCurrentLevel = objCacheState.getPendingPlansCurrentLevel();
			objTileDestConfigExplored = objCacheState.getTileDestConfigExplored();
			objQueueNextLevel = objCacheState.objPendingPlansNextLevel;

		} else {
			objCacheState = new CacheState();
			objResult = objCacheState.getResultMoves();
			objQueueCurrentLevel = objCacheState.getPendingPlansCurrentLevel();
			objTileDestConfigExplored = objCacheState.getTileDestConfigExplored();
			objQueueNextLevel = objCacheState.objPendingPlansNextLevel;
			Set<TileAimKey> objFirstSet = new HashSet<TileAimKey>();
			objFirstSet.add(objTileAimKey);
			BattlePlan objFirstBattlePlan = new BattlePlan(objFirstSet, this);
			if (objFirstBattlePlan.isComplete()) {
				objResult.add(objFirstBattlePlan);
			} else {
				if (objFirstBattlePlan.isObligatoryImproved()) {
					List<BattlePlan> objObligatoryBattlePlans = objFirstBattlePlan.getImprovedBattlePlans();
					for (BattlePlan objBattlePlan : objObligatoryBattlePlans) {
						if (!objBattlePlan.isIncompatible()) {
							if (!objBattlePlan.isObligatoryImproved()) {
								objQueueCurrentLevel.add(objBattlePlan);
							}
						}
					}
					if (objQueueCurrentLevel.size() > 0) {
						Collections.sort(objQueueCurrentLevel, objComparator);
						objCacheState.setBestCurrentLevel(objQueueCurrentLevel.get(0));
					}

				} else {
					objQueueCurrentLevel.add(objFirstBattlePlan);
					objCacheState.setBestCurrentLevel(objFirstBattlePlan);
				}
			}
		}

			while (!objQueueCurrentLevel.isEmpty()) {
				BattlePlan objBattlePlan = objQueueCurrentLevel.remove(0);
				if (!objTileDestConfigExplored.contains(objBattlePlan.getActionsOfPlan())) {
					objTileDestConfigExplored.add(objBattlePlan.getActionsOfPlan());
					if (!objBattlePlan.isIncompatible()) {
						if (objBattlePlan.isComplete()) {
							objResult.add(objBattlePlan);
						} else {
							if (objBattlePlan.canBeImproved()) {
								boolean bImproved = false;
								List<BattlePlan> objImprovements = objBattlePlan.getImprovedBattlePlans();
								List<BattlePlan> objObligatoryImprovements = new LinkedList<BattlePlan>();
								boolean bHasNoObligatoryImprovements = true;
								for (BattlePlan objBattlePlanImproved : objImprovements) {
									if (!objBattlePlanImproved.isIncompatible()) {
										if (objBattlePlanImproved.isObligatoryImproved()) {
											objObligatoryImprovements.add(objBattlePlanImproved);
										} else {
											bHasNoObligatoryImprovements = false;
											int iComparation = objComparator.compare(objBattlePlanImproved, objBattlePlan);
											if (iComparation == -1) {
												boolean bPartialImproved = addPlanToNextLevel(objBattlePlanImproved, objCacheState, objComparator);
												if (!bImproved && bPartialImproved) {
													bImproved = bPartialImproved;
												}
											}
										}

									}
								}
								if (bHasNoObligatoryImprovements) {
									for (BattlePlan objObligatory : objObligatoryImprovements) {
										List<BattlePlan> objObligatoryDerivedImprovements = objObligatory.getImprovedBattlePlans();
										for (BattlePlan objBattlePlanObligatory : objObligatoryDerivedImprovements) {
											if (!objBattlePlanObligatory.isIncompatible()) {
												if (!objBattlePlanObligatory.isObligatoryImproved()) {
													int iComparation = objComparator.compare(objBattlePlanObligatory, objBattlePlan);
													if (iComparation == -1) {
														boolean bPartialImproved = addPlanToNextLevel(objBattlePlanObligatory, objCacheState,
																objComparator);
														if (!bImproved && bPartialImproved) {
															bImproved = bPartialImproved;
														}

													}
												}

											}
										}
									}

								}
								if (!bImproved) {
									objResult.add(objBattlePlan);
								}
							} else {
								objResult.add(objBattlePlan);
							}
						}
					}
				}
				if (objQueueCurrentLevel.isEmpty()) {
					objQueueCurrentLevel.addAll(objQueueNextLevel);
					objQueueNextLevel.clear();
					if (objQueueCurrentLevel.size() > 0) {
						Collections.sort(objQueueCurrentLevel, objComparator);
						objCacheState.setBestCurrentLevel(objQueueCurrentLevel.get(0));
					}

				}
			}
		
		return objResult;
	}

	private boolean addPlanToNextLevel(BattlePlan objBattlePlan, CacheState objCacheState, BattlePlanComparator objComparator){
		if (objCacheState.getBestCurrentLevel()==null){
			objCacheState.getPendingPlansNextLevel().add(objBattlePlan);
			return true;
		}else{
			int iComparacion=objComparator.compare(objBattlePlan, objCacheState.getBestCurrentLevel());
			if (iComparacion==-1){
				objCacheState.getPendingPlansNextLevel().add(objBattlePlan);
				return true;
			}else{
				return false;
			}
		}
	}
	
	private List<BattlePlan> getAllObligatoryImprovedPlans(BattlePlan objPlan){
		List<BattlePlan> objTempList=new LinkedList<BattlePlan>();
		List<BattlePlan> objResult=new LinkedList<BattlePlan>();
		objTempList.add(objPlan);
		while (!objTempList.isEmpty()){
			BattlePlan objCurrent=objTempList.remove(0);
			if (objCurrent.isObligatoryImproved()){
				objTempList.addAll(objCurrent.getImprovedBattlePlans());
			}else{
				objResult.add(objCurrent);
			}
		}
		return objResult;
	}

	private void processBattleInfoForAnt(Tile objAnt, boolean bEnemy) {

		TileTemplate objAttackTemplate = ants.getAttackTemplate();

		if (!this.objAntProcesed.contains(objAnt)){
			this.objAntProcesed.add(objAnt);
			for (int i = 0; i < Aim.values().length + 1; i++) {
				Aim aim = null;
				if (i < Aim.values().length) {
					aim = Aim.values()[i];
				}
				TileAimKey objCurrentTileAimKey = ants.getTileAimKey(objAnt, aim);
				boolean bCanMove=true;
				if (aim!=null){
					if (!ants.getTileInformation(objCurrentTileAimKey.getTileDest()).isPassable()){
						bCanMove=false;
					}
					if (bCanMove){
						if (bEnemy){
							if (ants.getTileInformation(objCurrentTileAimKey.getTileDest()).getType()==Ilk.MY_ANT){
								bCanMove=false;
							}
						}else{
							if (ants.getTileInformation(objCurrentTileAimKey.getTileDest()).getType()==Ilk.ENEMY_ANT){
								bCanMove=false;
							}
							if (ants.getTileInformation(objCurrentTileAimKey.getTileDest()).isEnemyAdjacent()){
								bCanMove=false;
							}
						}
					}
				}
				if (bCanMove) {
					Set<Tile> objAffectedTiles = objAttackTemplate.getTilesInRange(objCurrentTileAimKey.getTileDest(), ants);
					for (Tile objTile : objAffectedTiles) {
						TileThreatInfo objTileCurrentBattleInfo;
						if (!objMapThreatInfo.containsKey(objTile)) {
							objTileCurrentBattleInfo = new TileThreatInfo(objTile,this);
							objMapThreatInfo.put(objTile, objTileCurrentBattleInfo);
						} else {
							objTileCurrentBattleInfo = objMapThreatInfo.get(objTile);
						}
	
						objTileCurrentBattleInfo.addTileAimKey(objCurrentTileAimKey, bEnemy);
					}
				}
	
			}
		}

	}
	
	
	public static void main(String[] args) {
		Ants ants=new Ants(3000, 500, 100, 100, 1000, 77, 5, 1, 42);
        ants.setTurnStartTime(System.currentTimeMillis());
        ants.clearMyAnts();
        ants.clearEnemyAnts();
//        ants.clearMyHills();
//        ants.clearEnemyHills();
//        ants.getFoodTiles().clear();
        ants.clearOrders();
        ants.clearVisibleTiles();
        
        ants.clearMap();

        
//		ants.update(Ilk.ENEMY_ANT, new Tile (1,5), 1);
//		ants.update(Ilk.ENEMY_ANT, new Tile (2,6), 1);
//		ants.update(Ilk.ENEMY_ANT, new Tile (3,7), 1);
//		ants.update(Ilk.MY_ANT, new Tile (3,3), 0);
//		ants.update(Ilk.MY_ANT, new Tile (4,4), 0);
//		ants.update(Ilk.MY_ANT, new Tile (5,5), 0);
		
		ants.update(Ilk.ENEMY_ANT, new Tile (1,2), 1);
		ants.update(Ilk.ENEMY_ANT, new Tile (1,3), 1);
		ants.update(Ilk.ENEMY_ANT, new Tile (1,4), 1);
		ants.update(Ilk.MY_ANT, new Tile (4,2), 0);
		ants.update(Ilk.MY_ANT, new Tile (4,3), 0);
		ants.update(Ilk.MY_ANT, new Tile (4,4), 0);

        
    	ants.updateVisibleTiles();
    	ants.updateSpacePartitioning();

		long lInicio=System.currentTimeMillis();
		BattlePlanGeneral objBattlePlanGeneral=new BattlePlanGeneral(ants);
		objBattlePlanGeneral.processBattleInfo();
		long lFin=System.currentTimeMillis();
		System.out.println("COMPLETADO "+(lFin-lInicio));
		
		for (int iRow=1;iRow<=4;iRow++){
			for (int iCol=2;iCol<=4;iCol++){
				Tile objTile=new Tile(iRow,iCol);
				TileThreatInfo objThreadInfo=objBattlePlanGeneral.getThreatInfo(objTile);
				TacticValoration objValorationPlayer=objThreadInfo.getTacticValorationForPlayer();
				TacticValoration objValorationEnemy=objThreadInfo.getTacticValorationForEnemy();
				System.out.println(String.format("Tile %s: Valoracion player: %s",objTile, objValorationPlayer));
				System.out.println(String.format("Tile %s: Valoracion enemy: %s",objTile, objValorationEnemy));
			}
			
		}
		
		lInicio=System.currentTimeMillis();
		BattlePlanComparator objComparator=new ZoneBattlePlanComparator(new Tile(4,4),JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE,ants);
		TileAimKey objTileAimKey=ants.getTileAimKey(new Tile(4,4), Aim.EAST);
		List<BattlePlan>objResult= new ArrayList<BattlePlan>(objBattlePlanGeneral.computeBattlePlanList(objTileAimKey,objComparator));
		lFin=System.currentTimeMillis();
		Collections.sort(objResult,objComparator);
		if (objResult.size()>0){
			BattlePlan objBattlePlan=objResult.get(1);
		}
//		Set<TileAimKey> objTest=new HashSet<TileAimKey>();
//		objTest.add(ants.getTileAimKey(new Tile(4,2), Aim.NORTH));
//		objTest.add(ants.getTileAimKey(new Tile(4,3), Aim.NORTH));
//		objTest.add(ants.getTileAimKey(new Tile(4,4), Aim.NORTH));
//		BattlePlan objTestPlan=new BattlePlan(objTest, objBattlePlanGeneral);
//		objTestPlan.calculateEnemyScenarios();
		System.out.println("COMPLETADO "+(lFin-lInicio));
		System.out.println(objResult.toString());
		
		lInicio=System.currentTimeMillis();
		objTileAimKey=ants.getTileAimKey(new Tile(4,3), Aim.EAST);
		objResult= new ArrayList<BattlePlan>(objBattlePlanGeneral.computeBattlePlanList(objTileAimKey,objComparator));
		lFin=System.currentTimeMillis();
		Collections.sort(objResult,objComparator);
		if (objResult.size()>=2){
			BattlePlan objBattlePlan=objResult.get(0);
			objBattlePlan.getKillRatio();
			objBattlePlan.getSurviveRatioComplete();
		}

		System.out.println("COMPLETADO "+(lFin-lInicio));
		System.out.println(objResult.toString());

		
	}


	public boolean containsThreatInfo(Tile objArg0) {
		return objMapThreatInfo.containsKey(objArg0);
	}


	public TileThreatInfo getThreatInfo(Tile objArg0) {
		return objMapThreatInfo.get(objArg0);
	}


	public void clearInfo() {
		
		this.objCache.clear();
		this.objMapThreatInfo.clear();
		this.objAntProcesed.clear();
		
	}


	public boolean isAffectedTile(Tile objTile) {
		boolean bAffected=false;
		if (objMapThreatInfo.containsKey(objTile)){
			TileThreatInfo objThreatInfo=objMapThreatInfo.get(objTile);
			bAffected=!objThreatInfo.getTileThreatEnemyTileAimKey().isEmpty();
		}
		return bAffected;
	}


	public Ants getAnts() {
		return ants;
	}


	public TacticValoration getTacticValoration(TileThreatInfo objThreatInfo, boolean bEnemy) {
		TacticValoration objResult=TacticValoration.LIVE;
		int iDebilityForTile=objThreatInfo.getDebility(bEnemy);
		Set<Tile> objEnemyTiles;
		if (bEnemy){
			objEnemyTiles=objThreatInfo.getTileThreatEnemyTiles();
		}else{
			objEnemyTiles=objThreatInfo.getTileThreatPlayerTiles();
		}
		
		Iterator<Tile> it=objEnemyTiles.iterator();
		while(it.hasNext()&&!objResult.equals(TacticValoration.DEAD)){
			Tile objCurrentTile=it.next();
			TileThreatInfo objCurrentThreatInfo=this.getThreatInfo(objCurrentTile);
			int iCurrentDebility=objCurrentThreatInfo.getDebility(!bEnemy);
			if (iCurrentDebility<iDebilityForTile){
				objResult=TacticValoration.DEAD;
			}else if (iCurrentDebility==iDebilityForTile){
				objResult=TacticValoration.KILL;
			}
			
		}
		return objResult;
	
		
	}
	
}

class CacheState{
	
	List<BattlePlan> objResultMoves=new LinkedList<BattlePlan>();
	List<BattlePlan> objPendingPlansCurrentLevel=new LinkedList<BattlePlan>();
	List<BattlePlan> objPendingPlansNextLevel=new LinkedList<BattlePlan>();
	Set<Set<TileAimKey>> objTileDestConfigExplored=new HashSet<Set<TileAimKey>>();
	private BattlePlan objBestCurrentLevel=null;
	public List<BattlePlan> getResultMoves() {
		return objResultMoves;
	}
	public List<BattlePlan> getPendingPlansCurrentLevel() {
		return objPendingPlansCurrentLevel;
	}
	public Set<Set<TileAimKey>> getTileDestConfigExplored() {
		return objTileDestConfigExplored;
	}
	public List<BattlePlan> getPendingPlansNextLevel() {
		return objPendingPlansNextLevel;
	}
	public BattlePlan getBestCurrentLevel() {
		return objBestCurrentLevel;
	}
	public void setBestCurrentLevel(BattlePlan objBestCurrentLevel) {
		this.objBestCurrentLevel = objBestCurrentLevel;
	}
	
	
	
}
