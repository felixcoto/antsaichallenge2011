package es.cccf.ants.battleplan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

import es.cccf.ants.spacepartitioning.Room;
import es.cccf.ants.spacepartitioning.RoomInfoPartition;
import es.cccf.ants.spacepartitioning.RoomInstanceNode;

public class ZoneBattlePlanComparator implements BattlePlanComparator{
	Set<Tile> objMyAntTiles;
	Set<Tile> objEnemyAntTiles;
	Ants ants;
	int iDistancia;
	Tile objTileStart;
	double dValoration;
	boolean bMyHillNear=false;
	int iMyHillDistance=0;	
	/**
	 * @param objDistancia
	 * @param objAnts
	 */
	public ZoneBattlePlanComparator(Tile objTileStart,int objDistancia, Ants objAnts) {
		iDistancia = objDistancia;
		ants = objAnts;
		this.objTileStart=objTileStart;
		this.objMyAntTiles=new HashSet<Tile>();
		this.objEnemyAntTiles=new HashSet<Tile>();
		processAnts();
	}
	private void processAnts() {
		List<RoomInstanceNode> objListRooms=ants.getRoomsAtCost(objTileStart, iDistancia);
		for (RoomInstanceNode objRoomInstanceNode : objListRooms) {
			Room<RoomInfoPartition> objRoom=objRoomInstanceNode.getRoom();
			RoomInfoPartition objInfo=objRoom.getLoad();
			for (Tile objMyAnt : objInfo.getMyAnts()) {
				int iDistance=objRoomInstanceNode.getCost();
				iDistance+=ants.getManhattanDistance(objMyAnt, objRoomInstanceNode.getTile());
				if (iDistance<=this.iDistancia){
					objMyAntTiles.add(objMyAnt);
				}
			}
			
			for (Tile objEnemyAnt : objInfo.getEnemyAnts()) {
				int iDistance=objRoomInstanceNode.getCost();
				iDistance+=ants.getManhattanDistance(objEnemyAnt, objRoomInstanceNode.getTile());
				if (iDistance<=this.iDistancia){
					objEnemyAntTiles.add(objEnemyAnt);
				}
			}
			
			for (Tile objMyHill : objInfo.getMyHills()) {
				int iDistance=objRoomInstanceNode.getCost();
				iDistance+=ants.getManhattanDistance(objMyHill, objRoomInstanceNode.getTile());
				if (iDistance<=this.iDistancia){
					if (bMyHillNear){
						if (iDistance<iMyHillDistance){
							iMyHillDistance=iDistance;
						}else{
							bMyHillNear=true;
							iMyHillDistance=iDistance;
						}
					}
				}
			}



		}
		dValoration=getValoration(objMyAntTiles.size(), objEnemyAntTiles.size());
		
	}

	private double getValoration(int iNumMyAnts,int iEnemyAnts){
		double dResult=0;
		int iNumAnts=iNumMyAnts+iEnemyAnts;
		if (iNumAnts>0){
			dResult=((double)iNumMyAnts)/((double)iNumAnts);
		}
		return dResult;
		
	}
	@Override
	public int compare(BattlePlan objO1, BattlePlan objO2) {
		int iAlliesLeft1=objMyAntTiles.size()- objO1.getAllyDeadAtMinDeadRatio();
		int iEnemyLeft1=objEnemyAntTiles.size()-objO1.getEnemyDeadAtMinDeadRatio();
		double dValoration1=getValoration(iAlliesLeft1, iEnemyLeft1);
		int iAlliesLeft2=objMyAntTiles.size()- objO2.getAllyDeadAtMinDeadRatio();
		int iEnemyLeft2=objEnemyAntTiles.size()-objO2.getEnemyDeadAtMinDeadRatio();
		double dValoration2=getValoration(iAlliesLeft2, iEnemyLeft2);
		if (dValoration1>dValoration2){
			return -1;
		}else if (dValoration2>dValoration1){
			return 1;
		}else{
			double dMinDeadRatio1=objO1.getMinDeadRatio();
			double dMinDeadRatio2=objO2.getMinDeadRatio();
			double dMinSurviveRatio1=objO1.getMinSurviveRatio();
			double dMinSurviveRatio2=objO2.getMinSurviveRatio();
			
			
			
			if (dMinDeadRatio1>dMinDeadRatio2){
				return -1;
			}else if (dMinDeadRatio1<dMinDeadRatio2){
				return 1;
			}else{
				if (dMinSurviveRatio1>dMinSurviveRatio2){
					return -1;
				}else{
					if (dMinSurviveRatio1<dMinSurviveRatio2){
						return 1;
					}else{
						return 0;
					}
				}
			}

		}

		
	
	}
	@Override
	public boolean canAcceptBattlePlan(BattlePlan objBattlePlan) {
		int iAlliesLeft=objMyAntTiles.size()- objBattlePlan.getAllyDeadAtMinDeadRatio();
		int iEnemyLeft=objEnemyAntTiles.size()-objBattlePlan.getEnemyDeadAtMinDeadRatio();
		double dValorationBattlePlan=getValoration(iAlliesLeft, iEnemyLeft);
		if (objBattlePlan.getMinDeadRatio()==0){
			return false;
		}else if (bMyHillNear){
			return true;
		}else if (this.dValoration<dValorationBattlePlan){
			return true;
		}else if (this.dValoration>=0.5&&dValorationBattlePlan<0.5){
			return false;
		}else if (this.dValoration>=0.5 && dValorationBattlePlan>=0.5){
			return true;
		}else{
			return false;
		}

	}
	
	
	public boolean canAcceptBattlePlanTest(BattlePlan objBattlePlan) {
		int iAlliesLeft=objMyAntTiles.size()- objBattlePlan.getAllyDeadAtMinDeadRatio();
		int iEnemyLeft=objEnemyAntTiles.size()-objBattlePlan.getEnemyDeadAtMinDeadRatio();
		double dValorationBattlePlan=getValoration(iAlliesLeft, iEnemyLeft);
		if (objBattlePlan.getMinDeadRatio()==0){
			return false;
		}else if (bMyHillNear){
			int iLimitDistance=iDistancia/2;
			if (iMyHillDistance<=iLimitDistance){
				return true;
			}else if (objBattlePlan.getMinSurviveRatio()==0){
				return false;
			}else{
				return true;
			}
		}else if (objBattlePlan.getMinSurviveRatio()==0){
			return false;
		}else if (this.dValoration==dValorationBattlePlan){
			return false;
		}else if (this.dValoration>=0.5&&dValorationBattlePlan<0.5){
			return false;
		}else if (this.dValoration<0.5){
			return this.dValoration<dValorationBattlePlan;
		}else if (this.dValoration>=0.5 && dValorationBattlePlan>0.5){
			return true;
		}else{
			return false;
		}

	}

	
	public boolean canAcceptBattlePlanOld(BattlePlan objBattlePlan) {
		int iAlliesLeft=objMyAntTiles.size()- objBattlePlan.getAllyDeadAtMinDeadRatio();
		int iEnemyLeft=objEnemyAntTiles.size()-objBattlePlan.getEnemyDeadAtMinDeadRatio();
		double dValorationBattlePlan=getValoration(iAlliesLeft, iEnemyLeft);
		if (objBattlePlan.getMinDeadRatio()==0){
			return false;
		}else if (bMyHillNear){
			int iLimitDistance=iDistancia/2;
			if (iMyHillDistance<=iLimitDistance){
				return true;
			}else if (objBattlePlan.getMinSurviveRatio()==0||objBattlePlan.getAllyDeadAtMinDeadRatio()>=objMyAntTiles.size()/2){
				return false;
			}else{
				return true;
			}
		}else if (objBattlePlan.getMinSurviveRatio()==0||objBattlePlan.getAllyDeadAtMinDeadRatio()>=objMyAntTiles.size()/2){
			return false;
		}else if (this.dValoration==dValorationBattlePlan){
			return false;
		}else if (this.dValoration>=0.5&&dValorationBattlePlan<0.5){
			return false;
		}else if (this.dValoration<0.5){
			return this.dValoration<dValorationBattlePlan;
		}else if (this.dValoration>=0.5 && dValorationBattlePlan>0.5){
			return true;
		}else{
			return false;
		}

	}

	
	
}
