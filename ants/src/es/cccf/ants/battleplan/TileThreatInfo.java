package es.cccf.ants.battleplan;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.aichallenge.ants.Tile;

import es.cccf.ants.spacepartitioning.TileAimKey;

public class TileThreatInfo {
	private Tile objTile;
	private Map<Tile, Set<TileAimKey>> objTileThreatEnemyTileAimKey;
	private Map<Tile, Set<TileAimKey>> objTileThreatPlayerTileAimKey;
	private Set<Tile> objTileThreatEnemyTiles=new HashSet<Tile>();
	private Set<Tile> objTileThreatPlayerTiles=new HashSet<Tile>();
	private TacticValoration objTacticValorationForPlayer;
	private TacticValoration objTacticValorationForEnemy;
	private BattlePlanGeneral objGeneral;
	
	public TileThreatInfo(Tile objTile,BattlePlanGeneral objGeneral) {
		this.objTile=objTile;
		objTileThreatEnemyTileAimKey=new HashMap<Tile, Set<TileAimKey>>();
		objTileThreatPlayerTileAimKey=new HashMap<Tile, Set<TileAimKey>>();
		this.objGeneral=objGeneral;
		objTacticValorationForEnemy=null;
		objTacticValorationForPlayer=null;
	}

	public Map<Tile, Set<TileAimKey>> getTileThreatEnemyTileAimKey() {
		return objTileThreatEnemyTileAimKey;
	}

	public Map<Tile, Set<TileAimKey>> getTileThreatPlayerTileAimKey() {
		return objTileThreatPlayerTileAimKey;
	}
	
	public void addTileAimKey(TileAimKey objKey,boolean bEnemy){
		Tile objTile=objKey.getTile();
		Map<Tile, Set<TileAimKey>> objCurrentMap;
		Set<Tile> objCurrentSet;
		if (bEnemy){
			objCurrentMap=objTileThreatEnemyTileAimKey;
			objCurrentSet=objTileThreatEnemyTiles;
		}else{
			objCurrentMap=objTileThreatPlayerTileAimKey;
			objCurrentSet=objTileThreatPlayerTiles;
		}
		objCurrentSet.add(objKey.getTileDest());
		Set <TileAimKey> objCurrentAimsForTile;
		if (objCurrentMap.containsKey(objTile)){
			objCurrentAimsForTile=objCurrentMap.get(objTile);
		}else{
			objCurrentAimsForTile=new HashSet<TileAimKey>();
			objCurrentMap.put(objTile, objCurrentAimsForTile);
		}
		objCurrentAimsForTile.add(objKey);
	}

	public int getDebility(boolean bEnemy){
		if (bEnemy){
			return Math.min(objTileThreatEnemyTileAimKey.size(), objTileThreatEnemyTiles.size()) ;
		}else{
			return Math.min(objTileThreatPlayerTileAimKey.size(), objTileThreatPlayerTiles.size()) ;
		}
	}
	
	public Set<TileAimKey> getAllTileAim(boolean bEnemy){
		Map<Tile, Set<TileAimKey>> objCurrent;
		if (bEnemy){
			objCurrent=objTileThreatEnemyTileAimKey;
		}else{
			objCurrent=objTileThreatPlayerTileAimKey;
		}
		Set<TileAimKey> objResult=new HashSet<TileAimKey>();
		for (Set<TileAimKey> objSetTileAimKey : objCurrent.values()) {
			objResult.addAll(objSetTileAimKey);
		}
		return objResult;
	}
	
	
	@Override
	public String toString() {
		return String.format("TileBattleInfo [objTileThreadEnemy=%s, objTileThreadPlayer=%s]", objTileThreatEnemyTileAimKey, objTileThreatPlayerTileAimKey);
	}
	
	public TacticValoration getTacticValorationForPlayer(){
		if (this.objTacticValorationForPlayer==null){
			this.objTacticValorationForPlayer=objGeneral.getTacticValoration(this,true);
		}
		return objTacticValorationForPlayer;
	}

	public TacticValoration getTacticValorationForEnemy(){
		if (this.objTacticValorationForEnemy==null){
			this.objTacticValorationForEnemy=objGeneral.getTacticValoration(this,false);
		}
		return objTacticValorationForEnemy;
	}

	public Set<Tile> getTileThreatEnemyTiles() {
		return objTileThreatEnemyTiles;
	}

	public Set<Tile> getTileThreatPlayerTiles() {
		return objTileThreatPlayerTiles;
	}
	
	
}
