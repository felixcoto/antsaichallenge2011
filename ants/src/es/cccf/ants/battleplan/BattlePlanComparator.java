package es.cccf.ants.battleplan;

import java.util.Comparator;

public interface BattlePlanComparator extends Comparator<BattlePlan> {
	public boolean canAcceptBattlePlan(BattlePlan objBattlePlan);
}
