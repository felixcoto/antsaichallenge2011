package es.cccf.ants.battleplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.BlockedMovesReasons;
import org.aichallenge.ants.Ilk;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.Tile;

import es.cccf.ants.spacepartitioning.TileAimKey;

public class BattlePlan {
	private Map<Tile, TileAimKey> objMyAntsTileAimKeys;
	private boolean bIncompatible=false;
	private boolean bComplete=false;
	private boolean bCanBeImproved=false;
	private Set<Tile> objSrcTiles;
	private Set<Tile> objDestTiles;
	Set<TileAimKey> objMyAntsActions;
	Set<Tile> objAntsInDanger;
	private Map<Tile,TileBattleResolutionInfo> objBattleResolution;
	private Set<TileAimKey> objObligatoryImprovements;
	int iRetribution=0;
	int iSurvive=0;
	private BattlePlanGeneral objGeneral;
	private Set<Tile> objProhibitedTilesForPush;
	private Double dSurviveRatioComplete;
	private Double dSurviveRatioMagnitude;
	private Double dKillRatio;
	private Integer iNumEnemies;
	private Double dKillRatioMagnitude;
	private Double dConfirmedKillRatio;
	private Map<Tile,Set<Tile>> objEnemyDestOriginTiles=new HashMap<Tile, Set<Tile>>();
	private Set<Tile> objEnemyOriginTiles=new HashSet<Tile>();
	private Set<Tile> objEnemyImmobileTiles=new HashSet<Tile>();
	private List<CombatStatistics> objEnemyScenarios;
	private Double dMinSurviveRatioScenarios;
	private Double dMinDeadRatioScenarios;
	private Integer iEnemyDeadAtMinDeadRatio;
	private Integer iAllyDeadAtMinDeadRatio;
	private Map<TileAimKey,Integer> objNumTimesAsPosibleImprovements;

	private EnemyCartessian objEnemyCartessian;
	
	
	public BattlePlan(Set<TileAimKey> objMyAntsActions,BattlePlanGeneral objGeneral){
		this(objMyAntsActions,null,null,objGeneral);
	}

	
	public BattlePlan(Set<TileAimKey> objMyAntsActions,Set<Tile>objProhibitedTilesForPush,EnemyCartessian objEnemyCartessian,BattlePlanGeneral objGeneral){
		if (objProhibitedTilesForPush!=null){
			this.objProhibitedTilesForPush=objProhibitedTilesForPush;
		}else{
			this.objProhibitedTilesForPush=new HashSet<Tile>();
		}
		
		if (objEnemyCartessian!=null){
			this.objEnemyCartessian=new EnemyCartessian(objEnemyCartessian);
		}else{
			this.objEnemyCartessian=new EnemyCartessian();
		}
		
		this.objMyAntsActions=objMyAntsActions;
		objMyAntsTileAimKeys=new HashMap<Tile, TileAimKey>();
		this.objAntsInDanger=new HashSet<Tile>();
		objDestTiles=new HashSet<Tile>();
		objSrcTiles=new HashSet<Tile>();
		objObligatoryImprovements=new HashSet<TileAimKey>();
		this.objGeneral=objGeneral;
		Ants ants=objGeneral.getAnts();
		objBattleResolution=new HashMap<Tile, TileBattleResolutionInfo>();
		objNumTimesAsPosibleImprovements=new HashMap<TileAimKey, Integer>();
		Iterator<TileAimKey> it=objMyAntsActions.iterator();
		while (it.hasNext()&&!bIncompatible){
			TileAimKey objCurrent=it.next();
			if (objMyAntsTileAimKeys.containsKey(objCurrent.getTile())){
				bIncompatible=true;
			}else{
				if (objDestTiles.contains(objCurrent.getTileDest())){
					bIncompatible=true;
				}else{
					
					ResultMove objResultMove=new ResultMove();
					boolean bCurrentlyPassable=ants.isCurrentlyPassable(objCurrent.getTileDest(), objResultMove);
					if (!bCurrentlyPassable){
						if (objCurrent.getAim()!=null &&( objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_ENEMY_ANT)||
								objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_ENEMY_ANT_ADJACENT)||
								objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_FOOD)||
								objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_WATER)||
								objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_MY_HILL))){
							bIncompatible=true;
						}else if (objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_DEST_ORDER)){
							if (!(ants.isAntIssuedOrder(objCurrent.getTile())&&ants.isAntIssuedThisOrder(objCurrent))){
								bIncompatible=true;
							}else{
								objMyAntsTileAimKeys.put(objCurrent.getTile(), objCurrent);
								objDestTiles.add(objCurrent.getTileDest());
								objSrcTiles.add(objCurrent.getTile());
							}
						}else if (objResultMove.containsReason(BlockedMovesReasons.BLOCKED_BY_MY_ANT)){
							objMyAntsTileAimKeys.put(objCurrent.getTile(), objCurrent);
							objDestTiles.add(objCurrent.getTileDest());
							objSrcTiles.add(objCurrent.getTile());
						}else{
							bIncompatible=true;
						}
					}else{
						objMyAntsTileAimKeys.put(objCurrent.getTile(), objCurrent);
						objDestTiles.add(objCurrent.getTileDest());
						objSrcTiles.add(objCurrent.getTile());
					}
				}
			}
		
		}
		if (!bIncompatible){
			process();
		}

		
	}

	private void process() {
		Ants ants=objGeneral.getAnts();
		this.objMyAntsActions.size();
		// Primero hacemos un recorrido por las  ordenes del plan de batalla
		// Buscamos los Tile de destino buscando posibles debilidades
		for (TileAimKey objMyAntTileAim : this.objMyAntsActions) {
			Tile objTileDest=objMyAntTileAim.getTileDest();
			if (!ants.isCurrentlyPassable(objTileDest, null)){
				if (ants.getTileInformation(objTileDest).getType()==Ilk.MY_ANT&&!ants.isAntIssuedOrder(objTileDest)&& !objSrcTiles.contains(objTileDest)){
					if (!objProhibitedTilesForPush.contains(objTileDest)){
						List<Aim> objListOfAim=new ArrayList<Aim>();
						objListOfAim.addAll(Aim.getPerpendicular(objMyAntTileAim.getAim()));
						objListOfAim.add(objMyAntTileAim.getAim());
						
						for (Aim aim : objListOfAim) {
							TileAimKey objTileAimKey=ants.getTileAimKey(objTileDest, aim);
							if (!objDestTiles.contains(objTileAimKey.getTileDest())){
								objObligatoryImprovements.add(objTileAimKey);
							}
						}
					}
					if (objObligatoryImprovements.isEmpty()){
						this.bIncompatible=true;
						return;
					}
				}
			}
			if (objObligatoryImprovements.isEmpty()&& objGeneral.containsThreatInfo(objTileDest)){
				TileThreatInfo objThreatInfo=objGeneral.getThreatInfo(objTileDest);
				int iDebility=objThreatInfo.getDebility(true);
				Set<TileAimKey> objThreatSet=objThreatInfo.getAllTileAim(true);
				if (objThreatSet.size()>0){
					objAntsInDanger.add(objMyAntTileAim.getTile());
				}
				for (TileAimKey objTileAimKey : objThreatSet) {
					TileBattleResolutionInfo objCurrentBattleResolution;
					if (objBattleResolution.containsKey(objTileAimKey.getTileDest())){
						objCurrentBattleResolution=objBattleResolution.get(objTileAimKey.getTileDest());
						objCurrentBattleResolution.setNewDebility(iDebility,objMyAntTileAim.getTile());
						objCurrentBattleResolution.addNewEnemyMove(objTileAimKey);
					}else{
						objCurrentBattleResolution=new TileBattleResolutionInfo(iDebility,objMyAntTileAim.getTile(),objTileAimKey.getTileDest());
						objBattleResolution.put(objTileAimKey.getTileDest(), objCurrentBattleResolution);
						objCurrentBattleResolution.addNewEnemyMove(objTileAimKey);
					}
					Set<Tile> objCurrentEnemySet;
					if (objEnemyDestOriginTiles.containsKey(objTileAimKey.getTileDest())){
						objCurrentEnemySet=objEnemyDestOriginTiles.get(objTileAimKey.getTileDest());
						
					}else{
						objCurrentEnemySet=new HashSet<Tile>();
						objEnemyDestOriginTiles.put(objTileAimKey.getTileDest(), objCurrentEnemySet);
					}
					objCurrentEnemySet.add(objTileAimKey.getTile());
					this.objEnemyOriginTiles.add(objTileAimKey.getTile());
					if (objTileAimKey.getAim()==null){
						this.objEnemyImmobileTiles.add(objTileAimKey.getTile());
					}
					
				}
				
			}
		}
		
		if (objObligatoryImprovements.isEmpty()){
			this.bComplete=true;
			this.bCanBeImproved=false;
			// Ahora recorremos las casillas de las que hemos detectado debilidades para ver posibles respuestas.
			for (Map.Entry<Tile, TileBattleResolutionInfo> objEntry:this.objBattleResolution.entrySet()) {
				TileBattleResolutionInfo objCurrentBattleResolution=objEntry.getValue();
				if ( objGeneral.containsThreatInfo(objEntry.getKey())){
					TileThreatInfo objThreatInfo=objGeneral.getThreatInfo(objEntry.getKey());
					Set<TileAimKey> objThreatSet=objThreatInfo.getAllTileAim(false);
					for (TileAimKey objTileAimKey : objThreatSet) {
						if (this.objMyAntsActions.contains(objTileAimKey)){
							objCurrentBattleResolution.addNewConfirmedDebility(objTileAimKey);
						}else{
							// Solo tenemos en cuenta otras hormigas que no estan ya en batalla para posibles mejoras del plan.
							if (!this.objMyAntsTileAimKeys.containsKey(objTileAimKey.getTile())){
								//Tampoco metemos los movimientos que colisionan con los movimientos anteriores
								if (!this.objDestTiles.contains(objTileAimKey.getTileDest())){
									// Si ya ha emitido movimiento, metemos solo el movimiento que ya ha realizado
									if (ants.isAntIssuedOrder(objTileAimKey.getTile())){
										if (ants.isAntIssuedThisOrder(objTileAimKey)){
											TileThreatInfo objCurrentThreadInfo=objGeneral.getThreatInfo(objTileAimKey.getTileDest());
											TacticValoration objTacticValoration=objCurrentThreadInfo.getTacticValorationForPlayer();
											objCurrentBattleResolution.addPosibleImprovement(objTileAimKey,objTacticValoration);
											int iNumTimes=1;
											if (objNumTimesAsPosibleImprovements.containsKey(objTileAimKey)){
												iNumTimes=objNumTimesAsPosibleImprovements.get(objTileAimKey)+1;
											}
											objNumTimesAsPosibleImprovements.put(objTileAimKey, iNumTimes);
										}
									}else{
										// Comprobamos que no colisione con 
										if (objTileAimKey.getAim()==null || !ants.isTileDestOrder(objTileAimKey.getTileDest()) ){
											TileThreatInfo objCurrentThreadInfo=objGeneral.getThreatInfo(objTileAimKey.getTileDest());
											TacticValoration objTacticValoration=objCurrentThreadInfo.getTacticValorationForPlayer();
											objCurrentBattleResolution.addPosibleImprovement(objTileAimKey,objTacticValoration);
											int iNumTimes=1;
											if (objNumTimesAsPosibleImprovements.containsKey(objTileAimKey)){
												iNumTimes=objNumTimesAsPosibleImprovements.get(objTileAimKey)+1;
											}
											objNumTimesAsPosibleImprovements.put(objTileAimKey, iNumTimes);
	
										}
									}
								}
							}
						}
					}
				}
				if (objCurrentBattleResolution.canBeImproved()){
					bCanBeImproved=true;
				}
				int iNeededDebility=objCurrentBattleResolution.getDebilityRequired();
				int iConfirmedDebility=objCurrentBattleResolution.getSizeDebilityConfirmed();
				if (iConfirmedDebility>=iNeededDebility){
					iRetribution++;
					if (iConfirmedDebility>iNeededDebility){
						iSurvive++;
					}else{
						bComplete=false;
					}
				}else{
					bComplete=false;
				}

			}
		}else{
			this.bComplete=false;
			this.bCanBeImproved=true;
			objBattleResolution.clear();
		}
		
	}
	
	public List<BattlePlan> getImprovedBattlePlans(){
		
		List<BattlePlan> objResult=new ArrayList<BattlePlan>();



		if (!objObligatoryImprovements.isEmpty()){
			objProhibitedTilesForPush.addAll(objSrcTiles);
			for (TileAimKey objTileAimKey : this.objObligatoryImprovements) {
				objProhibitedTilesForPush.add(objTileAimKey.getTile());
				Set<TileAimKey> objNewSet=new HashSet<TileAimKey>(this.objMyAntsActions);
				objNewSet.add(objTileAimKey);
				BattlePlan objNewPlan=new BattlePlan(objNewSet,this.objProhibitedTilesForPush,this.objEnemyCartessian, this.objGeneral);
				objResult.add(objNewPlan);
			}


		}else{
			
			List<AllyedCartessianProductHelper> objCartessianList=null;
			List<TileBattleResolutionInfo> objListOfBattleResolutions=new ArrayList<TileBattleResolutionInfo>(this.objBattleResolution.values());
			Collections.sort(objListOfBattleResolutions,new TileBattleComparator2());
			boolean bFinded=false;
			Iterator<TileBattleResolutionInfo> itTileResolution=objListOfBattleResolutions.iterator();
			while(itTileResolution.hasNext()&&!bFinded) {
				TileBattleResolutionInfo objCurrentResolution=itTileResolution.next();
				objCartessianList=new LinkedList<AllyedCartessianProductHelper>();
					int iSizeOfCartessianNeeded=objCurrentResolution.getDebilityRequired()-objCurrentResolution.getSizeDebilityConfirmed()+1;
					iSizeOfCartessianNeeded=Math.max(iSizeOfCartessianNeeded, 1);
					int iMaxSizeOfCartessian=0;
					int iPosibleImprovement=objCurrentResolution.getNumPosibleImprovement();
					if (iSizeOfCartessianNeeded-1<=iPosibleImprovement){
						bFinded=true;
						if (iSizeOfCartessianNeeded-1==iPosibleImprovement){
							iSizeOfCartessianNeeded--;
						}
						List<Set<TileAimKey>> objListOfImprovements=new ArrayList<Set<TileAimKey>>();
						List<Set<TileAimKey>> objPartialListOfImprovements=new ArrayList<Set<TileAimKey>>();
						TileSetPosibleImprovementsComparator objTileSetPosibleImprovementsComparator=new TileSetPosibleImprovementsComparator(objCurrentResolution.getConfirmedDebility(), this.objNumTimesAsPosibleImprovements, objGeneral.getAnts());
						
						objPartialListOfImprovements.addAll(objCurrentResolution.getPosibleImprovementLiveDestTile().values());
						Collections.sort(objPartialListOfImprovements,objTileSetPosibleImprovementsComparator);
						objListOfImprovements.addAll(objPartialListOfImprovements);
						objPartialListOfImprovements.clear();
						
						objPartialListOfImprovements.addAll(objCurrentResolution.getPosibleImprovementKillDestTile().values());
						Collections.sort(objPartialListOfImprovements,objTileSetPosibleImprovementsComparator);
						objListOfImprovements.addAll(objPartialListOfImprovements);
						objPartialListOfImprovements.clear();

						objPartialListOfImprovements.addAll(objCurrentResolution.getPosibleImprovementDeadDestTile().values());
						Collections.sort(objPartialListOfImprovements,objTileSetPosibleImprovementsComparator);
						objListOfImprovements.addAll(objPartialListOfImprovements);
						objPartialListOfImprovements.clear();

						


						boolean bFirst=true;
						
						
						List<AllyedCartessianProductHelper> objPartialResult=new ArrayList<AllyedCartessianProductHelper>();
						List<AllyedCartessianProductHelper> objPartialCartessianSet=new LinkedList<AllyedCartessianProductHelper>();
						Iterator<Set<TileAimKey>> it=objListOfImprovements.iterator();
						while ((!objPartialCartessianSet.isEmpty()||bFirst)&&it.hasNext()&&objPartialResult.isEmpty()){
							Set<TileAimKey> objCurrentPosibleImprovement=it.next();
							if (bFirst){
								for (TileAimKey objTileAimKey : objCurrentPosibleImprovement) {
									AllyedCartessianProductHelper objNewHelper=new AllyedCartessianProductHelper();
									objNewHelper.add(objTileAimKey);
									if (objNewHelper.size()>=iSizeOfCartessianNeeded){
										objPartialResult.add(objNewHelper);
									}else{
										objPartialCartessianSet.add(objNewHelper);
									}
								}
								bFirst=false;
							}else{
								Iterator<AllyedCartessianProductHelper> itInner=objPartialCartessianSet.iterator();
								List<AllyedCartessianProductHelper> objNewAllyed=new ArrayList<AllyedCartessianProductHelper>();
								while (itInner.hasNext()){
									boolean bNotDerived=false;
									AllyedCartessianProductHelper objOldHelper=itInner.next();
									for (TileAimKey objTileAimKey : objCurrentPosibleImprovement) {
										AllyedCartessianProductHelper objNewHelper=new AllyedCartessianProductHelper(objOldHelper);
										if (!objNewHelper.isContainedTileSrc(objTileAimKey.getTile())){
											objNewHelper.add(objTileAimKey);
										}else{
											bNotDerived=true;
											objNewHelper.removeSrcTile(objTileAimKey.getTile());
											objNewHelper.add(objTileAimKey);
										}
										if (objNewHelper.size()>=iSizeOfCartessianNeeded){
											objPartialResult.add(objNewHelper);
										}else{
											objNewAllyed.add(objNewHelper);
										}
									}
									if (!bNotDerived){
										itInner.remove();
									}
								}
								objPartialCartessianSet.addAll(objNewAllyed);
							}
						}
						objCartessianList.addAll(objPartialResult);
					}
			}
			this.getMinDeadRatio();
			for (AllyedCartessianProductHelper objCartessianProductHelper : objCartessianList) {
				objCartessianProductHelper.addAll(this.objMyAntsActions);
				objResult.add(new BattlePlan(objCartessianProductHelper.getTileAimKeySet(),null,objEnemyCartessian, objGeneral));
			}
		}
		return objResult;
	}


	public boolean isIncompatible() {
		return bIncompatible;
	}


	public boolean isComplete() {
		return bComplete;
	}

	public boolean isObligatoryImproved(){
		return !this.objObligatoryImprovements.isEmpty();
	}

	public boolean canBeImproved() {
		return bCanBeImproved;
	}


	
	
	public double getSurviveRatio(){
		return objBattleResolution.isEmpty()?1:((double)iSurvive)/((double)objBattleResolution.size());
	}
	
	public double getRetributionRatio(){
		return objBattleResolution.isEmpty()?1:((double)iRetribution)/((double)objBattleResolution.size());
	}


	
	
//	@Override
//	public String toString() {
//		return String
//				.format("BattlePlan [objMyAntsActions=%s, bIncompatible=%s, bComplete=%s, bCanBeImproved=%s, getSurviveRatio()=%.2f, getRetributionRatio()=%.2f]",
//						objMyAntsActions, bIncompatible, bComplete, bCanBeImproved, getSurviveRatio(), getRetributionRatio());
//	}
	
	public Set<TileAimKey> getActionsOfPlan(){
		return this.objMyAntsActions;
	}
	
	public TileAimKey getTileAimForAnt(Tile objMyAnt){
		return objMyAntsTileAimKeys.get(objMyAnt);
	}


	public Set<Tile> getDestTiles() {
		return objDestTiles;
	}

	private void calculateSurvive(){
		Set<TileAimKey> objPosibleDeadMoves=new HashSet<TileAimKey>();
		Set<TileAimKey> objAllMoves=new HashSet<TileAimKey>();
		for (TileBattleResolutionInfo objCurrentBattleResolution : this.objBattleResolution.values()) {
			for (TileAimKey objTileAimKey : objCurrentBattleResolution.getConfirmedDebility()) {
				objAllMoves.add(objTileAimKey);
				int iDebilityForAnt=objCurrentBattleResolution.getDebilityForAnt(objTileAimKey.getTile());
				if (iDebilityForAnt>=objCurrentBattleResolution.getSizeDebilityConfirmed()){
					objPosibleDeadMoves.add(objTileAimKey);
				}
			}
		}
			
		
		dSurviveRatioMagnitude=(double) (objAllMoves.size()-objPosibleDeadMoves.size());
		dSurviveRatioComplete=dSurviveRatioMagnitude/objAllMoves.size();
		

	}

	public double getSurviveRatioMagnitude(){
		if (this.dSurviveRatioMagnitude==null){
			calculateSurvive();
		}
		return dSurviveRatioMagnitude;
	}
	
	public double getSurviveRatioComplete() {
		if (this.dSurviveRatioComplete==null){
			calculateSurvive();
		}
		return dSurviveRatioComplete;
	}

	
	private void calculateKill(){
		Map<Tile,KillRatioHelper> objKillRatioMap=new HashMap<Tile, KillRatioHelper>(); 
		for (Entry<Tile,TileBattleResolutionInfo> objEntry : objBattleResolution.entrySet()) {
			TileBattleResolutionInfo objTileBattleResolutionInfo=objEntry.getValue();

			for (TileAimKey objTileAimKey : objTileBattleResolutionInfo.getEnemyMoves()) {
				Tile objEnemyAnt=objTileAimKey.getTile();
				KillRatioHelper objCurrentKillRatioHelper;
				if (objKillRatioMap.containsKey(objEnemyAnt)){
					objCurrentKillRatioHelper=objKillRatioMap.get(objEnemyAnt);
				}else{
					objCurrentKillRatioHelper=new KillRatioHelper();
					objKillRatioMap.put(objEnemyAnt,objCurrentKillRatioHelper);
				}
				if (objTileBattleResolutionInfo.getSizeDebilityConfirmed()>=objTileBattleResolutionInfo.getMinimunDebility()){
					objCurrentKillRatioHelper.addDeadAim(objTileAimKey);
				}else{
					objCurrentKillRatioHelper.addCurrentMove(objTileAimKey);
				}
			}
			
		}
		double dResult=0;
		double dResultConfirmedKill=0;
		int iNumEnemies=objKillRatioMap.size();
		this.iNumEnemies=iNumEnemies;
		for (Entry<Tile,KillRatioHelper> objEntry : objKillRatioMap.entrySet()) {
			dResultConfirmedKill+=objEntry.getValue().getKillRatio()==1?1:0;
			dResult+=objEntry.getValue().getKillRatio();
			
		}
		dConfirmedKillRatio=dResultConfirmedKill/iNumEnemies;
		dKillRatioMagnitude=dResult;
		dResult=dResult/iNumEnemies;
		dKillRatio=dResult;

	}

	public double getConfirmedKillRatio(){
		if (this.dConfirmedKillRatio==null){
			calculateKill();
		}
		return this.dConfirmedKillRatio;
	}
	
	public double getKillRatioMagnitude(){
		if (dKillRatioMagnitude==null){
			calculateKill();
		}
		return dKillRatioMagnitude;
	}
	
	public int getNumEnemies(){
		if (iNumEnemies==null){
			calculateKill();
		}
		return iNumEnemies;
	}
	
	public double getKillRatio() {
		if (dKillRatio==null){
			calculateKill();
		}
		return dKillRatio;
	}


	@Override
	public String toString() {
		return String
				.format("BattlePlan [objMyAntsTileAimKeys=%s, bIncompatible=%s, bComplete=%s, bCanBeImproved=%s, getSurviveRatio()=%s, getRetributionRatio()=%s, getSurviveRatioMagnitude()=%s, getSurviveRatioComplete()=%s, getKillRatioMagnitude()=%s, getKillRatio()=%s]",
						objMyAntsTileAimKeys, bIncompatible, bComplete, bCanBeImproved, getSurviveRatio(), getRetributionRatio(),
						getSurviveRatioMagnitude(), getSurviveRatioComplete(), getKillRatioMagnitude(), getKillRatio());
	}


	public int getEnemyDeadAtMinDeadRatio(){
		if (this.iEnemyDeadAtMinDeadRatio==null){
			calculateEnemyScenarios6();
		}
		return this.iEnemyDeadAtMinDeadRatio;
	}
	
	public int getAllyDeadAtMinDeadRatio(){
		if (this.iAllyDeadAtMinDeadRatio==null){
			calculateEnemyScenarios6();
		}
		return this.iAllyDeadAtMinDeadRatio;
	}

	
	public double getMinDeadRatio(){
		if (dMinDeadRatioScenarios==null){
			calculateEnemyScenarios6();
		}
		return dMinDeadRatioScenarios;
		
	}

	public double getMinSurviveRatio(){
		if (dMinSurviveRatioScenarios==null){
			calculateEnemyScenarios6();
		}
		return dMinSurviveRatioScenarios;
		
	}

	
	
	public void calculateEnemyScenarios6(){
		List<TileBattleResolutionInfo> objResInfoList=new ArrayList<TileBattleResolutionInfo>(objBattleResolution.values());
		Comparator<TileBattleResolutionInfo> objComparator=new TileBattleResolutionConfirmedDebilityComparator3();
		Collections.sort(objResInfoList,objComparator);
		List<EnemySceneHelper> objCurrentScene=new ArrayList<EnemySceneHelper>();
		List<CombatStatistics> objResult=new ArrayList<CombatStatistics>();
		int iEnemyDeadAtMinDeadRatio=-1;
		int iAllyDeadAtMinDeadRatio=-1;
		double dMinDeadRatio=1;
		double dMinSurviveRatio=1;
		boolean bDeadRatioZeroFinded=false;
		boolean bSurviveZeroFinded=false;
		TileBattleResolutionInfo objFirst=null;
		
		Iterator<TileBattleResolutionInfo> it=objResInfoList.iterator();
		if (it.hasNext()){
			TileBattleResolutionInfo objResInfo=it.next();
			objFirst=objResInfo;
			Tile objTileDest=objResInfo.getTile();
			EnemySceneHelper objHelper=new EnemySceneHelper();
			objHelper.add(objResInfo.getTile(), objEnemyDestOriginTiles.get(objTileDest));
			CombatStatistics objCombat=new CombatStatistics(objHelper.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objHelper.getSrcTileSet());
			objResult.add(objCombat);
			if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
				bDeadRatioZeroFinded=true;
			}
			
			if (objCombat.getDeadRatio()<dMinDeadRatio){
				dMinDeadRatio=objCombat.getDeadRatio();
				iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
				iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
			}else{
				if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
					iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
					iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
				}
			}
			
			
			if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
				bSurviveZeroFinded=true;
			}
			if (objCombat.getSurviveRatio()<dMinSurviveRatio){
				dMinSurviveRatio=objCombat.getSurviveRatio();
			}

			if (objHelper.size()<this.objEnemyOriginTiles.size()){
				objCurrentScene.add(objHelper);
			}
	
		}
		
		while (it.hasNext()&&!objCurrentScene.isEmpty()&&!(bSurviveZeroFinded&&bDeadRatioZeroFinded)){
			TileBattleResolutionInfo objResInfo=it.next();
			Tile objTileDest=objResInfo.getTile();
			Set<Tile> objOriginTiles=objEnemyDestOriginTiles.get(objTileDest);
			Iterator<EnemySceneHelper> itInner=objCurrentScene.iterator();
			List<EnemySceneHelper> objNewScenes=new ArrayList<EnemySceneHelper>();
			boolean bInsertedSimpleScene=false;
			
			while (itInner.hasNext()&&!(bSurviveZeroFinded&&bDeadRatioZeroFinded)){
				EnemySceneHelper objEnemySceneHelper=itInner.next();
				EnemySceneHelper objNewHelper=new EnemySceneHelper(objEnemySceneHelper);
				objNewHelper.add(objTileDest, objOriginTiles);
				if (objNewHelper.isValid()){
					itInner.remove();
					CombatStatistics objCombat=new CombatStatistics(objNewHelper.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objNewHelper.getSrcTileSet());
					objResult.add(objCombat);
					if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
						bDeadRatioZeroFinded=true;
					}
					if (objCombat.getDeadRatio()<dMinDeadRatio){
						dMinDeadRatio=objCombat.getDeadRatio();
						iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
						iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
					}else{
						if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
							iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
							iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
						}
					}
					if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
						bSurviveZeroFinded=true;
					}
					if (objCombat.getSurviveRatio()<dMinSurviveRatio){
						dMinSurviveRatio=objCombat.getSurviveRatio();
					}

					if (objNewHelper.size()<this.objEnemyOriginTiles.size()){
						objNewScenes.add(objNewHelper);
					}
				}else{
					List<EnemySceneHelper> objNewScenesForNewHelper=objNewHelper.makeRoomFor(objTileDest);
					for (EnemySceneHelper objNewHelper2 : objNewScenesForNewHelper) {
						if (objNewHelper2.size()==1){
							bInsertedSimpleScene=true;
						}
						CombatStatistics objCombat=new CombatStatistics(objNewHelper2.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objNewHelper2.getSrcTileSet());
						objResult.add(objCombat);
						if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
							bDeadRatioZeroFinded=true;
						}
						if (objCombat.getDeadRatio()<dMinDeadRatio){
							dMinDeadRatio=objCombat.getDeadRatio();
							iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
							iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
						}else{
							if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
								iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
								iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
							}
						}
						if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
							bSurviveZeroFinded=true;
						}
						if (objCombat.getSurviveRatio()<dMinSurviveRatio){
							dMinSurviveRatio=objCombat.getSurviveRatio();
						}


						if (objNewHelper2.size()<this.objEnemyOriginTiles.size()){
							objNewScenes.add(objNewHelper2);
						}
					}
				}
			}
			
			if (!bInsertedSimpleScene&&objFirst!=null&&objComparator.compare(objFirst, objResInfo)==0){
				EnemySceneHelper objHelper=new EnemySceneHelper();
				objHelper.add(objResInfo.getTile(), objEnemyDestOriginTiles.get(objTileDest));
				CombatStatistics objCombat=new CombatStatistics(objHelper.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objHelper.getSrcTileSet());
				objResult.add(objCombat);
				if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
					bDeadRatioZeroFinded=true;
				}
				
				if (objCombat.getDeadRatio()<dMinDeadRatio){
					dMinDeadRatio=objCombat.getDeadRatio();
					iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
					iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
				}else{
					if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
						iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
						iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
					}
				}
				
				
				if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
					bSurviveZeroFinded=true;
				}
				if (objCombat.getSurviveRatio()<dMinSurviveRatio){
					dMinSurviveRatio=objCombat.getSurviveRatio();
				}

				if (objHelper.size()<this.objEnemyOriginTiles.size()){
					objCurrentScene.add(objHelper);
				}

			}

			
			objCurrentScene.addAll(objNewScenes);
		}
		this.iAllyDeadAtMinDeadRatio=iAllyDeadAtMinDeadRatio;
		this.iEnemyDeadAtMinDeadRatio=iEnemyDeadAtMinDeadRatio;
		this.dMinDeadRatioScenarios=dMinDeadRatio;
		this.dMinSurviveRatioScenarios=dMinSurviveRatio;
		this.objEnemyScenarios=objResult;
		
	}


	public void calculateEnemyScenarios6old(){
		List<TileBattleResolutionInfo> objResInfoList=new ArrayList<TileBattleResolutionInfo>(objBattleResolution.values());
		Collections.sort(objResInfoList,new TileBattleResolutionConfirmedDebilityComparator3());
		List<EnemySceneHelper> objCurrentScene=new ArrayList<EnemySceneHelper>();
		List<CombatStatistics> objResult=new ArrayList<CombatStatistics>();
		int iEnemyDeadAtMinDeadRatio=-1;
		int iAllyDeadAtMinDeadRatio=-1;
		double dMinDeadRatio=1;
		double dMinSurviveRatio=1;
		boolean bDeadRatioZeroFinded=false;
		boolean bSurviveZeroFinded=false;

		
		Iterator<TileBattleResolutionInfo> it=objResInfoList.iterator();
		if (it.hasNext()){
			TileBattleResolutionInfo objResInfo=it.next();
			Tile objTileDest=objResInfo.getTile();
			EnemySceneHelper objHelper=new EnemySceneHelper();
			objHelper.add(objResInfo.getTile(), objEnemyDestOriginTiles.get(objTileDest));
			CombatStatistics objCombat=new CombatStatistics(objHelper.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objHelper.getSrcTileSet());
			objResult.add(objCombat);
			if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
				bDeadRatioZeroFinded=true;
			}
			
			if (objCombat.getDeadRatio()<dMinDeadRatio){
				dMinDeadRatio=objCombat.getDeadRatio();
				iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
				iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
			}else{
				if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
					iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
					iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
				}
			}
			
			
			if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
				bSurviveZeroFinded=true;
			}
			if (objCombat.getSurviveRatio()<dMinSurviveRatio){
				dMinSurviveRatio=objCombat.getSurviveRatio();
			}

			if (objHelper.size()<this.objEnemyOriginTiles.size()){
				objCurrentScene.add(objHelper);
			}
	
		}
		
		while (it.hasNext()&&!objCurrentScene.isEmpty()&&!(bSurviveZeroFinded&&bDeadRatioZeroFinded)){
			TileBattleResolutionInfo objResInfo=it.next();
			Tile objTileDest=objResInfo.getTile();
			Set<Tile> objOriginTiles=objEnemyDestOriginTiles.get(objTileDest);
			Iterator<EnemySceneHelper> itInner=objCurrentScene.iterator();
			List<EnemySceneHelper> objNewScenes=new ArrayList<EnemySceneHelper>();
			while (itInner.hasNext()&&!(bSurviveZeroFinded&&bDeadRatioZeroFinded)){
				EnemySceneHelper objEnemySceneHelper=itInner.next();
				EnemySceneHelper objNewHelper=new EnemySceneHelper(objEnemySceneHelper);
				objNewHelper.add(objTileDest, objOriginTiles);
				if (objNewHelper.isValid()){
					itInner.remove();
					CombatStatistics objCombat=new CombatStatistics(objNewHelper.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objNewHelper.getSrcTileSet());
					objResult.add(objCombat);
					if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
						bDeadRatioZeroFinded=true;
					}
					if (objCombat.getDeadRatio()<dMinDeadRatio){
						dMinDeadRatio=objCombat.getDeadRatio();
						iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
						iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
					}else{
						if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
							iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
							iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
						}
					}
					if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
						bSurviveZeroFinded=true;
					}
					if (objCombat.getSurviveRatio()<dMinSurviveRatio){
						dMinSurviveRatio=objCombat.getSurviveRatio();
					}

					if (objNewHelper.size()<this.objEnemyOriginTiles.size()){
						objNewScenes.add(objNewHelper);
					}
				}else{
					List<EnemySceneHelper> objNewScenesForNewHelper=objNewHelper.makeRoomFor(objTileDest);
					for (EnemySceneHelper objNewHelper2 : objNewScenesForNewHelper) {
						CombatStatistics objCombat=new CombatStatistics(objNewHelper2.getDestTileSet(),this.objBattleResolution,this.objAntsInDanger.size(),objNewHelper2.getSrcTileSet());
						objResult.add(objCombat);
						if (objCombat.getDeadRatio()==0&&!bDeadRatioZeroFinded){
							bDeadRatioZeroFinded=true;
						}
						if (objCombat.getDeadRatio()<dMinDeadRatio){
							dMinDeadRatio=objCombat.getDeadRatio();
							iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
							iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
						}else{
							if (iEnemyDeadAtMinDeadRatio==-1||(objCombat.getDeadRatio()==dMinDeadRatio&&iEnemyDeadAtMinDeadRatio<objCombat.getEnemyDead())){
								iEnemyDeadAtMinDeadRatio=objCombat.getEnemyDead();
								iAllyDeadAtMinDeadRatio=objCombat.getAllyDead();
							}
						}
						if (objCombat.getSurviveRatio()==0&&!bSurviveZeroFinded){
							bSurviveZeroFinded=true;
						}
						if (objCombat.getSurviveRatio()<dMinSurviveRatio){
							dMinSurviveRatio=objCombat.getSurviveRatio();
						}


						if (objNewHelper2.size()<this.objEnemyOriginTiles.size()){
							objNewScenes.add(objNewHelper2);
						}
					}
				}
			}
			objCurrentScene.addAll(objNewScenes);
		}
		this.iAllyDeadAtMinDeadRatio=iAllyDeadAtMinDeadRatio;
		this.iEnemyDeadAtMinDeadRatio=iEnemyDeadAtMinDeadRatio;
		this.dMinDeadRatioScenarios=dMinDeadRatio;
		this.dMinSurviveRatioScenarios=dMinSurviveRatio;
		this.objEnemyScenarios=objResult;
		
	}

	

	
	
	public List<CombatStatistics> getEnemyScenarios() {
		return objEnemyScenarios;
	}


	public Set<Tile> getEnemyOriginTiles() {
		return objEnemyOriginTiles;
	}



}


class TileBattleComparator implements Comparator<TileBattleResolutionInfo>{

	@Override
	public int compare(TileBattleResolutionInfo objO1, TileBattleResolutionInfo objO2) {
		int iDebilityLeft1=objO1.getDebilityRequired()-objO1.getSizeDebilityConfirmed();
		int iDebilityLeft2=objO2.getDebilityRequired()-objO2.getSizeDebilityConfirmed();
		if (iDebilityLeft1>iDebilityLeft2){
			return -1;
		}else if (iDebilityLeft1<iDebilityLeft2){
			return 1;
		}else{
			int iSizeOfPosibleImprovement1=objO1.getPosibleImprovement().size();
			int iSizeOfPosibleImprovement2=objO2.getPosibleImprovement().size();
			if (iSizeOfPosibleImprovement1>iSizeOfPosibleImprovement2){
				return 1;
			}else if (iSizeOfPosibleImprovement1<iSizeOfPosibleImprovement2){
				return -1;
			}else{
				return 0;
			}
		}
		
	}
	
}

class TileBattleComparator2 implements Comparator<TileBattleResolutionInfo>{

	@Override
	public int compare(TileBattleResolutionInfo objO1, TileBattleResolutionInfo objO2) {
		
		if (objO1.getSizeDebilityConfirmed()<=objO1.getDebilityRequired()&&objO2.getSizeDebilityConfirmed()>objO2.getDebilityRequired()){
			return -1;
		}else if (objO2.getSizeDebilityConfirmed()<=objO2.getDebilityRequired()&&objO1.getSizeDebilityConfirmed()>objO1.getDebilityRequired()){
			return 1;
		}else{
			int iPosibleImprovementsSize1= objO1.getNumPosibleImprovement();
			int iPosibleImprovementsSize2= objO2.getNumPosibleImprovement();
			if (iPosibleImprovementsSize1>0 && iPosibleImprovementsSize2==0){
				return -1;
			}else if (iPosibleImprovementsSize2>0 && iPosibleImprovementsSize1==0){
				return 1;
			}else if (iPosibleImprovementsSize2==0 && iPosibleImprovementsSize1==0){
				return 0;
			}else{
				int iDebilityLeft1=objO1.getDebilityRequired()-objO1.getSizeDebilityConfirmed();
				int iDebilityLeft2=objO2.getDebilityRequired()-objO2.getSizeDebilityConfirmed();



				iPosibleImprovementsSize1=objO1.getNumPosibleImprovementsLive();
				iPosibleImprovementsSize2=objO2.getNumPosibleImprovementsLive();
				
				
				
				
				
				
				if (iPosibleImprovementsSize1>iDebilityLeft1 && iPosibleImprovementsSize2<=iDebilityLeft2){
					return -1;
				}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1<=iDebilityLeft1){
					return 1;
				}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1>iDebilityLeft1){
					if (iDebilityLeft1<iDebilityLeft2){
						return -1;
					}else if (iDebilityLeft2<iDebilityLeft1){
						return 1;
					}else{
						return 0;
					}
				}else{
					iPosibleImprovementsSize1=objO1.getNumPosibleImprovementsLiveOrKill();
					iPosibleImprovementsSize2=objO2.getNumPosibleImprovementsLiveOrKill();
					if (iPosibleImprovementsSize1>iDebilityLeft1 && iPosibleImprovementsSize2<=iDebilityLeft2){
						return -1;
					}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1<=iDebilityLeft1){
						return 1;
					}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1>iDebilityLeft1){
						if (iDebilityLeft1<iDebilityLeft2){
							return -1;
						}else if (iDebilityLeft2<iDebilityLeft1){
							return 1;
						}else{
							return 0;
						}
					}else{
						iPosibleImprovementsSize1= objO1.getNumPosibleImprovement();
						iPosibleImprovementsSize2= objO2.getNumPosibleImprovement();

						if (iPosibleImprovementsSize1>iDebilityLeft1 && iPosibleImprovementsSize2<=iDebilityLeft2){
							return -1;
						}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1<=iDebilityLeft1){
							return 1;
						}else if (iPosibleImprovementsSize2>iDebilityLeft2 && iPosibleImprovementsSize1>iDebilityLeft1){
							if (iDebilityLeft1<iDebilityLeft2){
								return -1;
							}else if (iDebilityLeft2<iDebilityLeft1){
								return 1;
							}else{
								return 0;
							}
						}else{
							if (iDebilityLeft1<iDebilityLeft2){
								return -1;
							}else if (iDebilityLeft2<iDebilityLeft1){
								return 1;
							}else{
								return 0;
							}
						}
					}
				}
			}
		}
	}

}



class TileBattleResolutionInfo{
	int iDebilityRequired;
	int iMinimunDebility;
	private Set<TileAimKey> objConfirmedDebility;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovement;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementForDestTile;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementLive;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementLiveDestTile;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementKill;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementKillDestTile;
	private Set<Tile> objPosibleImprovementLiveOrKill;
	private Set<Tile> objPosibleImprovementLiveOrKillDestTile;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementDead;
	private Map<Tile,Set<TileAimKey>> objPosibleImprovementDeadDestTile;
	private Set<TileAimKey> objEnemyMoves;
	private Map<Tile,Integer> objDebilityMap;
	private NavigableMap<Integer,Set<Tile>> objInverseDebilityMap;
	private Tile objTile;
	public TileBattleResolutionInfo(TileBattleResolutionInfo objTileBattleResolutionInfo){
		this.objTile=objTileBattleResolutionInfo.objTile;
		iDebilityRequired=objTileBattleResolutionInfo.iDebilityRequired;
		iMinimunDebility=objTileBattleResolutionInfo.iMinimunDebility;
		this.objConfirmedDebility=new HashSet<TileAimKey>();
		this.objConfirmedDebility.addAll(this.objConfirmedDebility);
		this.objEnemyMoves=new HashSet<TileAimKey>();
		this.objEnemyMoves.addAll(objTileBattleResolutionInfo.objEnemyMoves);
		this.objPosibleImprovement=new HashMap<Tile, Set<TileAimKey>>();
		this.objDebilityMap=new HashMap<Tile, Integer>();
		this.objDebilityMap.putAll(objTileBattleResolutionInfo.objDebilityMap);
		this.objInverseDebilityMap=new TreeMap<Integer, Set<Tile>>();
		this.objPosibleImprovementLive=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementKill=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementDead=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementLiveDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementKillDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementDeadDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementLiveOrKill=new HashSet<Tile>(objTileBattleResolutionInfo.objPosibleImprovementLiveOrKill);
		this.objPosibleImprovementLiveOrKillDestTile=new HashSet<Tile>(objTileBattleResolutionInfo.objPosibleImprovementLiveOrKillDestTile);

		for (Entry<Integer, Set<Tile>> objEntry : objTileBattleResolutionInfo.objInverseDebilityMap.entrySet()) {
			this.objInverseDebilityMap.put(objEntry.getKey(), new HashSet<Tile>(objEntry.getValue()));
		}
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovement.entrySet()) {
			this.objPosibleImprovement.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementLive.entrySet()) {
			this.objPosibleImprovementLive.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementKill.entrySet()) {
			this.objPosibleImprovementKill.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementDead.entrySet()) {
			this.objPosibleImprovementDead.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementLiveDestTile.entrySet()) {
			this.objPosibleImprovementLiveDestTile.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementKillDestTile.entrySet()) {
			this.objPosibleImprovementKillDestTile.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementDeadDestTile.entrySet()) {
			this.objPosibleImprovementDeadDestTile.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}
		






		
		this.objPosibleImprovementForDestTile=new HashMap<Tile, Set<TileAimKey>>();
		for (Entry<Tile, Set<TileAimKey>> objEntry : objTileBattleResolutionInfo.objPosibleImprovementForDestTile.entrySet()) {
			this.objPosibleImprovementForDestTile.put(objEntry.getKey(), new HashSet<TileAimKey>(objEntry.getValue()));
		}

	}
	
	public TileBattleResolutionInfo(int iDebilityRequired,Tile objMyAntTile,Tile objDestTile) {
		this.objTile=objDestTile;
		this.iDebilityRequired=iDebilityRequired;
		this.iMinimunDebility=iDebilityRequired;
		objConfirmedDebility=new HashSet<TileAimKey>();
		objPosibleImprovement=new HashMap<Tile, Set<TileAimKey>>();
		objPosibleImprovementForDestTile=new HashMap<Tile, Set<TileAimKey>>();
		objDebilityMap=new HashMap<Tile, Integer>();
		objDebilityMap.put(objMyAntTile,iDebilityRequired);
		objInverseDebilityMap=new TreeMap<Integer, Set<Tile>>();
		addInverseDebility(iDebilityRequired, objMyAntTile);
		this.objEnemyMoves=new HashSet<TileAimKey>();
		this.objPosibleImprovementLive=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementKill=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementDead=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementLiveDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementKillDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementDeadDestTile=new HashMap<Tile, Set<TileAimKey>>();
		this.objPosibleImprovementLiveOrKill=new HashSet<Tile>();
		this.objPosibleImprovementLiveOrKillDestTile=new HashSet<Tile>();
	}

	
	
	private void addInverseDebility(int iDebilityRequired,Tile objMyAntTile){
		if (objInverseDebilityMap.containsKey(iDebilityRequired)){
			objInverseDebilityMap.get(iDebilityRequired).add(objMyAntTile);
		}else{
			Set<Tile> objNewSet=new HashSet<Tile>();
			objNewSet.add(objMyAntTile);
			objInverseDebilityMap.put(iDebilityRequired, objNewSet);
		}
	}
	
	public void addNewEnemyMove(TileAimKey objTileAimKey){
		this.objEnemyMoves.add(objTileAimKey);
	}
	
	
	public void setNewDebility(int iNewDebility,Tile objMyAntTile){
		this.objDebilityMap.put(objMyAntTile, iNewDebility);
		this.iDebilityRequired=Math.max(iNewDebility, iDebilityRequired);
		this.iMinimunDebility=Math.min(iNewDebility, iMinimunDebility);
		this.addInverseDebility(iNewDebility, objMyAntTile);
	}

	public int getDebilityForAnt(Tile objMyAntTile){
		return this.objDebilityMap.get(objMyAntTile);
	}
	
	public void addNewConfirmedDebility(TileAimKey objNewConfirmedDebility){
		this.objConfirmedDebility.add(objNewConfirmedDebility);
	}
	
	
	
	
	public boolean containsAntAsPosibleImprovement(Tile objTileAnt){
		return this.objPosibleImprovement.containsKey(objTileAnt);
	}
	
	
	
	public boolean containsTileAimKeyAsPosibleImprovement(TileAimKey objTileAimKey){
		if (this.objPosibleImprovement.containsKey(objTileAimKey.getTile())){
			return objPosibleImprovement.get(objTileAimKey.getTile()).contains(objTileAimKey);
		}else{
			return false;
		}
	}
	
	
	public void removeAntAsPosibleImprovement(Tile objTileAnt){
		if (containsAntAsPosibleImprovement(objTileAnt)){
			Set<TileAimKey> objRemovedSet=this.objPosibleImprovement.remove(objTileAnt);
			for (TileAimKey objTileAimKey : objRemovedSet) {
				this.objPosibleImprovementForDestTile.get(objTileAimKey.getTileDest()).remove(objTileAimKey);
			}
		}
	}
	

	public void addPosibleImprovement(TileAimKey objTileAimKey,TacticValoration objTacticValoration){
		Tile objMyAnt=objTileAimKey.getTile();
		if (this.objPosibleImprovement.containsKey(objMyAnt)){
			this.objPosibleImprovement.get(objMyAnt).add(objTileAimKey);	
		}else{
			Set<TileAimKey> objSetTileAimKey=new HashSet<TileAimKey>();
			objSetTileAimKey.add(objTileAimKey);
			objPosibleImprovement.put(objMyAnt, objSetTileAimKey);
		}
		Tile objDestTile=objTileAimKey.getTileDest();
		if (this.objPosibleImprovementForDestTile.containsKey(objDestTile)){
			this.objPosibleImprovementForDestTile.get(objDestTile).add(objTileAimKey);	
		}else{
			Set<TileAimKey> objSetTileAimKey=new HashSet<TileAimKey>();
			objSetTileAimKey.add(objTileAimKey);
			objPosibleImprovementForDestTile.put(objDestTile, objSetTileAimKey);
		}

		Map<Tile, Set<TileAimKey>> objPosibleImprovementType=null;
		switch(objTacticValoration){
		case LIVE:
			objPosibleImprovementType=objPosibleImprovementLive;
			objPosibleImprovementLiveOrKill.add(objMyAnt);
			break;
		case KILL:
			objPosibleImprovementType=objPosibleImprovementKill;
			objPosibleImprovementLiveOrKill.add(objMyAnt);
			break;
		case DEAD:
			objPosibleImprovementType=objPosibleImprovementDead;
			break;
		}
		if (objPosibleImprovementType.containsKey(objMyAnt)){
			objPosibleImprovementType.get(objMyAnt).add(objTileAimKey);	
		}else{
			Set<TileAimKey> objSetTileAimKey=new HashSet<TileAimKey>();
			objSetTileAimKey.add(objTileAimKey);
			objPosibleImprovementType.put(objMyAnt, objSetTileAimKey);
		}

		
		Map<Tile, Set<TileAimKey>> objPosibleImprovementDestTileType=null;
		switch(objTacticValoration){
		case LIVE:
			objPosibleImprovementDestTileType=objPosibleImprovementLiveDestTile;
			objPosibleImprovementLiveOrKillDestTile.add(objDestTile);
			break;
		case KILL:
			objPosibleImprovementDestTileType=objPosibleImprovementKillDestTile;
			objPosibleImprovementLiveOrKillDestTile.add(objDestTile);
			break;
		case DEAD:
			objPosibleImprovementDestTileType=objPosibleImprovementDeadDestTile;
			break;
		}
		if (objPosibleImprovementDestTileType.containsKey(objDestTile)){
			objPosibleImprovementDestTileType.get(objDestTile).add(objTileAimKey);	
		}else{
			Set<TileAimKey> objSetTileAimKey=new HashSet<TileAimKey>();
			objSetTileAimKey.add(objTileAimKey);
			objPosibleImprovementDestTileType.put(objDestTile, objSetTileAimKey);
		}


		

	}
	public int getNumPosibleImprovementsLive(){
		return Math.min(objPosibleImprovementLive.size(), objPosibleImprovementLiveDestTile.size());
	}
	
	public int getNumPosibleImprovementsKill(){
		return Math.min(objPosibleImprovementKill.size(), objPosibleImprovementKillDestTile.size());
	}
	
	public int getNumPosibleImprovementsDead(){
		return Math.min(objPosibleImprovementDead.size(), objPosibleImprovementDeadDestTile.size());
	}
	
	
	public int getNumPosibleImprovementsLiveOrKill(){
		return Math.min(objPosibleImprovementLiveOrKill.size(), objPosibleImprovementLiveOrKillDestTile.size());
	}


	
	public boolean containsConfirmedDebility(TileAimKey objTileAimKey){
		return this.objConfirmedDebility.contains(objTileAimKey);
	}
	
	public int getSizeDebilityConfirmed(){
		return this.objConfirmedDebility.size();
	}
	
	public boolean canBeImproved(){
		return !this.objPosibleImprovement.isEmpty();
	}

	public int getDebilityRequired() {
		return iDebilityRequired;
	}

	public int getNumPosibleImprovement(){
		return Math.min(objPosibleImprovement.size(), objPosibleImprovementForDestTile.size());
	}
	
	public Map<Tile, Set<TileAimKey>> getPosibleImprovement() {
		return objPosibleImprovement;
	}

	@Override
	public String toString() {
		return String.format("TileBattleResolutionInfo [iDebilityRequired=%s, objConfirmedDebility=%s, objPosibleImprovement=%s]", iDebilityRequired,
				objConfirmedDebility, objPosibleImprovement);
	}

	public int getMinimunDebility() {
		return iMinimunDebility;
	}

	public Set<TileAimKey> getEnemyMoves() {
		return objEnemyMoves;
	}

	public Set<TileAimKey> getConfirmedDebility() {
		return objConfirmedDebility;
	}

	public Tile getTile() {
		return objTile;
	}

	public NavigableMap<Integer, Set<Tile>> getInverseDebilityMap() {
		return objInverseDebilityMap;
	}

	public Map<Tile, Set<TileAimKey>> getPosibleImprovementLiveDestTile() {
		return objPosibleImprovementLiveDestTile;
	}

	public Map<Tile, Set<TileAimKey>> getPosibleImprovementKillDestTile() {
		return objPosibleImprovementKillDestTile;
	}
	
	public Map<Tile, Set<TileAimKey>> getPosibleImprovementDeadDestTile() {
		return objPosibleImprovementDeadDestTile;
	}

	
	
	
	
	
}


class AllyedCartessianProductHelper{
	Set<TileAimKey> objTileAimKeySet;
	Set<Tile> objTileDestSet;
	Set<Tile> objTileSrcSet;
	Map<Tile,TileAimKey> objRelationSrcTileAimKey;
	public AllyedCartessianProductHelper(){
		objTileAimKeySet=new HashSet<TileAimKey>();
		objTileDestSet=new HashSet<Tile>();
		objTileSrcSet=new HashSet<Tile>();
		objRelationSrcTileAimKey=new HashMap<Tile, TileAimKey>();
	}
	

	
	public void addAll(Set<TileAimKey> objMyAntsActions) {
		for (TileAimKey objTileAimKey : objMyAntsActions) {
			objTileAimKeySet.add(objTileAimKey);
			objTileDestSet.add(objTileAimKey.getTileDest());
		}
		
	}

	public AllyedCartessianProductHelper(AllyedCartessianProductHelper objHelper){
		objTileAimKeySet=new HashSet<TileAimKey>(objHelper.objTileAimKeySet);
		objTileDestSet=new HashSet<Tile>(objHelper.objTileDestSet);
		objTileSrcSet=new HashSet<Tile>(objHelper.objTileSrcSet);
		objRelationSrcTileAimKey=new HashMap<Tile, TileAimKey>(objHelper.objRelationSrcTileAimKey);
	}
	
	public void add(TileAimKey objNewAimKey){
		objTileAimKeySet.add(objNewAimKey);
		objTileDestSet.add(objNewAimKey.getTileDest());
		objTileSrcSet.add(objNewAimKey.getTile());
		objRelationSrcTileAimKey.put(objNewAimKey.getTile(),objNewAimKey);

	}
	
	public boolean isContainedTileDest(Tile objTile){
		return objTileDestSet.contains(objTile);
	}
	
	public boolean isContainedTileSrc(Tile objTileSrc){
		return objTileSrcSet.contains(objTileSrc);
	}

	public Set<TileAimKey> getTileAimKeySet() {
		return objTileAimKeySet;
	}
	
	public void removeSrcTile(Tile objSrcTile){
		TileAimKey objTileAimKey=objRelationSrcTileAimKey.remove(objSrcTile);
		objTileSrcSet.remove(objSrcTile);
		objTileAimKeySet.remove(objTileAimKey);
		objTileDestSet.remove(objTileAimKey.getTileDest());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objTileAimKeySet == null) ? 0 : objTileAimKeySet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AllyedCartessianProductHelper other = (AllyedCartessianProductHelper) obj;
		if (objTileAimKeySet == null) {
			if (other.objTileAimKeySet != null)
				return false;
		} else if (!objTileAimKeySet.equals(other.objTileAimKeySet))
			return false;
		return true;
	}

	public int size() {
		return objTileAimKeySet.size();
	}
	
	
	
}

class KillRatioHelper{
	Set<TileAimKey> objDeadMoves=new HashSet<TileAimKey>();
	Set<TileAimKey> objCurrentMoves=new HashSet<TileAimKey>();
	public void addDeadAim(TileAimKey objTileAimKey){
		objDeadMoves.add(objTileAimKey);
		objCurrentMoves.add(objTileAimKey);
	}
	
	public void addCurrentMove(TileAimKey objTileAimKey){
		objCurrentMoves.add(objTileAimKey);
	}
	public double getKillRatio(){
		double dDeadMoves=objDeadMoves.size();
		double dCurrentMoves=objCurrentMoves.size();
		return dDeadMoves/dCurrentMoves;
	}
	public double getSurviveRatio(){
		double dDeadMoves=objDeadMoves.size();
		double dCurrentMoves=objCurrentMoves.size();
		return (dCurrentMoves-dDeadMoves)/dCurrentMoves;
	}

}

class EnemyCartessianProductHelper{
	Set<Tile> objOriginTiles=new HashSet<Tile>();
	Set<Tile> objDestTiles=new HashSet<Tile>();
	
	public EnemyCartessianProductHelper() {
	}
	
	public EnemyCartessianProductHelper(EnemyCartessianProductHelper objOldHelper){
		this.objOriginTiles.addAll(objOldHelper.objOriginTiles);
		this.objDestTiles.addAll(objOldHelper.objDestTiles);
	}
	
	public void addOriginDestPair(Tile objTileOrigin,Tile objTileDest){
		this.objOriginTiles.add(objTileOrigin);
		this.objDestTiles.add(objTileDest);
	}
	
	public boolean containsOriginTile(Tile objTileOrigin){
		return this.objOriginTiles.contains(objTileOrigin);
	}
	public boolean containsDestTile(Tile objTileDest){
		return this.objDestTiles.contains(objTileDest);
	}
	public int size(){
		return objDestTiles.size();
	}
	public Set<Tile> getDestTileSet(){
		return objDestTiles;
	}
	
	
}

class EnemyCartessianProductHelper2{
	Set<Tile> objOriginTiles=new HashSet<Tile>();
	Set<Tile> objDestTiles=new HashSet<Tile>();
	
	public EnemyCartessianProductHelper2() {
	}
	
	public EnemyCartessianProductHelper2(EnemyCartessianProductHelper2 objOldHelper){
		this.objOriginTiles.addAll(objOldHelper.objOriginTiles);
		this.objDestTiles.addAll(objOldHelper.objDestTiles);
	}
	
	public void add(Tile objDest,Set<Tile> objOriginSet){
		this.objDestTiles.add(objDest);
		this.objOriginTiles.addAll(objOriginSet);
	}
	
	public boolean isValid(){
		return objOriginTiles.size()>=objDestTiles.size();
	}
	
	public int size(){
		return objDestTiles.size();
	}
	public Set<Tile> getDestTileSet(){
		return objDestTiles;
	}
	
	
}

class EnemyCartessian{
	private Set<Tile> objDestTilesProcesed;
	private List<EnemyCartessianProductHelper2> objCartessianGroups;
	
	public List<EnemyCartessianProductHelper2> process(Tile objTileDest,Set<Tile> objTileOriginSet){
		List<EnemyCartessianProductHelper2>  objNewGroups=new ArrayList<EnemyCartessianProductHelper2>();
		if (!objDestTilesProcesed.contains(objTileDest)){
			int iOldSize=objCartessianGroups.size();
			EnemyCartessianProductHelper2 objHelper=new EnemyCartessianProductHelper2();
			objHelper.add(objTileDest, objTileOriginSet);
			objCartessianGroups.add(objHelper);
			objNewGroups.add(objHelper);
			for(int i=0;i<iOldSize;i++){
				EnemyCartessianProductHelper2 objNewHelper=new EnemyCartessianProductHelper2(objCartessianGroups.get(i));
				objNewHelper.add(objTileDest, objTileOriginSet);
				if (objNewHelper.isValid()){
					objCartessianGroups.add(objNewHelper);
					objNewGroups.add(objNewHelper);
				}
			}
		}
		return objNewGroups;
	}

	public List<EnemyCartessianProductHelper2> getCartessianGroups() {
		return objCartessianGroups;
	}
	
	public EnemyCartessian(){
		objDestTilesProcesed=new HashSet<Tile>();
		objCartessianGroups=new ArrayList<EnemyCartessianProductHelper2>();
	}
	public EnemyCartessian(EnemyCartessian objOldCartessian){
		this.objCartessianGroups =new ArrayList<EnemyCartessianProductHelper2>(objOldCartessian.objCartessianGroups);
		this.objDestTilesProcesed=new HashSet<Tile>(objOldCartessian.objDestTilesProcesed);
	}
}


class EnemySceneHelper{
	Deque<Tile> objOrderDestTiles=new LinkedList<Tile>();
	Map<Tile,Set<Tile>> objOriginToDestTiles=new HashMap<Tile, Set<Tile>>();
	Map<Tile,Set<Tile>> objDestToOriginTiles=new HashMap<Tile, Set<Tile>>();
	
	
	
	public EnemySceneHelper() {
	}
	
	public EnemySceneHelper(EnemySceneHelper objOldHelper){
		
		this.objOrderDestTiles.addAll(objOldHelper.objOrderDestTiles);
		this.objDestToOriginTiles.putAll(objOldHelper.objDestToOriginTiles);
		for (Entry<Tile, Set<Tile>> objEntry : objOldHelper.objOriginToDestTiles.entrySet()) {
			Set<Tile> objNewSet=new HashSet<Tile>(objEntry.getValue());
			objOriginToDestTiles.put(objEntry.getKey(), objNewSet);
		}
	}
	
	public void add(Tile objDest,Set<Tile> objOriginSet){
		this.objOrderDestTiles.addLast(objDest);
		this.objDestToOriginTiles.put(objDest, objOriginSet);
		for (Tile objOriginTile : objOriginSet) {
			addOriginToDestTile(objOriginTile,objDest);
		}
		
	}
	
	private void addOriginToDestTile(Tile objOriginTile, Tile objDest) {
		if (this.objOriginToDestTiles.containsKey(objOriginTile)){
			Set<Tile> objSet=objOriginToDestTiles.get(objOriginTile);
			objSet.add(objDest);
		}else{
			Set<Tile> objSet=new HashSet<Tile>();
			objSet.add(objDest);
			this.objOriginToDestTiles.put(objOriginTile, objSet);
		}
		
	}

	public List<EnemySceneHelper> makeRoomFor(Tile objDestTile){
		List<EnemySceneHelper> objResult=new ArrayList<EnemySceneHelper>();
		if(!isValid()){
			Set<Tile> objOriginTilesForDestTile=this.objDestToOriginTiles.get(objDestTile);
			Set<Tile> objDestTilesCandidate=new HashSet<Tile>();
			for (Tile objOriginTile : objOriginTilesForDestTile) {
				objDestTilesCandidate.addAll(this.objOriginToDestTiles.get(objOriginTile));
			}
			boolean bFinded=false;
			Iterator<Tile> it=objOrderDestTiles.descendingIterator();
			while(it.hasNext()&&!bFinded){
				Tile objOtherDestTile=it.next();
				if (!objOtherDestTile.equals(objDestTile)&&objDestTilesCandidate.contains(objOtherDestTile)){
					EnemySceneHelper objNewHelper=new EnemySceneHelper(this);
					objNewHelper.removeDestTile(objOtherDestTile);
					if (objNewHelper.isValid()){
						objResult.add(objNewHelper);
						bFinded=true;
					}
				}
			}
		}
		return objResult;
	}

	private void removeDestTile(Tile objDestTile) {
		Set<Tile> objOriginTiles=objDestToOriginTiles.remove(objDestTile);
		for (Tile objOrigin : objOriginTiles) {
			Set<Tile> objOtherDestTiles=objOriginToDestTiles.get(objOrigin);
			objOtherDestTiles.remove(objDestTile);
			if (objOtherDestTiles.isEmpty()){
				objOriginToDestTiles.remove(objOrigin);
			}
		}
		
	}

	public boolean isValid(){
		return objOriginToDestTiles.size()>=objDestToOriginTiles.size();
	}
	
	public int size(){
		return objDestToOriginTiles.size();
	}
	public Set<Tile> getDestTileSet(){
		return objDestToOriginTiles.keySet();
	}
	public Set<Tile> getSrcTileSet(){
		return objOriginToDestTiles.keySet();
	}
	
	
}




class TileBattleResolutionConfirmedDebilityComparator3 implements Comparator<TileBattleResolutionInfo>{

	private int countAntsMore(TileBattleResolutionInfo objResInfo){
		NavigableMap<Integer,Set<Tile>> objInverseDebilityMap=objResInfo.getInverseDebilityMap();
		objInverseDebilityMap=objInverseDebilityMap.tailMap(objResInfo.getSizeDebilityConfirmed(), false);
		int iResult=0;
		for (Set<Tile> objSet : objInverseDebilityMap.values()) {
			iResult+=objSet.size();
		}
		return iResult;
	}
	
	private int countAntsLess(TileBattleResolutionInfo objResInfo){
		NavigableMap<Integer,Set<Tile>> objInverseDebilityMap=objResInfo.getInverseDebilityMap();
		objInverseDebilityMap=objInverseDebilityMap.headMap(objResInfo.getSizeDebilityConfirmed(), false);
		int iResult=0;
		for (Set<Tile> objSet : objInverseDebilityMap.values()) {
			iResult+=objSet.size();
		}
		return iResult;
	}

	
	private int countAntsEqual(TileBattleResolutionInfo objResInfo){
		NavigableMap<Integer,Set<Tile>> objInverseDebilityMap=objResInfo.getInverseDebilityMap();
		if (objInverseDebilityMap.containsKey(objResInfo.getSizeDebilityConfirmed())){
			return objInverseDebilityMap.get(objResInfo.getSizeDebilityConfirmed()).size();
		}else{
			return 0;
		}
	}
	
	public int compareInverseMap(NavigableMap<Integer,Set<Tile>> objMap1,NavigableMap<Integer,Set<Tile>> objMap2){
		Integer iHigher1=objMap1.lastKey();
		Integer iHigher2=objMap2.lastKey();
		Integer iResult=null;
		while (iResult==null){
			if (iHigher1==null && iHigher2==null){
				iResult=0;
			}else if (iHigher1!=null && iHigher2==null){
				iResult=-1;
			}else  if (iHigher2!=null && iHigher1==null){
				iResult=1;
			}else{
				int iBaseHigher1=iHigher1;
				int iBaseHigher2=iHigher2;
				if (iBaseHigher1==iBaseHigher2){
					int iSize1=objMap1.get(iHigher1).size();
					int iSize2=objMap1.get(iHigher1).size();
					if (iSize1>iSize2){
						iResult= -1;
					}else if (iSize2>iSize1){
						iResult= 1;
					}else{
						iHigher1=objMap1.lowerKey(iHigher1);
						iHigher2=objMap1.lowerKey(iHigher2);
						
					}
				}else if (iBaseHigher1>iBaseHigher2){
					iResult= -1;
				}else{
					iResult= 1;
				}
			}
			
		}
		return iResult;
		
	}
	@Override
	public int compare(TileBattleResolutionInfo objO1, TileBattleResolutionInfo objO2) {
		int iMore1=countAntsMore(objO1);
		int iMore2=countAntsMore(objO2);
		int iEqual1=countAntsEqual(objO1);
		int iEqual2=countAntsEqual(objO2);
		int iLess1=countAntsLess(objO1);
		int iLess2=countAntsLess(objO2);
		
		if (iLess1==0 && iLess2!=0){
			return -1;
		}else if (iLess2==0 && iLess1!=0){
				return 1;
		}else if (iLess1==0 && iLess2==0){
		
			if (iEqual1==0 && iEqual2!=0){
				return -1;
			}else if (iEqual2==0 && iEqual1!=0){
				return 1;
				
			}else if (iEqual1==0 && iEqual2==0){
				if (iMore1>iMore2){
					return -1;
				}else if (iMore2>iMore1){
					return 1;
				}else{
					return compareInverseMap(objO1.getInverseDebilityMap().tailMap(objO1.getSizeDebilityConfirmed(), false), objO2.getInverseDebilityMap().tailMap(objO2.getSizeDebilityConfirmed(), false));
				}
			}else{
				if (iMore1>iMore2){
					return -1;
				}else if (iMore2>iMore1){
					return 1;
				}else if (iEqual1>iEqual2){
					return -1;
				}else if (iEqual2>iEqual1){
					return 1;
				}else{
					return compareInverseMap(objO1.getInverseDebilityMap().tailMap(objO1.getSizeDebilityConfirmed(), true), objO2.getInverseDebilityMap().tailMap(objO2.getSizeDebilityConfirmed(), true));
				}
			}
		}else{
			if (iLess1<iLess2){
				return -1;
			}else if (iLess2<iLess1){
				return 1;
			}else{
				return 0;
			}
		}
	}
	
}



class TileSetPosibleImprovementsComparator implements Comparator<Collection<TileAimKey>>{
	Set<TileAimKey> objConfirmedDebility;
	Map<TileAimKey,Integer> objNumTimesPosibleImprovements;
	Ants ants;
	Map<Tile,Integer> objCalculatedDistance=new HashMap<Tile, Integer>();
	
	
	/**
	 * @param objConfirmedDebility
	 * @param objNumTimesPosibleImprovements
	 * @param objAnts
	 */
	public TileSetPosibleImprovementsComparator(Set<TileAimKey> objConfirmedDebility, Map<TileAimKey, Integer> objNumTimesPosibleImprovements,
			Ants objAnts) {
		this.objConfirmedDebility = objConfirmedDebility;
		this.objNumTimesPosibleImprovements = objNumTimesPosibleImprovements;
		ants = objAnts;
	}
	@Override
	public int compare(Collection<TileAimKey> objO1, Collection<TileAimKey> objO2) {
		TileAimKey objTileAimKey1=objO1.iterator().next();
		TileAimKey objTileAimKey2=objO2.iterator().next();
		int iNumTimes1=objNumTimesPosibleImprovements.get(objTileAimKey1);
		int iNumTimes2=objNumTimesPosibleImprovements.get(objTileAimKey2);
		
		if (iNumTimes1>iNumTimes2){
			return -1;
		}else if (iNumTimes2>iNumTimes1){
			return 1;
		}else{
			int iDistance1=calculateMinDistance(objTileAimKey1);
			int iDistance2=calculateMinDistance(objTileAimKey2);
			if (iDistance1<iDistance2){
				return -1;
			}else if (iDistance2<iDistance1){
				return 1;
			}else{
				return 0;
			}
		}
		
	}
	

	private int calculateMinDistance(TileAimKey objTileAimKey){
		
		if (objCalculatedDistance.containsKey(objTileAimKey.getTileDest())){
			return objCalculatedDistance.get(objTileAimKey.getTileDest());
		}else{
			Integer iDistance=null;
			Iterator<TileAimKey> it=objConfirmedDebility.iterator();
			while (it.hasNext()&&(iDistance==null||iDistance>1)){
				TileAimKey objCurrent=it.next();
				int iCurrentDistance=ants.getManhattanDistance(objCurrent.getTileDest(), objTileAimKey.getTileDest());
				if (iDistance==null||iCurrentDistance<iDistance){
					iDistance=iCurrentDistance;
				}
			}
			if (iDistance==null){
				iDistance=0;
			}
			objCalculatedDistance.put(objTileAimKey.getTileDest(), iDistance);
			return iDistance;
		}
		
		
	}
	
	
	
}
