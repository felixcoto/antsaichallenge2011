package es.cccf.ants.battleplan;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.aichallenge.ants.Tile;

import es.cccf.ants.spacepartitioning.TileAimKey;

class CombatStatistics{
	private int iTotalAlly=0;
	private int iEnemyDead=0;
	private int iAllyDead=0;
	private double dKillRatio=0;
	private double dSurviveRatio=0;
	private double dDeadRatio=0;
	private Map<Tile,Integer> objEnemyDebilityMap=new HashMap<Tile, Integer>();
	private Map<Tile,Integer> objAllyDebilityMap=new HashMap<Tile, Integer>();
	private Map<Tile,Set<Tile>> objCombatRelations=new HashMap<Tile, Set<Tile>>();
	private Set<Tile> objEnemyTiles;
	
	public CombatStatistics() {
		// TODO Auto-generated constructor stub
	}
	
	public CombatStatistics(Set<Tile> objEnemyDestTile,Map<Tile,TileBattleResolutionInfo> objBattleResolution,int iTotalAlly,Set<Tile> objEnemyTiles){
		for (Tile objEnemyTileDest : objEnemyDestTile) {
			TileBattleResolutionInfo objResInfo=objBattleResolution.get(objEnemyTileDest);
			this.addEnemyDebility(objEnemyTileDest, objResInfo.getSizeDebilityConfirmed());
			for (TileAimKey objTileAimKey:objResInfo.getConfirmedDebility()) {
				this.addCombatRelation(objEnemyTileDest, objTileAimKey.getTile());
				this.addAllyDebilitySum1(objTileAimKey.getTile());
			}
		}
		this.objEnemyTiles=objEnemyTiles;
		this.setTotalAlly(iTotalAlly);
		this.computeCombat();

	}
	
	public void setTotalAlly(int iTotalAlly){
		this.iTotalAlly=iTotalAlly;
	}
	
	public void addEnemyDebility(Tile objEnemy,int iDebility){
		objEnemyDebilityMap.put(objEnemy,iDebility);
	}
	
	public void addAllyDebilitySum1(Tile objAlly){
		int iDebility=0;
		if (objAllyDebilityMap.containsKey(objAlly)){
			iDebility=objAllyDebilityMap.get(objAlly);
		}
		iDebility++;
		objAllyDebilityMap.put(objAlly, iDebility);
	}
	
	public void addCombatRelation(Tile objTile1,Tile objTile2){
		addCombat(objTile1, objTile2);
		addCombat(objTile2, objTile1);
	}
	private void addCombat(Tile objTile1,Tile objTile2){
		if (objCombatRelations.containsKey(objTile1)){
			objCombatRelations.get(objTile1).add(objTile2);
		}else{
			Set<Tile> objNewSet=new HashSet<Tile>();
			objNewSet.add(objTile2);
			objCombatRelations.put(objTile1, objNewSet);
		}
	}
	
	public void computeCombat(){
		for (Entry<Tile, Integer> objEntry : objEnemyDebilityMap.entrySet()) {
			int iDebility=objEntry.getValue();
			boolean bDead=false;
			Iterator<Tile> itAlly=objCombatRelations.get(objEntry.getKey()).iterator();
			while(itAlly.hasNext()&&!bDead){
				Tile objAlly=itAlly.next();
				int iDebilityAlly=objAllyDebilityMap.get(objAlly);
				if (iDebilityAlly<=iDebility){
					bDead=true;
				}
			}
			if (bDead){
				this.iEnemyDead++;
			}
		}
		
		for (Entry<Tile, Integer> objEntry : objAllyDebilityMap.entrySet()) {
			int iDebility=objEntry.getValue();
			boolean bDead=false;
			Iterator<Tile> itEnemy=objCombatRelations.get(objEntry.getKey()).iterator();
			while(itEnemy.hasNext()&&!bDead){
				Tile objEnemy=itEnemy.next();
				int iDebilityEnemy=objEnemyDebilityMap.get(objEnemy);
				if (iDebilityEnemy<=iDebility){
					bDead=true;
				}
			}
			if (bDead){
				this.iAllyDead++;
			}
		}
		
		dKillRatio=((double)iEnemyDead)/((double)objEnemyDebilityMap.size());
		dSurviveRatio=((double)iTotalAlly-iAllyDead)/((double)iTotalAlly);
		dDeadRatio=((double)iEnemyDead)/((double)iEnemyDead+iAllyDead);

	}

	@Override
	public String toString() {
		return String.format("CombatStatistics [iTotalAlly=%s, iAllyDead=%s, iEnemyDead=%s, dKillRatio=%s, dSurviveRatio=%s, dRatioDead=%s]",
				iTotalAlly, iAllyDead, iEnemyDead, dKillRatio, dSurviveRatio, dDeadRatio);
	}

	public double getSurviveRatio() {
		return dSurviveRatio;
	}

	public double getDeadRatio() {
		return dDeadRatio;
	}

	public Set<Tile> getEnemyTiles() {
		return objEnemyTiles;
	}

	public int getEnemyDead() {
		return iEnemyDead;
	}

	public int getAllyDead() {
		return iAllyDead;
	}
	
	
	
}