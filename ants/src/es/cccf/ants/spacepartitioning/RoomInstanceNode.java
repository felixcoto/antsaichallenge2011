package es.cccf.ants.spacepartitioning;

import java.util.NavigableSet;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Tile;

public class RoomInstanceNode {
	private RoomInstanceKey objKey;
	
	private Tile objTile;
	private Tile objTile2;
	private int iCost=0;
	
	private int iDepth=0;
	private Aim aim;
	private NavigableSet<Integer> objConnectedTilesBetweenRooms;
	private RoomInstanceNode objParent=null;
	private AStarState aStarState;
	

	

	/**
	 * @param objRoom
	 * @param objTile
	 * @param objTile2
	 * @param objHorizontalComponent TODO
	 * @param objVerticalComponent TODO
	 * @param objParent
	 */
	public RoomInstanceNode(Room<RoomInfoPartition> objRoom,RoomMovementType objMovementType, Tile objTile, Tile objTile2,  RoomInstanceNode objParent) {
		this.objKey=new RoomInstanceKey(objRoom, objMovementType);
		this.objTile = objTile;
		this.objTile2 = objTile2;
		this.objParent = objParent;
		if (this.objParent!=null){
			this.aStarState=objParent.getaStarState();
		}
	}


	
	public RoomInstanceNode(Room<RoomInfoPartition> objRoom,RoomMovementType objMovementType, Tile objTile,AStarState aStarState){
		this(objRoom,objMovementType,objTile,null,null);
		this.aStarState=aStarState;
	}

	public int getCost() {
		return iCost;
	}

	public void setCost(int objCost) {
		iCost = objCost;
	}

	public int getHeuristic() {
		return this.aStarState.getHeuristic(objTile);
	}


	public int getDepth() {
		return iDepth;
	}

	public void setDepth(int objDepth) {
		iDepth = objDepth;
	}

	public Room<RoomInfoPartition> getRoom() {
		return objKey.getRoom();
	}

	public Tile getTile() {
		return objTile;
	}


	public RoomInstanceNode getParent() {
		return objParent;
	}


	public Tile getTile2() {
		return objTile2;
	}



	public void setTile2(Tile objTile2) {
		this.objTile2 = objTile2;
	}



	public void setTile(Tile objTile) {
		this.objTile = objTile;
	}



	public RoomInstanceKey getKey() {
		return objKey;
	}





	public RoomMovementType getMovementType() {
		return objKey.getMovementType();
	}



	@Override
	public String toString() {
		return String.format("RoomInstanceNode [objKey=%s, objTile=%s, iCost=%s, iHeuristic=%s]", objKey, objTile, iCost, getHeuristic());
	}
	
	public int getTotalEstimatedCost(){
		return this.iCost+this.getHeuristic();
	}



	public AStarState getaStarState() {
		return aStarState;
	}
	
}