package es.cccf.ants.spacepartitioning;

import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.aichallenge.ants.Tile;

import es.cccf.ants.spacepartitioning.RoomVisitor.VisitChild;

public class Room<T>{
	// general
	int iWidth;
	int iHeight;

	int iNorthLimit;
	int iSouthLimit;
	int iWestLimit;
	int iEastLimit;

	
	// Type of Room
	boolean bRoot;
	boolean bLeaf;
	
	// if not Leaf
	Partition partitionType;
	int iPartitionLimit;
	Room<T> objRoomLessOrEqual; // If is horizontal partition means North partition, if is vertical partition means West partition
	Room<T> objRoomMore; // If is horizontal partition means South partition, if is vertical partition means East partition

	
	

	
	
	
	//If is not Root
	Room<T> objRoot;
	Room<T> objParent;

	T objLoad;

	
	
	/**
	 * Constructor for Root
	 * @param width width of the room
	 * @param height
	 */
	public Room (int iWidth,int iHeight){
		this.iWidth=iWidth;
		this.iHeight=iHeight;
		this.iNorthLimit=0;
		this.iSouthLimit=iHeight-1;
		this.iWestLimit=0;
		this.iEastLimit=iWidth-1;
		
		
		this.bRoot=true;
		this.bLeaf=true;
		
		
		
		objRoot=this;
		objParent=null;
		
	}
	
	private Room (int iNorthLimit,int iSouthLimit,int iWestLimit,int iEastLimit,Room<T> objRoot,Room<T> objParent){
		this.iNorthLimit=iNorthLimit;
		this.iSouthLimit=iSouthLimit;
		this.iWestLimit=iWestLimit;
		this.iEastLimit=iEastLimit;
		this.iWidth=iEastLimit-iWestLimit+1;
		this.iHeight=iSouthLimit-iNorthLimit+1;
		this.objRoot=objRoot;
		this.objParent=objParent;
		this.bRoot=false;
		this.bLeaf=true;
		
	}
	
	
	public void splitRoom(Partition partitionType,int iPartitionLimit){
		if (!this.bLeaf){
			throw new UnsupportedOperationException("Only in leaf");
		}
		
		this.bLeaf=false;
		this.partitionType=partitionType;
		this.iPartitionLimit=iPartitionLimit;
		
		switch(this.partitionType){
		case HORIZONTAL:
			objRoomLessOrEqual =new Room<T>(this.iNorthLimit,iPartitionLimit,this.iWestLimit,this.iEastLimit,this.objRoot,this);
			objRoomMore        =new Room<T>(iPartitionLimit+1, this.iSouthLimit,this.iWestLimit,this.iEastLimit,this.objRoot,this);
			break;
		case VERTICAL:
			objRoomLessOrEqual =new Room<T>(this.iNorthLimit, this.iSouthLimit,this.iWestLimit,iPartitionLimit,this.objRoot,this);
			objRoomMore        =new Room<T>(this.iNorthLimit, this.iSouthLimit,iPartitionLimit+1,this.iEastLimit,this.objRoot,this);
		}
		
	}

	
	public void mergeChildRooms(){
		if (this.bLeaf){
			throw new UnsupportedOperationException("Only in not leaf");
		}
		this.bLeaf=true;
		this.objRoomLessOrEqual=null;
		this.objRoomMore=null;
		this.partitionType=null;
	}
	
	
	
	public int getSize(){
		return this.iHeight*this.iWidth;
	}
	
	public boolean isValidTileForRoom(Tile objTile){
		return objTile.getRow()>=iNorthLimit&&objTile.getRow()<=iSouthLimit && objTile.getCol()>=iWestLimit && objTile.getCol()<=iEastLimit;
	}
	
	
	public Room<T> getRoomAtTile(Tile objTile){
		if (!this.bRoot){
			throw new UnsupportedOperationException("Only in root");
		}
		
		ListLeafVisitor<T> objListLeaf=new ListLeafVisitor<T>(objTile.getRow(),objTile.getRow(),objTile.getCol(),objTile.getCol());
		 this.accept(objListLeaf);
		 List<Room<T>> objRoomList=objListLeaf.getResult();
		 Room<T> objResult=null;
		 if (!objRoomList.isEmpty()){
			 objResult=objRoomList.get(0);
		 }
		return objResult;
		

	}
	
	public List<Room<T>> getRoomListInRange(int iNorthLimit, int iSouthLimit, int iWestLimit, int iEastLimit){
		ListLeafVisitor<T> objVisitor=new ListLeafVisitor<T>(iNorthLimit, iSouthLimit, iWestLimit, iEastLimit);
		this.accept(objVisitor);
		return objVisitor.getResult();
		
	}
	
	
	public void accept(RoomVisitor<T> objRoomVisitor){
		Deque<Room<T>> objRemainingRooms=new LinkedList<Room<T>>();
		objRemainingRooms.add(this);
		while (!objRemainingRooms.isEmpty()){
			Room<T> objCurrentRoom=objRemainingRooms.removeFirst();
			Set<VisitChild> objVisitSet=objRoomVisitor.visited(objCurrentRoom);
			if (!objCurrentRoom.isLeaf()){
				for (VisitChild objVisitChild : objVisitSet) {
					switch(objVisitChild){
					case LOWER_OR_EQUAL:
						objRemainingRooms.add(objCurrentRoom.getRoomLessOrEqual());
						break;
					case MORE:
						objRemainingRooms.add(objCurrentRoom.getRoomMore());

					}
				}
			}
		}

	}
	
	

	public boolean isRoot() {
		return bRoot;
	}

	public boolean isLeaf() {
		return bLeaf;
	}

	public Partition getPartitionType() {
		return partitionType;
	}

	public int getPartitionLimit() {
		return iPartitionLimit;
	}

	public Room<T> getRoomLessOrEqual() {
		return objRoomLessOrEqual;
	}

	public Room<T> getRoomMore() {
		return objRoomMore;
	}

	public Room<T> getParent() {
		return objParent;
	}


	public int getNorthLimit() {
		return iNorthLimit;
	}

	public int getSouthLimit() {
		return iSouthLimit;
	}

	public int getWestLimit() {
		return iWestLimit;
	}

	public int getEastLimit() {
		return iEastLimit;
	}

	public T getLoad() {
		return objLoad;
	}

	public void setLoad(T objLoad) {
		this.objLoad = objLoad;
	}

	public int getWidth() {
		return iWidth;
	}

	public int getHeight() {
		return iHeight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + iEastLimit;
		result = prime * result + iNorthLimit;
		result = prime * result + iSouthLimit;
		result = prime * result + iWestLimit;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (iEastLimit != other.iEastLimit)
			return false;
		if (iNorthLimit != other.iNorthLimit)
			return false;
		if (iSouthLimit != other.iSouthLimit)
			return false;
		if (iWestLimit != other.iWestLimit)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Room [iNorthLimit=%s, iSouthLimit=%s, iWestLimit=%s, iEastLimit=%s, iPartitionLimit=%s, partitionType=%s, objLoad=%s]",
				iNorthLimit, iSouthLimit, iWestLimit, iEastLimit, iPartitionLimit, partitionType, objLoad);
	}
	
	
}


class ListLeafVisitor<T> extends AbstractRangeRoomVisitor<T>{

	List<Room<T>> objResult;
	
	public ListLeafVisitor(int iNorthLimit, int iSouthLimit, int iWestLimit, int iEastLimit) {
		super(iNorthLimit, iSouthLimit, iWestLimit, iEastLimit);
		objResult=new LinkedList<Room<T>>();
	}



	

	public List<Room<T>> getResult() {
		return objResult;
	}


	@Override
	protected void doVisit(Room<T> objRoom) {
		if (objRoom.isLeaf()){
			objResult.add(objRoom);
		}
	}


	
}