package es.cccf.ants.spacepartitioning;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

public class RoomExplorationInfo {

	private Ants ants;
	private Room<RoomExplorationInfo> objRoom;
	private NavigableMap<Integer,Integer> objHistoricOccupation;
	private Tile objCenter;
	private Tile objDestination;
	private Set<Tile> objFreeTiles;
	private Set<Tile> objAntTiles;
	private int iLastTurn;
	
	public RoomExplorationInfo(Ants objAnts, Room<RoomExplorationInfo> objRoom) {
		ants=objAnts;
		this.objRoom=objRoom;
		this.objFreeTiles=new HashSet<Tile>();
		this.objAntTiles=new HashSet<Tile>();
		this.objHistoricOccupation=new TreeMap<Integer, Integer>();
		int iRow=objRoom.getNorthLimit()+Math.round(objRoom.getHeight()/2.0f);
		int iCol=objRoom.getWestLimit()+Math.round(objRoom.getWidth()/2.0f);
		objDestination=new Tile(iRow,iCol);
		objCenter=objDestination;
		for (iRow=this.objRoom.getNorthLimit();iRow<=objRoom.getSouthLimit();iRow++){
			for (iCol=this.objRoom.getWestLimit();iCol<=objRoom.getEastLimit();iCol++){
				objFreeTiles.add(new Tile(iRow,iCol));
			}
		}
		iLastTurn=ants.getCurrentTurn();
		
	}
	
	public void addWater(Tile objTile){
		checkTurn();
		objFreeTiles.remove(objTile);
		if (objTile.equals(objDestination)){
			int iDistance=0;
			Tile objCloser=null;
			Iterator<Tile> it=objFreeTiles.iterator();
			while (it.hasNext()&&iDistance!=1){
				Tile objCurrentFreeTile=it.next();	
				if (objCloser==null){
					objCloser=objCurrentFreeTile;
					iDistance=ants.getDistance(objCurrentFreeTile, objCenter);
				}else{
					int iCurrentDistance=ants.getDistance(objCurrentFreeTile, objCenter);
					if (iCurrentDistance<iDistance){
						objCloser=objCurrentFreeTile;
						iDistance=iCurrentDistance;
					}
				}
			}
			objDestination=objCloser;
		}
	}
	
	private void checkTurn(){
		if (iLastTurn!=ants.getCurrentTurn()){
			iLastTurn=ants.getCurrentTurn();
			objAntTiles.clear();
		}
	}
	
	public void addAnt(Tile objMyAnt){
		checkTurn();
		objAntTiles.add(objMyAnt);
		int iCurrentTurn=ants.getCurrentTurn();
		int iOccupation=1;
		if (objHistoricOccupation.containsKey(iCurrentTurn)){
			iOccupation=objHistoricOccupation.get(iCurrentTurn)+1;
		}
		objHistoricOccupation.put(iCurrentTurn, iOccupation);
	}
	
	public double calculateMeanOccupation(int iNumberTurns){
		checkTurn();
		int iLastTurnToSee=ants.getCurrentTurn()-iNumberTurns;
		NavigableMap<Integer, Integer> objSubMap=objHistoricOccupation.tailMap(iLastTurnToSee, true);
		double occupation=0;
		for (Integer iCurrentOccupation : objSubMap.values()) {
			occupation+=iCurrentOccupation;
		}
		return occupation/iNumberTurns;
	}
	
	public boolean isVisited(){
		return !objHistoricOccupation.isEmpty();
	}

	public Tile getDestination() {
		return objDestination;
	}

	public Set<Tile> getAntTiles() {
		return objAntTiles;
	}

	public void setAntTiles(Set<Tile> objAntTiles) {
		this.objAntTiles = objAntTiles;
	}

	public int getNorthLimit() {
		return objRoom.getNorthLimit();
	}

	public int getSouthLimit() {
		return objRoom.getSouthLimit();
	}

	public int getWestLimit() {
		return objRoom.getWestLimit();
	}

	public int getEastLimit() {
		return objRoom.getEastLimit();
	}

	@Override
	public String toString() {
		return String
				.format("RoomExplorationInfo [getNorthLimit()=%s, getSouthLimit()=%s, getWestLimit()=%s, getEastLimit()=%s, objDestination=%s, objCenter=%s, objFreeTiles=%s, objAntTiles=%s, objHistoricOccupation=%s]",
						getNorthLimit(), getSouthLimit(), getWestLimit(), getEastLimit(), objDestination, objCenter, objFreeTiles, objAntTiles,
						objHistoricOccupation);
	}
	

}
