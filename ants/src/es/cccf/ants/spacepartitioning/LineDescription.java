package es.cccf.ants.spacepartitioning;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

class LineDescription{
	NavigableSet<Integer> objUpperLimits;
	NavigableSet<Integer> objLowerLimits;
	NavigableSet<Integer> objOccupied;
	NavigableSet<Integer> objFree;
	
	public LineDescription(int iLength) {
		objUpperLimits=new TreeSet<Integer>();
		objLowerLimits=new TreeSet<Integer>();
		objOccupied=new TreeSet<Integer>();
		objFree=new TreeSet<Integer>();
		for (int i=0;i<iLength;i++){
			objFree.add(i);
		}
	}
	
	
	public void addOcupied(Integer iPos){
		objOccupied.add(iPos);
		objFree.remove(iPos);
		Integer iUpperLimitNext=objUpperLimits.higher(iPos);
		Integer iLowerLimitNext=objLowerLimits.lower(iPos);
		
		boolean bAdjacentUpperLimits=false;
		if (iUpperLimitNext!=null){
			bAdjacentUpperLimits=iUpperLimitNext-iPos==1?true:false;
		}
		
		boolean bAdjacentLowerLimits=false;
		if (iLowerLimitNext!=null){
			bAdjacentLowerLimits=iPos-iLowerLimitNext==1?true:false;
		}
		
		if (bAdjacentLowerLimits&&bAdjacentUpperLimits){
			objUpperLimits.remove(iUpperLimitNext);
			objLowerLimits.remove(iLowerLimitNext);
		}else if (!bAdjacentLowerLimits&&!bAdjacentUpperLimits){
			objLowerLimits.add(iPos);
			objUpperLimits.add(iPos);
		}else{
			if (bAdjacentLowerLimits){
				objLowerLimits.remove(iLowerLimitNext);
				objLowerLimits.add(iPos);
			}
			if (bAdjacentUpperLimits){
				objUpperLimits.remove(iUpperLimitNext);
				objUpperLimits.add(iPos);
			}
		}
	}
	
	public void merge(LineDescription objLineDescription){
		this.objUpperLimits.addAll(objLineDescription.objUpperLimits);
		this.objLowerLimits.addAll(objLineDescription.objLowerLimits);
	}


	public Integer nextHighLimit(Integer objE) {
		return objUpperLimits.higher(objE);
	}


	public Integer nextLowLimit(Integer objE) {
		return objLowerLimits.lower(objE);
	}


	public NavigableSet<Integer> getUpperLimits() {
		return objUpperLimits;
	}


	public NavigableSet<Integer> getLowerLimits() {
		return objLowerLimits;
	}


	public NavigableSet<Integer> getOccupied() {
		return objOccupied;
	}


	public NavigableSet<Integer> getFree() {
		return objFree;
	}
	

	
	
	
}