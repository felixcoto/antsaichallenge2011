package es.cccf.ants.spacepartitioning;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

public class AntsSpacePartitioningTest2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File objFileInit = new File(args[0]);
		File[] aFile;

		if (objFileInit.isDirectory()) {
			aFile = objFileInit.listFiles(new FileFilter() {

				@Override
				public boolean accept(File objPathname) {
					return objPathname.getName().endsWith(".map");
				}
			});
		} else {
			aFile = new File[] { objFileInit };
		}
		for (File objFile : aFile) {

			try {
				Scanner objScanner = new Scanner(objFile);
				int rows;
				int cols;
				List<Tile> objTileList;
				objScanner.findInLine("rows");
				rows = objScanner.nextInt();
				objScanner.nextLine();
				objScanner.findInLine("cols");
				cols = objScanner.nextInt();
				objTileList = new ArrayList<Tile>();
				objScanner.nextLine();
				objScanner.nextLine();
				int iLine = 0;
				while (objScanner.hasNext("m")) {
					String mapIndicator = objScanner.next("m");
					String sLineMap = objScanner.next();
					char[] aChar = sLineMap.toCharArray();

					for (int i = 0; i < aChar.length; i++) {
						if (aChar[i] == '%')
							objTileList.add(new Tile(iLine, i));
					}
					iLine++;
					objScanner.nextLine();
				}
				System.out.println("Nombre Mapa:" + objFile.getName());
				System.out.println("Numero Tiles=" + objTileList.size());
				Ants ants=new Ants(0,0,rows,cols,900,77,5,1,42);
				AntsSpacePartitioning antsSpacePartitioning = new AntsSpacePartitioning(ants);

				for (Tile objTile : objTileList) {
					antsSpacePartitioning.addOccupiedBlock(objTile);
				}
				long lInicio = System.currentTimeMillis();
				antsSpacePartitioning.processUpdate();
				long lFin = System.currentTimeMillis();
				System.out.println("Tiempo: " + (lFin - lInicio));
				ListLeafVisitor<RoomInfoPartition> objVisitor = new ListLeafVisitor<RoomInfoPartition>(0, rows - 1, 0, cols - 1);
				antsSpacePartitioning.objRoomRoot.accept(objVisitor);
				List<Room<RoomInfoPartition>> objLeafRooms = new ArrayList<Room<RoomInfoPartition>>(objVisitor.getResult());
				Collections.sort(objLeafRooms, new RoomSizeComparator());
				Room<RoomInfoPartition> objSmall = objLeafRooms.get(0);
				Room<RoomInfoPartition> objBig = objLeafRooms.get(objLeafRooms.size() - 1);
				Room<RoomInfoPartition> objMedian = objLeafRooms.get(objLeafRooms.size() / 2);
				Room<RoomInfoPartition> obj90Big= objLeafRooms.get((int) Math.floor(objLeafRooms.size()*0.9));
				Room<RoomInfoPartition> obj10Big= objLeafRooms.get((int) Math.floor(objLeafRooms.size()*0.1));
				
				Room<RoomInfoPartition> objRoomTest = antsSpacePartitioning.objRoomRoot.getRoomAtTile(new Tile(14, 47));
				System.out.println(String.format("NUMERO HABITACIONES:%d", objLeafRooms.size()));
				System.out.println(String.format("GRANDE Tam=%d (%d,%d,%d,%d)", objBig.getSize(), objBig.getNorthLimit(), objBig.getSouthLimit(),
						objBig.getWestLimit(), objBig.getEastLimit()));
				System.out.println(String.format("90%% Tam=%d (%d,%d,%d,%d)", obj90Big.getSize(), obj90Big.getNorthLimit(), obj90Big.getSouthLimit(),
						obj90Big.getWestLimit(), obj90Big.getEastLimit()));

				System.out.println(String.format("MEDIANA Tam=%d (%d,%d,%d,%d)", objMedian.getSize(), objMedian.getNorthLimit(),
						objMedian.getSouthLimit(), objMedian.getWestLimit(), objMedian.getEastLimit()));
				System.out.println(String.format("10%% Tam=%d (%d,%d,%d,%d)", obj10Big.getSize(), obj10Big.getNorthLimit(), obj10Big.getSouthLimit(),
						obj10Big.getWestLimit(), obj10Big.getEastLimit()));

				System.out.println(String.format("PEQUENA Tam=%d (%d,%d,%d,%d)", objSmall.getSize(), objSmall.getNorthLimit(),
						objSmall.getSouthLimit(), objSmall.getWestLimit(), objSmall.getEastLimit()));

				
				List<Room<RoomInfoPartition>> objClosedRooms=new ArrayList<Room<RoomInfoPartition>>();
				List<Room<RoomInfoPartition>> objMixedRooms=new ArrayList<Room<RoomInfoPartition>>();
				List<Room<RoomInfoPartition>> objOpenRooms=new ArrayList<Room<RoomInfoPartition>>();
				
				for (Room<RoomInfoPartition> objRoom : objLeafRooms) {
					RoomInfoPartition objInfo=objRoom.getLoad();
					if (objInfo.getNumBlockedTiles()==0){
						objOpenRooms.add(objRoom);
					}else if (objInfo.getNumBlockedTiles()==objRoom.getSize()){
						objClosedRooms.add(objRoom);
					}else{
						objMixedRooms.add(objRoom);
					}
				}
				
				
				System.out.println(String.format("Numero de habitaciones segun ocupacion. Abiertas: %d, Cerradas %d, Mixtas %d",objOpenRooms.size(),objClosedRooms.size(),objMixedRooms.size()));
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

}

class RoomSizeComparator implements Comparator<Room<RoomInfoPartition>> {

	@Override
	public int compare(Room<RoomInfoPartition> objO1, Room<RoomInfoPartition> objO2) {
		return objO1.getSize() - objO2.getSize();
	}

}
