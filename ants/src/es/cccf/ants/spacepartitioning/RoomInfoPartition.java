package es.cccf.ants.spacepartitioning;

import java.util.HashSet;
import java.util.Set;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;


public class RoomInfoPartition {
	private int iNumBlockedTiles;

	private boolean bDirty;
	private boolean bRecalculatedEntropy;
	
	private double dEntropy;
	
	private double dInformationGain;
	private double dInformationGainRatio;

	private boolean bSingleTileConnected=false;
	private boolean bSingleRoomConnected=false;
	
	private int iLastTurn=0;
	private Set<Tile> objMyAnts=new HashSet<Tile>();
	private Set<Tile> objEnemyAnts=new HashSet<Tile>();
	private Set<Tile> objMyHills=new HashSet<Tile>();
	private Set<Tile> objEnemyHills=new HashSet<Tile>();
	
	
	/**
	 * @param objNumBlockedTiles
	 * @param objDEntropy
	 */
	public RoomInfoPartition(int objNumBlockedTiles, double objDEntropy) {
		iNumBlockedTiles = objNumBlockedTiles;
		dEntropy = objDEntropy;
		bDirty=false;
		bRecalculatedEntropy=true;
	}

	public int getNumBlockedTiles() {
		return iNumBlockedTiles;
	}

	public void setNumBlockedTiles(int objNumBlockedTiles) {
		iNumBlockedTiles = objNumBlockedTiles;
	}

	public void addBlockedTile(){
		iNumBlockedTiles++;
	}
	
	public boolean isDirty() {
		return bDirty;
	}

	public void setDirty(boolean objDirty) {
		bDirty = objDirty;
	}

	public double getEntropy() {
		return dEntropy;
	}

	public void setEntropy(double objDEntropy) {
		dEntropy = objDEntropy;
	}

	public double getInformationGain() {
		return dInformationGain;
	}

	public void setInformationGain(double objDGain) {
		dInformationGain = objDGain;
	}

	public boolean isRecalculatedEntropy() {
		return bRecalculatedEntropy;
	}

	public void setRecalculatedEntropy(boolean objRecalculatedEntropy) {
		bRecalculatedEntropy = objRecalculatedEntropy;
	}

	public double getInformationGainRatio() {
		return dInformationGainRatio;
	}

	public void setInformationGainRatio(double objDInformationGainRatio) {
		dInformationGainRatio = objDInformationGainRatio;
	}

	public boolean isSingleTileConnected() {
		return bSingleTileConnected;
	}

	public void setSingleTileConnected(boolean objSingleTileConnected) {
		bSingleTileConnected = objSingleTileConnected;
	}

	public boolean isSingleRoomConnected() {
		return bSingleRoomConnected;
	}

	public void setSingleRoomConnected(boolean objSingleRoomConnected) {
		bSingleRoomConnected = objSingleRoomConnected;
	}

	@Override
	public String toString() {
		return String
				.format("RoomInfoPartition [iNumBlockedTiles=%s, dEntropy=%s, dInformationGain=%s, dInformationGainRatio=%s, bSingleTileConnected=%s, bSingleRoomConnected=%s]",
						iNumBlockedTiles, dEntropy, dInformationGain, dInformationGainRatio, bSingleTileConnected, bSingleRoomConnected);
	}

	private void checkTurn(Ants ants){
		if (iLastTurn!=ants.getCurrentTurn()){
			iLastTurn=ants.getCurrentTurn();
			objMyAnts.clear();
			objEnemyAnts.clear();
			objEnemyHills.clear();
			objMyHills.clear();
		}

	}
	
	
	public int getNumAnts(Ants ants){
		checkTurn(ants);
		return objMyAnts.size()+objEnemyAnts.size();
	}
	
	public void addAnt(Tile objAntTile,boolean bEnemy,Ants ants){
		checkTurn(ants);
		if (bEnemy){
			objEnemyAnts.add(objAntTile);
		}else{
			objMyAnts.add(objAntTile);
		}
	}
	
	public void addHill(Tile objHillTile,boolean bEnemy,Ants ants){
		checkTurn(ants);
		if (bEnemy){
			objEnemyHills.add(objHillTile);
		}else{
			objMyHills.add(objHillTile);
		}
	}

	

	public Set<Tile> getMyAnts() {
		return objMyAnts;
	}

	public Set<Tile> getEnemyAnts() {
		return objEnemyAnts;
	}

	public Set<Tile> getMyHills() {
		return objMyHills;
	}

	public Set<Tile> getEnemyHills() {
		return objEnemyHills;
	}


}
