package es.cccf.ants.spacepartitioning;

import java.util.Comparator;

class RoomInstanceComparator implements Comparator<RoomInstanceNode>{

	@Override
	public int compare(RoomInstanceNode objArg0, RoomInstanceNode objArg1) {
		int f1=objArg0.getCost()+objArg0.getHeuristic();
		int f2=objArg1.getCost()+objArg1.getHeuristic();
		if (f1<f2){
			return -1;
		}else if (f1>f2){
			return 1;
		}else if (objArg0.getCost()>objArg1.getCost()){
			return 1;
		}else if (objArg0.getCost()<objArg1.getCost()){
			return -1;
		}else{
			return 0;
		}
	}
	
}