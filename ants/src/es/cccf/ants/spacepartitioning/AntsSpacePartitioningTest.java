package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Ilk;
import org.aichallenge.ants.Tile;

public class AntsSpacePartitioningTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Tile[] aTest=new Tile[]{
				new Tile(0,0),
				new Tile(1,0),
				new Tile(2,0),
				new Tile(3,0),
				new Tile(4,0),
				new Tile(5,0),
				new Tile(6,1),
				new Tile(6,2),
				new Tile(6,3),
				new Tile(5,3),
				new Tile(4,2),
				new Tile(3,2),
				new Tile(2,2),
				new Tile(1,2)
		};
		
	int[] aRowDelta=new int[]{0,30,50,80};
	int[] aColDelta=new int[]{0,0,0,90};
//	int[] aRowDelta=new int[]{0};
//	int[] aColDelta=new int[]{0};

	int rows=100;
	int cols=100;
	Ants ants=new Ants(0,0,rows,cols,900,77,5,1,42);
	AntsSpacePartitioning antsSpacePartitioning=new AntsSpacePartitioning(ants);
	long lInicio=System.currentTimeMillis();
	ArrayList<Tile> objTiles=new ArrayList<Tile>();
	for (int i=0;i<aRowDelta.length;i++){
		int rowDelta=aRowDelta[i];
		int colDelta=aColDelta[i];
		for (Tile objTile : aTest) {
			Tile objNewTile=new Tile(objTile.getRow()+rowDelta,objTile.getCol()+colDelta);
			antsSpacePartitioning.addOccupiedBlock(objNewTile);
			objTiles.add(objNewTile);
		}
		antsSpacePartitioning.processUpdate();
	}

	long lFin=System.currentTimeMillis();
	System.out.println("Tiempo: "+(lFin-lInicio));
	ListLeafVisitor<RoomInfoPartition> objVisitor = new ListLeafVisitor<RoomInfoPartition>(0, rows - 1, 0, cols - 1);
	antsSpacePartitioning.objRoomRoot.accept(objVisitor);
	List<Room<RoomInfoPartition>> objLeafRooms = new ArrayList<Room<RoomInfoPartition>>(objVisitor.getResult());
	Collections.sort(objLeafRooms, new RoomSizeComparator());
	Room<RoomInfoPartition> objSmall = objLeafRooms.get(0);
	Room<RoomInfoPartition> objBig = objLeafRooms.get(objLeafRooms.size() - 1);
	Room<RoomInfoPartition> objMedian = objLeafRooms.get(objLeafRooms.size() / 2);

	Room<RoomInfoPartition> objRoomTest = antsSpacePartitioning.objRoomRoot.getRoomAtTile(new Tile(14, 47));
	System.out.println(String.format("NUMERO HABITACIONES:%d", objLeafRooms.size()));
	System.out.println(String.format("GRANDE Tam=%d (%d,%d,%d,%d)", objBig.getSize(), objBig.getNorthLimit(), objBig.getSouthLimit(),
			objBig.getWestLimit(), objBig.getEastLimit()));
	System.out.println(String.format("MEDIANA Tam=%d (%d,%d,%d,%d)", objMedian.getSize(), objMedian.getNorthLimit(),
			objMedian.getSouthLimit(), objMedian.getWestLimit(), objMedian.getEastLimit()));
	System.out.println(String.format("PEQUENA Tam=%d (%d,%d,%d,%d)", objSmall.getSize(), objSmall.getNorthLimit(),
			objSmall.getSouthLimit(), objSmall.getWestLimit(), objSmall.getEastLimit()));

	}

}
