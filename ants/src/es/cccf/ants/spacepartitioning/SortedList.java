package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class SortedList<E> {
	/** The list of elements */
	private ArrayList<E> list = new ArrayList<E>();
	boolean bDirty=false;
	private Comparator<E> objComparator;
	
	
	
	/**
	 * @param objComparator
	 */
	public SortedList(Comparator<E> objComparator) {
		this.objComparator = objComparator;
	}

	public E peekFirst(){
		if (bDirty){
			Collections.sort(list,objComparator);
			bDirty=false;
		}
		return list.get(0);

	}
	/**
	 * Retrieve the first element from the list
	 *  
	 * @return The first element from the list
	 */
	public E removeFirst() {
		if (bDirty){
			Collections.sort(list,objComparator);
			bDirty=false;
		}
		return list.remove(0);
	}
	
	/**
	 * Empty the list
	 */
	public void clear() {
		list.clear();
	}
	
	/**
	 * Add an element to the list - causes sorting
	 * 
	 * @param o The element to add
	 */
	public void add(E o) {
		list.add(o);
		bDirty=true;

	}
	
	public void setDirty(){
		this.bDirty=true;
	}
	
	/**
	 * Remove an element from the list
	 * 
	 * @param o The element to remove
	 */
	public void remove(Object o) {
		list.remove(o);
	}

	/**
	 * Get the number of elements in the list
	 * 
	 * @return The number of element in the list
		 */
	public int size() {
		return list.size();
	}
	
	/**
	 * Check if an element is in the list
	 * 
	 * @param o The element to search for
	 * @return True if the element is in the list
	 */
	public boolean contains(Object o) {
		return list.contains(o);
	}
}