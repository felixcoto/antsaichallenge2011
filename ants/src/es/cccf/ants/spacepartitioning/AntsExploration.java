package es.cccf.ants.spacepartitioning;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

public class AntsExploration {
	private static final int TURNS_TO_CALCULATE_MEAN=50;
	private static final double MEAN_CONFIDENCE_VALUE=0.0;
	
	private Ants ants;
	private Room<RoomExplorationInfo> objRoomRoot;
	/**
	 * @param objAnts
	 */
	public AntsExploration(Ants objAnts) {
		ants = objAnts;
		objRoomRoot=new Room<RoomExplorationInfo>(ants.getCols(), ants.getRows());
		splitRoom();
	}
	
	
	private void splitRoom(){
		objRoomRoot.accept(new SplitRoomExploration(ants));
	}
	
	
	public void addWaterTile(Tile objWaterTile){
		Room<RoomExplorationInfo> objRoom=objRoomRoot.getRoomAtTile(objWaterTile);
		objRoom.getLoad().addWater(objWaterTile);
	}
	
	public void addAnt(Tile objMyAnt){
		Room<RoomExplorationInfo> objRoom=objRoomRoot.getRoomAtTile(objMyAnt);
		objRoom.getLoad().addAnt(objMyAnt);
	}
	
	public AntsExplorationResult processExploration(){
		ProcessInfo objProcessInfo=new ProcessInfo();
		objRoomRoot.accept(objProcessInfo);
		AntsExplorationResult objResult=new AntsExplorationResult();
		double dMean=0;
		int iNumberOfRooms=0;
		for (Double dCurrentMean:objProcessInfo.getOccupationMeanMap().keySet()) {
			iNumberOfRooms++;
			dMean+=dCurrentMean;
		}
		
		if (iNumberOfRooms>0){
			NavigableMap<Double, RoomExplorationInfo> objMoreMeanRooms=null;
			NavigableMap<Double, RoomExplorationInfo> objLessMeanRooms=null;

			dMean=dMean/iNumberOfRooms;
			
			objMoreMeanRooms=objProcessInfo.getOccupationMeanMap().tailMap(dMean+MEAN_CONFIDENCE_VALUE, false);
			objLessMeanRooms=objProcessInfo.getOccupationMeanMap().headMap(dMean-MEAN_CONFIDENCE_VALUE, true);
			if (!objMoreMeanRooms.isEmpty()&&!objLessMeanRooms.isEmpty()){
				List<Tile> objListAntsInPopulatedArea=objResult.getListAntsInPopulatedArea();
				List<Tile> objListDestinationDeserted=objResult.getListDestinationDeserted();
				for (RoomExplorationInfo objInfo : objMoreMeanRooms.values()) {
					objListAntsInPopulatedArea.addAll(objInfo.getAntTiles());
				}
				for(RoomExplorationInfo objInfo:objLessMeanRooms.values()){
					objListDestinationDeserted.add(objInfo.getDestination());
				}
			}
			
		}
		List<Tile> objListDestinationUnseen=objResult.getListDestinationUnseen();
		for (RoomExplorationInfo objInfo : objProcessInfo.getUnseenRooms()) {
			objListDestinationUnseen.add(objInfo.getDestination());
		}
		return objResult;
	}
	
	private class ProcessInfo implements RoomVisitor<RoomExplorationInfo>{
		NavigableMap<Double, RoomExplorationInfo> objOccupationMeanMap=new TreeMap<Double, RoomExplorationInfo>();
		List<RoomExplorationInfo> objUnseenRooms=new LinkedList<RoomExplorationInfo>();
		@Override
		public Set<es.cccf.ants.spacepartitioning.RoomVisitor.VisitChild> visited(Room<RoomExplorationInfo> objRoom) {
			Set<VisitChild> objSet=new HashSet<RoomVisitor.VisitChild>();
			if (objRoom.isLeaf()){
				RoomExplorationInfo objInfo=objRoom.getLoad();
				if (objInfo.getDestination()!=null){
					if (!objInfo.isVisited()){

						objUnseenRooms.add(objInfo);

					}else{

						Double objMean=objInfo.calculateMeanOccupation(TURNS_TO_CALCULATE_MEAN);
						if (objMean==0){
							objUnseenRooms.add(objInfo);
						}else{
							objOccupationMeanMap.put(objMean,objInfo);
						}

					}
				}
			}else{
				objSet.add(VisitChild.LOWER_OR_EQUAL);
				objSet.add(VisitChild.MORE);
			}
			return objSet;
		}
		public NavigableMap<Double, RoomExplorationInfo> getOccupationMeanMap() {
			return objOccupationMeanMap;
		}
		public List<RoomExplorationInfo> getUnseenRooms() {
			return objUnseenRooms;
		}


	}
	
}

class SplitRoomExploration implements RoomVisitor<RoomExplorationInfo>{
	Ants ants;
	int iMaxSize;
	/**
	 * @param objAnts
	 */
	public SplitRoomExploration(Ants objAnts) {
		ants = objAnts;
		iMaxSize=(int) Math.ceil(Math.sqrt(ants.getViewRadius2()));
	}

	@Override
	public Set<es.cccf.ants.spacepartitioning.RoomVisitor.VisitChild> visited(Room<RoomExplorationInfo> objRoom) {
		Set<VisitChild> objSet=new HashSet<RoomVisitor.VisitChild>();
		if (objRoom.isLeaf()){
			if (objRoom.getHeight()>iMaxSize){
				objRoom.splitRoom(Partition.HORIZONTAL, (int) (objRoom.getNorthLimit()+(Math.round(objRoom.getHeight()/2.0))));
				objSet.add(VisitChild.LOWER_OR_EQUAL);
				objSet.add(VisitChild.MORE);
			}else if (objRoom.getWidth()>iMaxSize){
				objRoom.splitRoom(Partition.VERTICAL, (int) (objRoom.getWestLimit()+(Math.round(objRoom.getWidth()/2.0))));
				objSet.add(VisitChild.LOWER_OR_EQUAL);
				objSet.add(VisitChild.MORE);
			}else{
				objRoom.setLoad(new RoomExplorationInfo(ants,objRoom));
			}
		}else{
			objSet.add(VisitChild.LOWER_OR_EQUAL);
			objSet.add(VisitChild.MORE);
			
		}
		return objSet;
	}
	
	
}