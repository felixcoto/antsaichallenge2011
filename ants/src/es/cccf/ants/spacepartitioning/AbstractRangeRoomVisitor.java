package es.cccf.ants.spacepartitioning;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractRangeRoomVisitor<T> implements RoomVisitor<T>{
	int iNorthLimit;
	int iSouthLimit;
	int iWestLimit;
	int iEastLimit;
	
	
	/**
	 * @param objNorthLimit
	 * @param objSouthLimit
	 * @param objWestLimit
	 * @param objEastLimit
	 */
	public AbstractRangeRoomVisitor(int objNorthLimit, int objSouthLimit, int objWestLimit, int objEastLimit) {
		iNorthLimit = objNorthLimit;
		iSouthLimit = objSouthLimit;
		iWestLimit = objWestLimit;
		iEastLimit = objEastLimit;
	}


	protected abstract void doVisit(Room<T> objRoom);


	@Override
	public Set<VisitChild> visited(Room<T> objRoom) {
		Set<VisitChild> objResult=new HashSet<RoomVisitor.VisitChild>();
		doVisit(objRoom);
		if (!objRoom.isLeaf()){
			int iLowValue=0;
			int iHighValue=0;

			switch(objRoom.getPartitionType()){
			case HORIZONTAL:
				iLowValue=iNorthLimit;
				iHighValue=iSouthLimit;
				break;
			case VERTICAL:
				iLowValue=iWestLimit;
				iHighValue=iEastLimit;
				break;
			}

			if (iHighValue<=objRoom.getPartitionLimit()){
				objResult.add(VisitChild.LOWER_OR_EQUAL);
			}else{
				if (iLowValue>objRoom.getPartitionLimit()){
					objResult.add(VisitChild.MORE);
				}else{
					objResult.add(VisitChild.MORE);
					objResult.add(VisitChild.LOWER_OR_EQUAL);
				}
			}
		}
		return objResult;
	}
	
	
}
