package es.cccf.ants.spacepartitioning;

import java.util.Set;


public interface RoomVisitor<T> {
	public Set<VisitChild> visited(Room<T> objRoom);
	public enum VisitChild{LOWER_OR_EQUAL,MORE};
}
