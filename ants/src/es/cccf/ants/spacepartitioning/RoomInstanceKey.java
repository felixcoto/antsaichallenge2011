package es.cccf.ants.spacepartitioning;

import org.aichallenge.ants.Aim;

class RoomInstanceKey{
	private Room<RoomInfoPartition>objRoom;
	private RoomMovementType objMovementType;
	public Room<RoomInfoPartition> getRoom() {
		return objRoom;
	}
	public void setRoom(Room<RoomInfoPartition> objRoom) {
		this.objRoom = objRoom;
	}
	public RoomMovementType getMovementType() {
		return objMovementType;
	}
	public void setMovementType(RoomMovementType objMovementType) {
		this.objMovementType = objMovementType;
	}
	
	public RoomInstanceKey() {
	
	}
	
	
	public RoomInstanceKey(RoomInstanceKey objInstance){
		this(objInstance.getRoom(),objInstance.getMovementType());
	}
	
	/**
	 * @param objRoom
	 * @param objMovementType
	 * @param objHorizontalComponent
	 * @param objVerticalComponent
	 */
	public RoomInstanceKey(Room<RoomInfoPartition> objRoom, RoomMovementType objMovementType) {
		this.objRoom = objRoom;
		this.objMovementType = objMovementType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objMovementType == null) ? 0 : objMovementType.hashCode());
		result = prime * result + ((objRoom == null) ? 0 : objRoom.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoomInstanceKey other = (RoomInstanceKey) obj;
		if (objMovementType != other.objMovementType)
			return false;
		if (objRoom == null) {
			if (other.objRoom != null)
				return false;
		} else if (!objRoom.equals(other.objRoom))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return String.format("RoomInstanceKey [objRoom=%s, objMovementType=%s]", objRoom, objMovementType);
	}
	
	
	
	
}