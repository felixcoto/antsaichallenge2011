package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

import es.cccf.ants.JacobStraussBot;
import es.cccf.ants.LogLevel;


public class AntsSpacePartitioning {
	private static final double PERMITED_INFOMATION_LOST=0.05;
	private static final boolean USE_CONGESTION_CALC=true;
	
	Ants ants;
	private LineDescription[] rowDescriptions;
	private LineDescription[] colDescriptions;
	private int iWidth;
	private int iHeight;
	Room<RoomInfoPartition> objRoomRoot;
	MapInfoTransitions objMapInfoTransitions;
	Map<Tile,AStarState> objCacheBusqueda;
	

public AntsSpacePartitioning(Ants ants){
	this.iHeight=ants.getRows();
	this.iWidth=ants.getCols();
	this.ants=ants;
	rowDescriptions=new LineDescription[this.iHeight];
	colDescriptions=new LineDescription[this.iWidth];
	objMapInfoTransitions=new MapInfoTransitions(iHeight, iWidth);
	objCacheBusqueda=new HashMap<Tile, AStarState>(this.iHeight*this.iWidth/3);
	
	for (int i=0;i<this.iHeight;i++){
		rowDescriptions[i]=new LineDescription(iWidth);
		
	}

	
	for (int i=0;i<this.iWidth;i++){
		colDescriptions[i]=new LineDescription(iHeight);
	}
	
	
	objRoomRoot=new Room<RoomInfoPartition>(iWidth, iHeight);
	objRoomRoot.setLoad(new RoomInfoPartition(0, 0));


}


	public void addOccupiedBlock(Tile objTile){
		rowDescriptions[objTile.getRow()].addOcupied(objTile.getCol());
		colDescriptions[objTile.getCol()].addOcupied(objTile.getRow());
		objRoomRoot.accept(new AddOccupiedBlockVisitor(objTile.getRow(), objTile.getRow(), objTile.getCol(), objTile.getCol()));
		objMapInfoTransitions.addOccupied(objTile, ants);
	}
	
	public void addAntTile(Tile objTile,boolean bEnemy,Ants ants){
		Room<RoomInfoPartition> objRoom=objRoomRoot.getRoomAtTile(objTile);
		objRoom.getLoad().addAnt(objTile,bEnemy, ants);
	}
	
	public void addHillTile(Tile objTile,boolean bEnemy,Ants ants){
		Room<RoomInfoPartition> objRoom=objRoomRoot.getRoomAtTile(objTile);
		objRoom.getLoad().addHill(objTile,bEnemy, ants);
	}


	public void processUpdate(){
		objRoomRoot.accept(new ProcessSplitRoom());
	}
	
	
	private ExpandResult expandRoom(RoomInstanceNode objRoomInstance, Tile objTileEnd){
		ExpandResult objResult=new ExpandResult();
		int iNumRoomConnected=0;
		int iNumTilesConected=0;
		//		Map<Aim,List<Room<RoomInfoPartition>>> objMapRoomConnected=new HashMap<Aim,List<Room<RoomInfoPartition>>>();

		Room<RoomInfoPartition> objRoom=objRoomInstance.getRoom();
		RoomInfoPartition objInfo=objRoom.getLoad();
		
		Tile objTile=objRoomInstance.getTile();
		for (Aim aim : Aim.values()) {
			//			List<Room<RoomInfoPartition>> objCurrentList=new ArrayList<Room<RoomInfoPartition>>();
			//			objMapRoomConnected.put(aim, objCurrentList);
			int iComponentOriginValue=0;
			int iComponentDestinyValue=0;
			int iOtherComponentOriginValue=0;
			NavigableSet<Integer> objConnectedTilesAim=this.objMapInfoTransitions.getPassForRoomAim(objRoom, aim, ants);
			iNumTilesConected+=objConnectedTilesAim.size();

			if (!objConnectedTilesAim.isEmpty()){
				List<Room<RoomInfoPartition>> objListConnectedRooms;
				switch (aim) {
				case NORTH:
					iComponentOriginValue=objRoom.getNorthLimit();
					iOtherComponentOriginValue=objTile.getCol();
					iComponentDestinyValue=ants.adjustRow(iComponentOriginValue+aim.getRowDelta());
					objListConnectedRooms=objRoomRoot.getRoomListInRange(iComponentDestinyValue, iComponentDestinyValue, objConnectedTilesAim.first(), objConnectedTilesAim.last());
					break;
				case SOUTH:
					iComponentOriginValue=objRoom.getSouthLimit();
					iOtherComponentOriginValue=objTile.getCol();
					iComponentDestinyValue=ants.adjustRow(iComponentOriginValue+aim.getRowDelta());
					objListConnectedRooms=objRoomRoot.getRoomListInRange(iComponentDestinyValue, iComponentDestinyValue, objConnectedTilesAim.first(), objConnectedTilesAim.last());
					break;
				case WEST:
					iComponentOriginValue=objRoom.getWestLimit();
					iOtherComponentOriginValue=objTile.getRow();
					iComponentDestinyValue=ants.adjustCol(iComponentOriginValue+aim.getColDelta());
					objListConnectedRooms=objRoomRoot.getRoomListInRange(objConnectedTilesAim.first(), objConnectedTilesAim.last(),iComponentDestinyValue,iComponentDestinyValue);
					break;
				case EAST:
					iComponentOriginValue=objRoom.getEastLimit();
					iOtherComponentOriginValue=objTile.getRow();
					iComponentDestinyValue=ants.adjustCol(iComponentOriginValue+aim.getColDelta());
					objListConnectedRooms=objRoomRoot.getRoomListInRange(objConnectedTilesAim.first(), objConnectedTilesAim.last(),iComponentDestinyValue,iComponentDestinyValue);
					break;


				default:
					objListConnectedRooms=null;
					break;
				}

				double dCongestion=0;
				if (USE_CONGESTION_CALC){
					int iFreeTiles=objRoom.getSize()-objRoom.getLoad().getNumBlockedTiles();
					int iNumAnts=objRoom.getLoad().getNumAnts(ants);
					dCongestion=((double)iNumAnts)/((double)iFreeTiles);
				}


				for (Room<RoomInfoPartition> objRoomConnected : objListConnectedRooms) {
					NavigableSet<Integer> objConnectedTilesRoom=null;
					int iLowConnectValue=0;
					int iHighConnectValue=0;

					switch (aim){
					case NORTH:
					case SOUTH:
						iLowConnectValue=Math.max(objRoom.getWestLimit(), objRoomConnected.getWestLimit());
						iHighConnectValue=Math.min(objRoom.getEastLimit(), objRoomConnected.getEastLimit());

						break;
					case WEST:
					case EAST:
						iLowConnectValue=Math.max(objRoom.getNorthLimit(), objRoomConnected.getNorthLimit());
						iHighConnectValue=Math.min(objRoom.getSouthLimit(), objRoomConnected.getSouthLimit());


					}

					objConnectedTilesRoom=objConnectedTilesAim.subSet(iLowConnectValue, true, iHighConnectValue, true);

					if (!objConnectedTilesRoom.isEmpty()){
						iNumRoomConnected++;
						Integer iOtherComponentDestinyValue=objConnectedTilesRoom.ceiling(iOtherComponentOriginValue);
						if (iOtherComponentDestinyValue==null){
							iOtherComponentDestinyValue=objConnectedTilesRoom.floor(iOtherComponentOriginValue);
						}
						Tile objFirstStepTile=null;
						switch(aim){
						case NORTH:
						case SOUTH:
							objFirstStepTile=new Tile(iComponentOriginValue,iOtherComponentDestinyValue);
							break;
						case WEST:
						case EAST:
							objFirstStepTile=new Tile(iOtherComponentDestinyValue,iComponentOriginValue);
						}
						Tile objSecondStepTile=ants.getTile(objFirstStepTile, aim);

						RoomInstanceNode objCurrentResult=new RoomInstanceNode(objRoomConnected, RoomMovementType.ENTER,  objSecondStepTile,objFirstStepTile,  objRoomInstance);
						int iCost=objRoomInstance.getCost();
						int iCurrentCost=getCost(objTile, objFirstStepTile)+1;
						if (USE_CONGESTION_CALC){
							int iCongestionCost=(int) Math.floor(iCurrentCost*dCongestion);
							iCurrentCost+=iCongestionCost;
						}
						iCost+=iCurrentCost;
						objCurrentResult.setCost(iCost);
						objCurrentResult.setDepth(objRoomInstance.getDepth()+1);
						
						
						
						RoomInfoPartition objInfoConnectedRoom=objRoomConnected.getLoad();
						if (objInfoConnectedRoom.isSingleRoomConnected()||objInfoConnectedRoom.isSingleTileConnected()){
							if (objTileEnd!=null&&objRoomConnected.isValidTileForRoom(objTileEnd)){
								objResult.addOpenPath(objCurrentResult);
								
							}else{
								objResult.addClosedPath(objCurrentResult);
							}
						}else{
							objResult.addOpenPath(objCurrentResult);
						}

					}
				}

			}



		}
		
		if (objTileEnd!=null&&objRoom.isValidTileForRoom(objTileEnd)){
			RoomInstanceNode objCurrentResult=new RoomInstanceNode(objRoom, RoomMovementType.INSIDE, objTileEnd, objTile,  objRoomInstance);
			int iCost=objRoomInstance.getCost();
			iCost+=ants.getManhattanDistance(objTile, objTileEnd);
			objCurrentResult.setCost(iCost);

			objCurrentResult.setDepth(objRoomInstance.getDepth()+1);
			objResult.addOpenPath(objCurrentResult);

		}
		
		if (iNumTilesConected<=1){
			objInfo.setSingleTileConnected(true);
		}
		
		if (iNumRoomConnected<=1){
			objInfo.setSingleRoomConnected(true);
		}



		return objResult;
	}
	
	
	
	public Path findPath(Tile objTileStart,Tile objTileEnd, int iMaxCost, int iMaxms){
		Path objResult=null;
		
		if (ants.getCurrentTurn()==110 && objTileStart.getRow()==39 && objTileStart.getCol()==112){
			System.err.println("TEST");
		}

		boolean bFinded=false;
		boolean bTimeout=false;
		boolean bMaxCostExceded=false;


		long lTiempoLimite=System.currentTimeMillis()+iMaxms;
		
		SortedList<RoomInstanceNode> objOpenList;
		Map<RoomInstanceKey,RoomInstanceNode> objOpenNodesMap;
		Map<RoomInstanceKey,RoomInstanceNode> objClosedNodesMap;

		
		
		AStarState objAStarState;
		
		if (this.objCacheBusqueda.containsKey(objTileStart)){
			objAStarState=this.objCacheBusqueda.get(objTileStart);
			objOpenList=objAStarState.getOpenList();
			objOpenNodesMap=objAStarState.getOpenNodesMap();
			objClosedNodesMap=objAStarState.getClosedNodesMap();
			if (objTileEnd!=null){
				Room<RoomInfoPartition> objFirstRoom=this.getRoomAtTile(objTileEnd);
				RoomInstanceKey objKey=new RoomInstanceKey(objFirstRoom,RoomMovementType.ENTER);
				if (objClosedNodesMap.containsKey(objKey)){
					RoomInstanceNode objNode=objClosedNodesMap.remove(objKey);
					objOpenNodesMap.put(objKey, objNode);
					objOpenList.add(objNode);
				}
				objKey=new RoomInstanceKey(objFirstRoom,RoomMovementType.START);
				if (objClosedNodesMap.containsKey(objKey)){
					RoomInstanceNode objNode=objClosedNodesMap.remove(objKey);
					objOpenNodesMap.put(objKey, objNode);
					objOpenList.add(objNode);
				}
				objKey=new RoomInstanceKey(objFirstRoom,RoomMovementType.INSIDE);
				if (objClosedNodesMap.containsKey(objKey)){
					objClosedNodesMap.remove(objKey);
				}
				if (objOpenNodesMap.containsKey(objKey)){
					objOpenNodesMap.remove(objKey);
				}
			}
			objAStarState.setTileEnd(objTileEnd);
			objOpenList.setDirty();
			
		}else{
			objAStarState=new AStarState(objTileEnd,ants);
			objCacheBusqueda.put(objTileStart, objAStarState);
			objOpenList=objAStarState.getOpenList();
			objOpenNodesMap=objAStarState.getOpenNodesMap();
			objClosedNodesMap=objAStarState.getClosedNodesMap();
			Room<RoomInfoPartition> objFirstRoom=this.getRoomAtTile(objTileStart);
			RoomInstanceNode objFirstInstance=new RoomInstanceNode(objFirstRoom,RoomMovementType.START, objTileStart, objAStarState);
			RoomInstanceNode objCurrent=objFirstInstance;
			objOpenList.add(objCurrent);
			objOpenNodesMap.put(objCurrent.getKey(),objCurrent);

		}
		
		
		

		RoomInstanceNode objCurrent=null;
		while (objOpenList.size()>0&&!bFinded&&!bTimeout&&!bMaxCostExceded){


			objCurrent=objOpenList.removeFirst();


			if (objOpenNodesMap.containsKey(objCurrent.getKey())&&objCurrent.equals(objOpenNodesMap.get(objCurrent.getKey()))){
				if (System.currentTimeMillis()>lTiempoLimite){
					bTimeout=true;
					objOpenList.add(objCurrent);
				}else if (objCurrent.getTotalEstimatedCost()>iMaxCost){
					bMaxCostExceded=true;
					objOpenList.add(objCurrent);
				}else{


					objClosedNodesMap.put(objCurrent.getKey(), objCurrent);
					objOpenNodesMap.remove(objCurrent.getKey());


					if (objTileEnd!=null&&objCurrent.getTile().equals(objTileEnd)){
						bFinded=true;
						//Lo volvemos a insertar para que pueda ser procesado en las siguientes busquedas
						objOpenNodesMap.put(objCurrent.getKey(), objCurrent);
						objOpenList.add(objCurrent);
						objClosedNodesMap.remove(objCurrent.getKey());
					}else{
						ExpandResult objExpandResult=expandRoom(objCurrent, objTileEnd);

						for (RoomInstanceNode objNewRoomInstance : objExpandResult.getOpenPaths()) {
							RoomInstanceKey objNewKey=objNewRoomInstance.getKey();

							if (objCurrent.getKey().getMovementType().equals(RoomMovementType.START)){
								objAStarState.addRoomFirstExpansion(objNewRoomInstance);
							}
							
							if (!objClosedNodesMap.containsKey(objNewKey)){


								if (!objOpenNodesMap.containsKey(objNewKey)){
									objOpenList.add(objNewRoomInstance);
									objOpenNodesMap.put(objNewKey, objNewRoomInstance);
								}else{
									RoomInstanceNode objOldRoomInstance=objOpenNodesMap.get(objNewKey);
									if (objOldRoomInstance.getCost()>objNewRoomInstance.getCost()){
										objOpenNodesMap.remove(objNewKey);
										objOpenList.add(objNewRoomInstance);
										objOpenNodesMap.put(objNewKey, objNewRoomInstance);
									}
								}


							}
						}
						for (RoomInstanceNode objNewRoomInstanceNode : objExpandResult.getClosedPaths()) {
							RoomInstanceKey objNewKey=objNewRoomInstanceNode.getKey();

							if (objCurrent.getKey().getMovementType().equals(RoomMovementType.START)){
								objAStarState.addRoomFirstExpansion(objNewRoomInstanceNode);
							}
							
							if (objClosedNodesMap.containsKey(objNewKey)){
								RoomInstanceNode objOldRoomInstance=objClosedNodesMap.get(objNewKey);
								if (objOldRoomInstance.getCost()>objNewRoomInstanceNode.getCost()){
									objClosedNodesMap.put(objNewKey, objNewRoomInstanceNode);
								}
							}else{
								objClosedNodesMap.put(objNewKey, objNewRoomInstanceNode);
							}

						}

					}
				}
			}
		}

		if (bFinded){
			objResult=new Path(objCurrent,true,0);
		}else{
			if (objOpenList.size()>0){
				objResult=new Path(objCurrent,false,objAStarState.getHeuristic(objCurrent.getTile()));
			}else{
				if (objTileEnd!=null&&JacobStraussBot.isLevelEnabled(LogLevel.INFO)){
					JacobStraussBot.log("No se puede alcanzar destino "+objTileEnd.toString(), objTileStart, LogLevel.INFO);
				}

			}
		}




		return objResult;
	}

	public List<RoomInstanceNode> getRoomsAtCost(Tile objTileStart,int iMaxCost,int iTimeout){
		this.findPath(objTileStart, null, iMaxCost, iTimeout);
		AStarState aStarState=this.objCacheBusqueda.get(objTileStart);
		List<RoomInstanceNode> objPartialResult=new ArrayList<RoomInstanceNode>(aStarState.getClosedNodesMap().values());
		Collections.sort(objPartialResult,new RoomInstanceComparator());
		List<RoomInstanceNode> objResult=new ArrayList<RoomInstanceNode>();
		Iterator<RoomInstanceNode> it=objPartialResult.iterator();
		boolean bEnd=false;
		while(it.hasNext()&&!bEnd){
			RoomInstanceNode objInstance=it.next();
			if (objInstance.getCost()<=iMaxCost){
				objResult.add(objInstance);
			}else{
				bEnd=true;
			}
		}
		return objResult;
		
	}
	
	
	private class ProcessSplitRoom implements RoomVisitor<RoomInfoPartition>{

		@Override
		public Set<es.cccf.ants.spacepartitioning.RoomVisitor.VisitChild> visited(Room<RoomInfoPartition> objRoom) {
			
			Set<VisitChild> objResult=new HashSet<RoomVisitor.VisitChild>();
			RoomInfoPartition objInfo=objRoom.getLoad();
			objInfo.setSingleRoomConnected(false);
			if (objInfo.isDirty()){
				objInfo.setDirty(false);
				int iRoomSize=objRoom.getSize();
				if (!objRoom.isLeaf()){
					
						Room<RoomInfoPartition> objRoomLessOrEqual=objRoom.getRoomLessOrEqual();
						Room<RoomInfoPartition> objRoomMore=objRoom.objRoomMore;
						RoomInfoPartition objInfoLessOrEqual=objRoomLessOrEqual.getLoad();
						RoomInfoPartition objInfoMore=objRoomMore.getLoad();
						int iRoomLessOrEqualSize=objRoomLessOrEqual.getSize();
						int iRoomMoreSize=objRoomMore.getSize();
						double dTotalEntropy=objInfo.getEntropy();
						if (!objInfo.isRecalculatedEntropy()){
							dTotalEntropy=EntropyCalculator.calculateEntropyValue(iRoomSize, objInfo.getNumBlockedTiles());
						}
						double dEntropy1=EntropyCalculator.calculateEntropyValue(iRoomLessOrEqualSize, objInfoLessOrEqual.getNumBlockedTiles());
						double dEntropy2=EntropyCalculator.calculateEntropyValue(iRoomMoreSize, objInfoMore.getNumBlockedTiles());
						double dNewInformationGain=EntropyCalculator.calculateInfoGain(iRoomSize, dTotalEntropy, iRoomLessOrEqualSize, dEntropy1, iRoomMoreSize, dEntropy2);
						double dNewInformationGainRatio=dTotalEntropy>0?dNewInformationGain/dTotalEntropy:1;
						if (objRoom.isRoot()){
							if (JacobStraussBot.isLevelEnabled(LogLevel.INFO)){
								JacobStraussBot.log(String.format("Estadistica arbol completo: anterior: %.5f, nuevo: %.5f", objInfo.getInformationGainRatio(),dNewInformationGainRatio)	, null, LogLevel.INFO);
							}
								
						}

						if (dNewInformationGainRatio>objInfo.getInformationGainRatio()){
							objInfo.setInformationGainRatio(dNewInformationGainRatio);
							objInfo.setInformationGain(dNewInformationGain);
							objInfo.setDirty(false);
							objInfo.setEntropy(dTotalEntropy);
							objInfoLessOrEqual.setEntropy(dEntropy1);
							objInfoLessOrEqual.setRecalculatedEntropy(true);
							objInfoMore.setEntropy(dEntropy2);
							objInfoMore.setRecalculatedEntropy(true);
							objInfo.setInformationGainRatio(dNewInformationGainRatio);
						}else{
							double dDiference=objInfo.getInformationGainRatio()-dNewInformationGainRatio;
							if (dDiference>=PERMITED_INFOMATION_LOST){
								if (objRoom.isRoot()){
									if (JacobStraussBot.isLevelEnabled(LogLevel.INFO)){
										JacobStraussBot.log(String.format("Invalidado arbol completo: anterior: %.5f, nuevo: %.5f", objInfo.getInformationGainRatio(),dNewInformationGainRatio)	, null, LogLevel.INFO);
									}
										
								}
								objCacheBusqueda.clear();
								objRoom.mergeChildRooms();
							}
						}

				}
				
				if (objRoom.isLeaf()){
					NavigableSet<Integer> objHorizontalSplits=new TreeSet<Integer>();
					NavigableSet<Integer> objVerticalSplits=new TreeSet<Integer>();
					if (objInfo.getNumBlockedTiles()>0 && objInfo.getNumBlockedTiles()!=iRoomSize){

					if (objRoom.getWidth()>2){
						
						for (int iRow=objRoom.getNorthLimit();iRow<=objRoom.getSouthLimit()&&objVerticalSplits.size()<objRoom.getWidth()-1;iRow++){
							LineDescription objRowDescription=rowDescriptions[iRow];
							NavigableSet<Integer> objSetUpperLimits=objRowDescription.getUpperLimits().subSet(objRoom.getWestLimit(),false, objRoom.getEastLimit(),true);
							NavigableSet<Integer> objSetLowerLimits=objRowDescription.getLowerLimits().subSet(objRoom.getWestLimit(),true, objRoom.getEastLimit(),false);

							if (!objSetUpperLimits.isEmpty()&&!objSetLowerLimits.isEmpty()){
								Integer iUpperLimit=objSetUpperLimits.first();
								while (iUpperLimit!=null){
									Integer iLowerLimit=objSetLowerLimits.ceiling(iUpperLimit);
									if (iLowerLimit!=null){
										objVerticalSplits.add(iLowerLimit);
										objVerticalSplits.add(iUpperLimit-1);
										iUpperLimit=objSetUpperLimits.higher(iLowerLimit);
									}else{
										iUpperLimit=null;
									}
								}
							}
						}
					}
					if (objRoom.getHeight()>2){
						for (int iCol=objRoom.getWestLimit();iCol<=objRoom.getEastLimit()&&objHorizontalSplits.size()<objRoom.getHeight()-1;iCol++){
							LineDescription objColDescription=colDescriptions[iCol];
							NavigableSet<Integer> objSetUpperLimits=objColDescription.getUpperLimits().subSet(objRoom.getNorthLimit(),false, objRoom.getSouthLimit(),true);
							NavigableSet<Integer> objSetLowerLimits=objColDescription.getLowerLimits().subSet(objRoom.getNorthLimit(),true, objRoom.getSouthLimit(),false);
							if (!objSetUpperLimits.isEmpty()&&!objSetLowerLimits.isEmpty()){
								Integer iUpperLimit=objSetUpperLimits.first();
								while (iUpperLimit!=null){
									Integer iLowerLimit=objSetLowerLimits.ceiling(iUpperLimit);
									if (iLowerLimit!=null){
										objHorizontalSplits.add(iLowerLimit);
										objHorizontalSplits.add(iUpperLimit-1);
										iUpperLimit=objSetUpperLimits.higher(iLowerLimit);
									}else{
										iUpperLimit=null;
									}
								}
							}
						}
					}
					
					
					if (objHorizontalSplits.isEmpty()&&objVerticalSplits.isEmpty()
							&& objRoom.getWidth()>1 && objRoom.getHeight()>1){
						boolean bNorthDiscont=false;
						boolean bSouthDiscont=false;
						boolean bWestDiscont=false;
						boolean bEastDiscont=false;
						int iNorthDiscont=0;
						int iSouthDiscont=0;
						int iWestDiscont=0;
						int iEastDiscont=0;
						
						
						int iLastBlankHigh=0;
						int iLastBlankLow=0;
						boolean bFindedBlank=false;
						for (int iRow=objRoom.getNorthLimit();iRow<=objRoom.getSouthLimit()&&!bSouthDiscont;iRow++){
							LineDescription objRowDescription=rowDescriptions[iRow];
							NavigableSet<Integer> objSetFreeTiles=objRowDescription.getFree().subSet(objRoom.getWestLimit(),true, objRoom.getEastLimit(),true);
							if (!bFindedBlank){
								if (!objSetFreeTiles.isEmpty()){
									bFindedBlank=true;
									iLastBlankHigh=objSetFreeTiles.last();
									iLastBlankLow=objSetFreeTiles.first();
								}
							}else{
									if (!bNorthDiscont){
										if (objSetFreeTiles.isEmpty()){
											bNorthDiscont=true;
											iNorthDiscont=iRow-1;
										}else{
											if (objSetFreeTiles.contains(iLastBlankHigh)|| objSetFreeTiles.contains(iLastBlankLow)){
												iLastBlankHigh=objSetFreeTiles.last();
												iLastBlankLow=objSetFreeTiles.first();
											}else{
												bNorthDiscont=true;
												bSouthDiscont=true;
												iNorthDiscont=iRow-1;
												iSouthDiscont=iRow;
											}
										}
									}else{
										if (!objSetFreeTiles.isEmpty()){
											bSouthDiscont=true;
											iSouthDiscont=iRow;
										}
									}
								}
							}
						
						if (bNorthDiscont&&bSouthDiscont){
							objHorizontalSplits.add(iNorthDiscont);
							objHorizontalSplits.add(iSouthDiscont-1);
						}
						
						
						iLastBlankHigh=0;
						iLastBlankLow=0;
						bFindedBlank=false;

						for (int iCol=objRoom.getWestLimit();iCol<=objRoom.getEastLimit()&&!bEastDiscont;iCol++){
							LineDescription objColDescription=colDescriptions[iCol];
							NavigableSet<Integer> objSetFreeTiles=objColDescription.getFree().subSet(objRoom.getNorthLimit(),true, objRoom.getSouthLimit(),true);
							if (!bFindedBlank){
								if (!objSetFreeTiles.isEmpty()){
									bFindedBlank=true;
									iLastBlankHigh=objSetFreeTiles.last();
									iLastBlankLow=objSetFreeTiles.first();
								}
							}else{
									if (!bWestDiscont){
										if (objSetFreeTiles.isEmpty()){
											bWestDiscont=true;
											iWestDiscont=iCol-1;
										}else{
											if (objSetFreeTiles.contains(iLastBlankHigh)|| objSetFreeTiles.contains(iLastBlankLow)){
												iLastBlankHigh=objSetFreeTiles.last();
												iLastBlankLow=objSetFreeTiles.first();
											}else{
												bWestDiscont=true;
												bEastDiscont=true;
												iWestDiscont=iCol-1;
												iEastDiscont=iCol;
											}
										}
									}else{
										if (!objSetFreeTiles.isEmpty()){
											bEastDiscont=true;
											iEastDiscont=iCol;
										}
									}
								}
							}
						
						if (bWestDiscont&&bEastDiscont){
							objVerticalSplits.add(iWestDiscont);
							objVerticalSplits.add(iEastDiscont-1);
						}

						
						
					}
					}
					
					if (objHorizontalSplits.isEmpty()&& objRoom.getHeight()>=iHeight/2){
						objHorizontalSplits.add(objRoom.getNorthLimit()+(objRoom.getHeight()/2));
					}
					
					if (objVerticalSplits.isEmpty()&&objRoom.getWidth()>=iWidth/2){
						objVerticalSplits.add(objRoom.getWestLimit()+(objRoom.getWidth()/2));
					}
					
					
					if (!objHorizontalSplits.isEmpty()||!objVerticalSplits.isEmpty()){
						double dTotalEntropy=objInfo.getEntropy();
						if (!objInfo.isRecalculatedEntropy()){
							dTotalEntropy=EntropyCalculator.calculateEntropyValue(iRoomSize, objInfo.getNumBlockedTiles());
						}
						Partition objPartitionType=null;
						double dInformationGain=-1;
						int iPartitionLimit=0;
						int iNumOccupiedBlocks1=0;
						int iNumOccupiedBlocks2=0;
						double dEntropy1=0;
						double dEntropy2=0;
						
						int iOccupiedBlocksUpper=0;
						int iOccupiedBlocksLower=objInfo.getNumBlockedTiles();
						if (!objHorizontalSplits.isEmpty()){
							int iLimit=objHorizontalSplits.last();
							for (int i=objRoom.getNorthLimit();i<=iLimit;i++){
								NavigableSet<Integer> objRowOccupied=rowDescriptions[i].getOccupied().subSet(objRoom.getWestLimit(), true, objRoom.getEastLimit(), true);
								int iNumOccupiesCurrentRow=objRowOccupied.size();
								iOccupiedBlocksUpper+=iNumOccupiesCurrentRow;
								iOccupiedBlocksLower-=iNumOccupiesCurrentRow;
								if (objHorizontalSplits.contains(i)){
									double dInformationGainCurrent=0;
									double dEntropy1Current=0;
									double dEntropy2Current=0;
									int iSizeUpper=objRoom.getWidth()*(i-objRoom.getNorthLimit()+1);
									int iSizeLower=iRoomSize-iSizeUpper;
									dEntropy1Current=EntropyCalculator.calculateEntropyValue(iSizeUpper, iOccupiedBlocksUpper);
									dEntropy2Current=EntropyCalculator.calculateEntropyValue(iSizeLower, iOccupiedBlocksLower);
									dInformationGainCurrent=EntropyCalculator.calculateInfoGain(iRoomSize, dTotalEntropy, iSizeUpper, dEntropy1Current, iSizeLower, dEntropy2Current);
									if (dInformationGainCurrent>dInformationGain){
										objPartitionType=Partition.HORIZONTAL;
										iPartitionLimit=i;
										iNumOccupiedBlocks1=iOccupiedBlocksUpper;
										iNumOccupiedBlocks2=iOccupiedBlocksLower;
										dEntropy1=dEntropy1Current;
										dEntropy2=dEntropy2Current;
										dInformationGain=dInformationGainCurrent;
									}
								}
							}
						}
						iOccupiedBlocksUpper=0;
						iOccupiedBlocksLower=objInfo.getNumBlockedTiles();
						if (!objVerticalSplits.isEmpty()){
							int iLimit=objVerticalSplits.last();
							for (int i=objRoom.getWestLimit();i<=iLimit;i++){
								NavigableSet<Integer> objColOccupied=colDescriptions[i].getOccupied().subSet(objRoom.getNorthLimit(), true, objRoom.getSouthLimit(), true);
								int iNumOccupiesCurrentRow=objColOccupied.size();
								iOccupiedBlocksUpper+=iNumOccupiesCurrentRow;
								iOccupiedBlocksLower-=iNumOccupiesCurrentRow;
								if (objVerticalSplits.contains(i)){
									double dInformationGainCurrent=0;
									double dEntropy1Current=0;
									double dEntropy2Current=0;
									int iSizeUpper=objRoom.getHeight()*(i-objRoom.getWestLimit()+1);
									int iSizeLower=iRoomSize-iSizeUpper;
									dEntropy1Current=EntropyCalculator.calculateEntropyValue(iSizeUpper, iOccupiedBlocksUpper);
									dEntropy2Current=EntropyCalculator.calculateEntropyValue(iSizeLower, iOccupiedBlocksLower);
									dInformationGainCurrent=EntropyCalculator.calculateInfoGain(iRoomSize, dTotalEntropy, iSizeUpper, dEntropy1Current, iSizeLower, dEntropy2Current);
									if (dInformationGainCurrent>dInformationGain){
										objPartitionType=Partition.VERTICAL;
										iPartitionLimit=i;
										iNumOccupiedBlocks1=iOccupiedBlocksUpper;
										iNumOccupiedBlocks2=iOccupiedBlocksLower;
										dEntropy1=dEntropy1Current;
										dEntropy2=dEntropy2Current;
										dInformationGain=dInformationGainCurrent;
									}
								}
							}
						}
						
						objRoom.splitRoom(objPartitionType, iPartitionLimit);
						RoomInfoPartition objInfoLower=new RoomInfoPartition(iNumOccupiedBlocks1, dEntropy1);
						objInfoLower.setDirty(true);
						objInfoLower.setRecalculatedEntropy(true);
						objRoom.getRoomLessOrEqual().setLoad(objInfoLower);
						RoomInfoPartition objInfoMore=new RoomInfoPartition(iNumOccupiedBlocks2, dEntropy2);
						objInfoMore.setDirty(true);
						objInfoMore.setRecalculatedEntropy(true);
						objRoom.getRoomMore().setLoad(objInfoMore);

						objInfo.setDirty(false);
						objInfo.setEntropy(dTotalEntropy);
						objInfo.setInformationGain(dInformationGain);
						double dInformationGainRatio=dTotalEntropy>0?dInformationGain/dTotalEntropy:1;
						objInfo.setInformationGainRatio(dInformationGainRatio);
						objInfo.setRecalculatedEntropy(true);
						
						
					}else{
						objInfo.setDirty(false);
					}

					
					
				}
				
				
			}
			if (!objRoom.isLeaf()){
				objResult.add(VisitChild.LOWER_OR_EQUAL);
				objResult.add(VisitChild.MORE);
			}

			return objResult;
		}
		
	}

	public Room<RoomInfoPartition> getRoomAtTile(Tile objTile) {
		return objRoomRoot.getRoomAtTile(objTile);
	}


	public int getCost(Tile t1, Tile t2) {
	    int rowDelta = Math.abs(t1.getRow() - t2.getRow());
	    int colDelta = Math.abs(t1.getCol() - t2.getCol());
	    return rowDelta  + colDelta ;
	}
	
	
	
}

class AddOccupiedBlockVisitor extends AbstractRangeRoomVisitor<RoomInfoPartition>{

	public AddOccupiedBlockVisitor(int objNorthLimit, int objSouthLimit, int objWestLimit, int objEastLimit) {
		super(objNorthLimit, objSouthLimit, objWestLimit, objEastLimit);
	}

	@Override
	protected void doVisit(Room<RoomInfoPartition> objRoom) {
		RoomInfoPartition objInfo=objRoom.getLoad();
		objInfo.addBlockedTile();
		objInfo.setDirty(true);
		objInfo.setRecalculatedEntropy(false);
	}



	

}
class MapInfoTransitions{
	private List<NavigableSet<Integer>> objFreePassBetweenRowsSet;
	private List<NavigableSet<Integer>> objFreePassBetweenColsSet;

	
	
	public MapInfoTransitions(int iHeight,int iWidth) {
	 	objFreePassBetweenRowsSet=new ArrayList<NavigableSet<Integer>>(iHeight);
		objFreePassBetweenColsSet=new ArrayList<NavigableSet<Integer>>(iWidth);
		
		for (int i=0;i<iHeight;i++){
			NavigableSet<Integer> objCurrent=new TreeSet<Integer>();
			objFreePassBetweenRowsSet.add(objCurrent);
			for (int j=0;j<iWidth;j++){
				objCurrent.add(j);
			}
		}

		for (int i=0;i<iWidth;i++){
			NavigableSet<Integer> objCurrent=new TreeSet<Integer>();
			objFreePassBetweenColsSet.add(objCurrent);
			for (int j=0;j<iHeight;j++){
				objCurrent.add(j);
			}
		}



	}
	
	public void addOccupied(Tile objTile, Ants ants){
		objFreePassBetweenRowsSet.get(objTile.getRow()).remove(objTile.getCol()); // REMOVE PASS FROM NORTH
		objFreePassBetweenColsSet.get(objTile.getCol()).remove(objTile.getRow()); // REMOVE PASS FROM WEST
		Tile objSouthTile=ants.getTile(objTile, Aim.SOUTH);
		objFreePassBetweenRowsSet.get(objSouthTile.getRow()).remove(objSouthTile.getCol()); // REMOVE PASS FROM SOUTH
		Tile objEastTile=ants.getTile(objTile, Aim.EAST);
		objFreePassBetweenColsSet.get(objEastTile.getCol()).remove(objEastTile.getRow()); // REMOVE PASS FROM EAST
	}
	
	public NavigableSet<Integer> getPassForRoomAim(Room<RoomInfoPartition> objRoom,Aim aim,Ants ants){
		NavigableSet<Integer> objResult;
		switch (aim) {
		case NORTH:
			objResult=objFreePassBetweenRowsSet.get(objRoom.getNorthLimit()).subSet(objRoom.getWestLimit(), true, objRoom.getEastLimit(), true);
			break;
		case SOUTH:
			objResult=objFreePassBetweenRowsSet.get(ants.adjustRow(objRoom.getSouthLimit()+1)).subSet(objRoom.getWestLimit(), true, objRoom.getEastLimit(), true);
			break;
		case WEST:
			objResult=objFreePassBetweenColsSet.get(objRoom.getWestLimit()).subSet(objRoom.getNorthLimit(), true, objRoom.getSouthLimit(), true);
			break;
		case EAST:
			objResult=objFreePassBetweenColsSet.get(ants.adjustCol(objRoom.getEastLimit()+1)).subSet(objRoom.getNorthLimit(), true, objRoom.getSouthLimit(), true);
			break;
		default:
			objResult=null;
			break;
		}
		return objResult;
	}

	
}