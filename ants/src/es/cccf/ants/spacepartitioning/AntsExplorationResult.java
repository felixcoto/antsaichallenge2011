package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.List;

import org.aichallenge.ants.Tile;

public class AntsExplorationResult {
	private List<Tile> objListAntsInPopulatedArea;
	private List<Tile> objListDestinationUnseen;
	private List<Tile> objListDestinationDeserted;
	

	public AntsExplorationResult() {
		objListAntsInPopulatedArea=new ArrayList<Tile>();
		objListDestinationDeserted=new ArrayList<Tile>();
		objListDestinationUnseen=new ArrayList<Tile>();
	}


	public List<Tile> getListAntsInPopulatedArea() {
		return objListAntsInPopulatedArea;
	}


	public List<Tile> getListDestinationUnseen() {
		return objListDestinationUnseen;
	}


	public List<Tile> getListDestinationDeserted() {
		return objListDestinationDeserted;
	}
	
	
	
}
