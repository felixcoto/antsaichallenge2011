package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

public class AStarState {
	Ants ants;
	Tile objTileEnd;
	private SortedList<RoomInstanceNode> objOpenList=new SortedList<RoomInstanceNode>(new RoomInstanceComparator());
	private Map<RoomInstanceKey,RoomInstanceNode> objOpenNodesMap=new HashMap<RoomInstanceKey, RoomInstanceNode>();
	private Map<RoomInstanceKey,RoomInstanceNode> objClosedNodesMap=new HashMap<RoomInstanceKey, RoomInstanceNode>();
	private List<RoomInstanceNode> objFirstExpansion=new ArrayList<RoomInstanceNode>();
	boolean bFirstExpansionComplete=false;
	public SortedList<RoomInstanceNode> getOpenList() {
		return objOpenList;
	}
	public Map<RoomInstanceKey, RoomInstanceNode> getOpenNodesMap() {
		return objOpenNodesMap;
	}
	public Map<RoomInstanceKey, RoomInstanceNode> getClosedNodesMap() {
		return objClosedNodesMap;
	}
	public Tile getTileEnd() {
		return objTileEnd;
	}
	public void setTileEnd(Tile objTileEnd) {
		this.objTileEnd = objTileEnd;
	}
	public void addRoomFirstExpansion(RoomInstanceNode objFirstExpansion){
		if (!this.bFirstExpansionComplete){
			this.objFirstExpansion.add(objFirstExpansion);
		}
	}
	public void setFirstExpansionComplete(){
		this.bFirstExpansionComplete=true;
	}
	/**
	 * @param objTileEnd
	 */
	public AStarState(Tile objTileEnd,Ants ants) {
		this.objTileEnd = objTileEnd;
		this.ants=ants;
	}

	
	public int getHeuristic(Tile objTileStart){
		int iResult=0;
		if (objTileEnd!=null){
			iResult=ants.getManhattanDistance(objTileStart, objTileEnd);
		}
		return iResult;
	}
	public List<RoomInstanceNode> getFirstExpansion() {
		return objFirstExpansion;
	}
	
	
}
