package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.List;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Ilk;
import org.aichallenge.ants.Tile;

public class AntsPathfindTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Tile[] aTest=new Tile[]{
				new Tile(0,0),
				new Tile(1,0),
				new Tile(2,0),
				new Tile(3,0),
				new Tile(4,0),
				new Tile(5,0),
				new Tile(6,1),
				new Tile(6,2),
				new Tile(6,3),
				new Tile(5,3),
				new Tile(4,2),
				new Tile(3,2),
				new Tile(2,2),
				new Tile(1,2)
		};

//		int[] aRowDelta=new int[]{0,30,50,80};
//		int[] aColDelta=new int[]{0,0,0,90};
		int[] aRowDelta=new int[]{0};
		int[] aColDelta=new int[]{0};

		int rows=100;
		int cols=100;
		Ants ants=new Ants(0,0,rows,cols,900,77,5,1,42);

		AntsSpacePartitioning antsSpacePartitioning=new AntsSpacePartitioning(ants);

		ArrayList<Tile> objTiles=new ArrayList<Tile>();
		for (int i=0;i<aRowDelta.length;i++){
			int rowDelta=aRowDelta[i];
			int colDelta=aColDelta[i];
			for (Tile objTile : aTest) {
				Tile objNewTile=new Tile(objTile.getRow()+rowDelta,objTile.getCol()+colDelta);
				antsSpacePartitioning.addOccupiedBlock(objNewTile);
				objTiles.add(objNewTile);
				ants.update(Ilk.WATER, objNewTile, 0);
			}

		}
		long lInicio=System.currentTimeMillis();
		antsSpacePartitioning.processUpdate();
		long lFin=System.currentTimeMillis();
		System.out.println("Tiempo: "+(lFin-lInicio));
		
		Tile objTileStart=new Tile(30,30);
		Tile objTileEnd=new Tile(5,2);
		
		Path objPath=antsSpacePartitioning.findPath(objTileStart, objTileEnd,999,25);
		System.out.println("PATH:"+objPath.toString());

		System.out.println("INICIANDO TEST VELOCIDAD");
		lInicio=System.currentTimeMillis();
		for (int i=0;i<1000;i++){
			objPath=antsSpacePartitioning.findPath(objTileStart, objTileEnd,999,25);
		}
		lFin=System.currentTimeMillis();
		System.out.println("Tiempo: "+(lFin-lInicio));
	}

}
