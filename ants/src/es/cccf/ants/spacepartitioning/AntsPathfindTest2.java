package es.cccf.ants.spacepartitioning;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Tile;

public class AntsPathfindTest2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
			File objFileInit = new File(args[0]);
			File[] aFile;

//			if (objFileInit.isDirectory()) {
//				aFile = objFileInit.listFiles(new FileFilter() {
//
//					@Override
//					public boolean accept(File objPathname) {
//						return objPathname.getName().endsWith(".map");
//					}
//				});
//			} else {
				aFile = new File[] { objFileInit };
//		}
			for (File objFile : aFile) {

				try {
					Scanner objScanner = new Scanner(objFile);
					int rows;
					int cols;
					List<Tile> objTileList;
					objScanner.findInLine("rows");
					rows = objScanner.nextInt();
					objScanner.nextLine();
					objScanner.findInLine("cols");
					cols = objScanner.nextInt();
					objTileList = new ArrayList<Tile>();
					objScanner.nextLine();
					objScanner.nextLine();
					int iLine = 0;
					Ants ants=new Ants(0,0,rows,cols,900,77,5,1,42);
					while (objScanner.hasNext("m")) {
						String mapIndicator = objScanner.next("m");
						String sLineMap = objScanner.next();
						char[] aChar = sLineMap.toCharArray();

						for (int i = 0; i < aChar.length; i++) {
							if (aChar[i] == '%')
								objTileList.add(new Tile(iLine, i));
						}
						iLine++;
						objScanner.nextLine();
					}
					System.out.println("Nombre Mapa:" + objFile.getName());
					System.out.println("Numero Tiles=" + objTileList.size());
					AntsSpacePartitioning antsSpacePartitioning = new AntsSpacePartitioning(ants);

					for (Tile objTile : objTileList) {
						antsSpacePartitioning.addOccupiedBlock(objTile);
					}
					long lInicio = System.currentTimeMillis();
					antsSpacePartitioning.processUpdate();
					long lFin = System.currentTimeMillis();
					System.out.println("Tiempo procesar particionamiento 1: " + (lFin - lInicio));
					
					
					Tile objNewWaterTile=new Tile(21,99);
					lInicio = System.currentTimeMillis();
					antsSpacePartitioning.addOccupiedBlock(objNewWaterTile);
					antsSpacePartitioning.processUpdate();
					lFin = System.currentTimeMillis();
					System.out.println("Tiempo procesar particionamiento 2: " + (lFin - lInicio));

					
					
					Tile objTile1=new Tile(27,86);
					Tile objTile2=new Tile(27,76);
					lInicio = System.currentTimeMillis();
					Path objPath=antsSpacePartitioning.findPath(objTile1, objTile2,999,25);
					lFin = System.currentTimeMillis();
					System.out.println("Tiempo encontrar camino: " + (lFin - lInicio));
					
					lInicio = System.currentTimeMillis();
					objPath=antsSpacePartitioning.findPath(objTile1, objTile2,999,25);
					lFin = System.currentTimeMillis();
					System.out.println("Tiempo encontrar camino2: " + (lFin - lInicio));

					
					System.out.println("Camino:"+objPath.toString());
					
					System.out.println("INICIANDO TEST VELOCIDAD");
					lInicio=System.currentTimeMillis();
					for (int i=0;i<1000;i++){
						objPath=antsSpacePartitioning.findPath(objTile1, objTile2,999,25);
					}
					lFin=System.currentTimeMillis();
					System.out.println("Tiempo: "+(lFin-lInicio));

					
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Throwable th) {
					th.printStackTrace();
				}
			}

	}
				

}
