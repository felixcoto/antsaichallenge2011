package es.cccf.ants.spacepartitioning;

import java.util.ArrayList;
import java.util.List;

public class ExpandResult {
	private List<RoomInstanceNode> objClosedPaths;
	private List<RoomInstanceNode> objOpenPaths;
	boolean bReInsertOriginAsOpen;
	
	
	public ExpandResult() {
		objClosedPaths=new ArrayList<RoomInstanceNode>();
		objOpenPaths=new ArrayList<RoomInstanceNode>();
		bReInsertOriginAsOpen=false;
	}


	public boolean isReInsertOriginAsOpen() {
		return bReInsertOriginAsOpen;
	}


	public void setReInsertOriginAsOpen(boolean objReInsertOriginAsOpen) {
		bReInsertOriginAsOpen = objReInsertOriginAsOpen;
	}


	public boolean addClosedPath(RoomInstanceNode objE) {
		return objClosedPaths.add(objE);
	}


	public boolean addOpenPath(RoomInstanceNode objE) {
		return objOpenPaths.add(objE);
	}


	public List<RoomInstanceNode> getClosedPaths() {
		return objClosedPaths;
	}


	public List<RoomInstanceNode> getOpenPaths() {
		return objOpenPaths;
	}
	
}
