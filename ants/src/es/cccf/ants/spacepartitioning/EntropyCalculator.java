package es.cccf.ants.spacepartitioning;

public class EntropyCalculator {
	private static final double LOG_2=Math.log(2);
	
	public static double calculateEntropyValue(double iSize,double iOccupiedBlocks){
		if (iOccupiedBlocks==0 || iSize==iOccupiedBlocks){
			return 0;
		}
		double iFreeBlocks=iSize-iOccupiedBlocks;
		double pf=iOccupiedBlocks/iSize;
		double pb=iFreeBlocks/iSize;
		double result=(-pf*(Math.log(pf)/LOG_2))-(pb*(Math.log(pb)/LOG_2));
		return result;
	}
	
	public static double calculateInfoGain(double dTotalSize,double dEntropyTotal,double dSize1,double dEntropy1,double dSize2,double dEntropy2){
		double dPartialResult=dEntropy1*(dSize1/dTotalSize);
		dPartialResult+=dEntropy2*(dSize2/dTotalSize);
		double dResult=dEntropyTotal-dPartialResult;
		return dResult;
	}
}
