package es.cccf.ants.spacepartitioning;

import java.util.Iterator;
import java.util.LinkedList;

import org.aichallenge.ants.Tile;

public class Path implements Iterable<Tile>{
	int iCost;
	LinkedList<Tile> objRelevantPoints;
	boolean bComplete;
	int iHeuristic;

	public Path(RoomInstanceNode objLastInstance,boolean bComplete,int iHeuristic){
		iCost=objLastInstance.getCost();
		objRelevantPoints=new LinkedList<Tile>();
		RoomInstanceNode objCurrent=objLastInstance;
		while (objCurrent!=null){
			addRelevantPointReverse(objCurrent.getTile());
			addRelevantPointReverse(objCurrent.getTile2());
			objCurrent=objCurrent.getParent();
		}
		this.bComplete=bComplete;
		this.iHeuristic=iHeuristic;
		
	}
	
	private void addRelevantPointReverse(Tile objTile){
		if(objTile!=null){
			if (objRelevantPoints.isEmpty()||!objRelevantPoints.getFirst().equals(objTile)){
				objRelevantPoints.addFirst(objTile);
			}
		}
	}

	public int getCost() {
		return iCost+iHeuristic;
	}
	
	public Tile getNextTile(){
		return this.getNextStep(null);
	}
	
	public Tile getNextStep(Tile objTile){
		if (objTile==null){
			return objRelevantPoints.getFirst();
		}else{
			int i=objRelevantPoints.indexOf(objTile);
			if (i==-1){
				return objRelevantPoints.getFirst();
			}else if (i+1==objRelevantPoints.size()){
				return objRelevantPoints.getLast();
			}else{
				return objRelevantPoints.get(i+1);
			}
		}
	}

	
	
	@Override
	public Iterator<Tile> iterator() {
		return objRelevantPoints.iterator();
	}

	@Override
	public String toString() {
		return String.format("Path [iCost=%s, objRelevantPoints=%s]", iCost, objRelevantPoints);
	}

	public boolean isComplete() {
		return bComplete;
	}

	public Tile getFirst() {
		return objRelevantPoints.getFirst();
	}

	public Tile getLast() {
		return objRelevantPoints.getLast();
	}
	
	
	
}
