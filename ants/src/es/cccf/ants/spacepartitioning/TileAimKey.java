package es.cccf.ants.spacepartitioning;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Tile;

public class TileAimKey {

	private Tile objTile;
	private Aim aim;
	private Tile objTileDest;
	
	
	
	/**
	 * @param objTile
	 * @param objAim
	 * @param objTileDest
	 */
	public TileAimKey(Tile objTile, Aim objAim, Tile objTileDest) {
		this.objTile = objTile;
		aim = objAim;
		this.objTileDest = objTileDest;
	}
	public Tile getTile() {
		return objTile;
	}
	public Aim getAim() {
		return aim;
	}
	public Tile getTileDest() {
		return objTileDest;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aim == null) ? 0 : aim.hashCode());
		result = prime * result + ((objTile == null) ? 0 : objTile.hashCode());
		result = prime * result + ((objTileDest == null) ? 0 : objTileDest.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TileAimKey other = (TileAimKey) obj;
		if (aim != other.aim)
			return false;
		if (objTile == null) {
			if (other.objTile != null)
				return false;
		} else if (!objTile.equals(other.objTile))
			return false;
		if (objTileDest == null) {
			if (other.objTileDest != null)
				return false;
		} else if (!objTileDest.equals(other.objTileDest))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return String.format("TileAimKey [objTile=%s, aim=%s]", objTile, aim);
	}
	
	
	
}
