package es.cccf.ants;

import org.aichallenge.ants.Tile;

import es.cccf.ants.actions.Action;



public class AccionPrioridad implements Comparable<AccionPrioridad>{
	private static long NUMERO_ACCION=0;
	int prioridad;
	Tile objTileObjetivo;
	Action objAccion;
	long lNumeroAccion;
	/**
	 * @param objPrioridad
	 * @param objTileObjetivo
	 * @param objAccion
	 */
	public AccionPrioridad(int objPrioridad, Tile objTileObjetivo, Action objAccion) {
		prioridad = objPrioridad;
		this.objTileObjetivo = objTileObjetivo;
		this.objAccion = objAccion;
		this.lNumeroAccion=NUMERO_ACCION++;
	}
	public int getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(int objPrioridad) {
		prioridad = objPrioridad;
	}
	public Tile getTileObjetivo() {
		return objTileObjetivo;
	}
	public Action getAccion() {
		return objAccion;
	}
	@Override
	public int compareTo(AccionPrioridad objO) {
		if (this.getPrioridad()<objO.getPrioridad()){
			return -1;
		}else if (this.getPrioridad()>objO.getPrioridad()){
			return 1;
		} else if (this.getNumeroAccion()<objO.getNumeroAccion()){
			return -1;
		}else if (this.getNumeroAccion()>objO.getNumeroAccion()){
			return 1;
		}else {
			return 0;
		}	
	}
	public long getNumeroAccion() {
		return lNumeroAccion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (lNumeroAccion ^ (lNumeroAccion >>> 32));
		result = prime * result + prioridad;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccionPrioridad other = (AccionPrioridad) obj;
		if (lNumeroAccion != other.lNumeroAccion)
			return false;
		if (prioridad != other.prioridad)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return String.format("AccionPrioridad [prioridad=%s, objTileObjetivo=%s, objAccion=%s, lNumeroAccion=%s]", prioridad, objTileObjetivo,
				objAccion, lNumeroAccion);
	}
	
}