package es.cccf.ants;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Ilk;
import org.aichallenge.ants.ResultMove;
import org.aichallenge.ants.Tile;

import es.cccf.ants.battleplan.BattlePlanComparator;
import es.cccf.ants.battleplan.ZoneBattlePlanComparator;

public class AttackTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Ants ants=new Ants(3000, 500, 100, 100, 1000, 77, 5, 1, 42);
        ants.setTurnStartTime(System.currentTimeMillis());
        ants.clearMyAnts();
        ants.clearEnemyAnts();
//        ants.clearMyHills();
//        ants.clearEnemyHills();
//        ants.getFoodTiles().clear();
        ants.clearOrders();
        ants.clearVisibleTiles();
        
        ants.clearMap();
        ants.clearCacheBattlePlanGeneral();

		ants.update(Ilk.ENEMY_ANT, new Tile (1,2), 1);
		ants.update(Ilk.ENEMY_ANT, new Tile (1,3), 1);
		ants.update(Ilk.MY_ANT, new Tile (4,2), 0);
		ants.update(Ilk.MY_ANT, new Tile (4,3), 0);
		ants.update(Ilk.MY_ANT, new Tile (4,4), 0);
		
    	ants.updateVisibleTiles();
    	ants.updateSpacePartitioning();
    	ants.updateBattlePlanGeneral();

		long lInicio=System.currentTimeMillis();
		Tile objTileInicio=new Tile (4,3);
		Tile objTileDest=new Tile (3,3);
		
		BattlePlanComparator objComparator=new ZoneBattlePlanComparator(objTileInicio, JacobStraussBot.ZONE_SCENARIOS_COMPARATOR_DISTANCE, ants);
		ResultMove objResultMove=ants.issueMove(objTileInicio,objTileDest, objComparator);
		long lFin=System.currentTimeMillis();
		System.out.println("COMPLETADO "+(lFin-lInicio));
		
		
		
		System.out.println(objResultMove.toString());

	}

}
