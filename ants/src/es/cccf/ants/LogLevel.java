package es.cccf.ants;

public enum LogLevel {
	NONE,
	DEBUG,
	INFO,
	ERROR
}
