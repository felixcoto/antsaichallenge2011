package es.cccf.ants;


import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Bot;
import org.aichallenge.ants.ResultadoAccion;
import org.aichallenge.ants.Tile;

import es.cccf.ants.actions.AttackHill;
import es.cccf.ants.actions.SeekFoodAction;
import es.cccf.ants.actions.CombatActions;
import es.cccf.ants.actions.DefendHill;
import es.cccf.ants.actions.ExploreAction;
import es.cccf.ants.actions.RandomAction;

/**
 * Starter bot implementation.
 */
public class JacobStraussBot extends Bot {
	
	
	public static int SEEK_FOOD_NUM_ANTS_SEARCH=50;
	public static int SEEK_FOOD_INITIAL_PRIORITY=15;
	public static int SEEK_FOOD_MODIFIER=1;
	
	public static int ATTACK_HILL_NUM_ANTS_SEARCH=999;
	public static int ATTACK_HILL_INITIAL_PRIORITY=5;
	public static int ATTACK_HILL_MODIFIER=1;
	public static int ATTACK_HILL_MODIFIER_ANTS_ASSIGNED=2;
	

	public static int ATTACK_ANT_NUM_ANTS_SEARCH=30;
	public static int ATTACK_ANT_INITIAL_PRIORITY=50;
	public static int ATTACK_ANT_MODIFIER=1;
	public static int ATTACK_ANT_DISTANCE_COMBAT_MODIFIER=10;
	public static int ATTACK_ANT_MODIFIER_ANTS_ASSIGNED=2;

	public static int DEFEND_HILL_NUM_ANTS_SEARCH=20;
	public static int DEFEND_HILL_MIN_DISTANCE_ENEMY_TO_HILL=30;
	public static int DEFEND_HILL_INITIAL_PRIORITY=10;
	public static int DEFEND_HILL_MODIFIER_ENEMY_DISTANCE=1;
	public static int DEFEND_HILL_MODIFIER_MYANT_TO_ENEMY_DISTANCE=2;
	public static int DEFEND_HILL_MODIFIER_MYANT_TO_HILL_DISTANCE=2;
	public static int DEFEND_HILL_MODIFIER_ANTS_ASSIGNED=3;
	public static int DEFEND_HILL_MAX_ANTS_ASSIGNED=3;
	public static int DEFEND_HILL_COMPLETE_ASIGNEMENT=50;

	
	public static int ZONE_SCENARIOS_COMPARATOR_DISTANCE=10;
	
	public static int EXPLORE_INITIAL_PRIORITY=60;
	public static int EXPLORE_MODIFIER=1;
	public static int EXPLORE_NUM_ANTS_SEARCH=20;
	public static int EXPLORE_ASSIGNED_MODIFIER=2;
	public static int EXPLORE_NOT_UNSEEN_MODIFIER=80;
	
	
	
	public static int RANDOM_ACCION_INITIAL_PRIORITY=5000;
	public static int RANDOM_ACCION_MODIFIER=2;

	
	
	
    private static LogLevel currentLoglevel=LogLevel.NONE;
    public static Writer objTeeWriter=null;
    private static boolean bErrLog=false;
   
     
     
     
    public JacobStraussBot(){
     	try {
     		String sNombreFichero=System.getProperty("FICHEROLOG");
    		if (sNombreFichero!=null){
    			
    			objTeeWriter=new FileWriter(sNombreFichero);
    		}
    	} catch (IOException e1) {
    		//Owned
    	}
    	String loglevelString=System.getProperty("LOGLEVEL", "NONE");
    	try {
			currentLoglevel=LogLevel.valueOf(loglevelString.toUpperCase());
		} catch (Exception e) {
		}
    	bErrLog=Boolean.getBoolean("LOG_AT_SYSERR");

     }
    @Override
    public void doTurn() {
        Ants ants = getAnts();
        if (ants.getCurrentTurn()==0){
        	// es el turno de carga, aprovechamos para asegurar la carga de todas las clases
        	
        	ants.getPath(new Tile(0,0), new Tile(ants.getRows()-1,ants.getCols()-1));
        	ants.getPath(new Tile(ants.getRows()/2,ants.getCols()/2), new Tile(0,ants.getCols()-1));
        	Class<?> clazz=SeekFoodAction.class;

        	clazz=AttackHill.class;
        	long lInicio=System.currentTimeMillis();
        	long [][] arrayReserva=new long[1024][1024*20];
        	arrayReserva=null;
        	System.gc();
        	long lFin=System.currentTimeMillis();
        	log("Tiempo para asignar array"+(lFin-lInicio), null, LogLevel.INFO);
        	return;
        }
        
        if (ants.getCurrentTurn()==1){
        	System.err.println("PRUEBA");
        }
        
        int lTimeLimit=(int) (ants.getTurnTime()*0.1);
        log("Iniciando Turno "+ants.getCurrentTurn(), null, LogLevel.INFO);
        log("Tiempo pendiente:"+ants.getTimeRemaining(), null, LogLevel.INFO);
        NavigableSet<AccionPrioridad> objListaAcciones=new TreeSet<AccionPrioridad>();
        objListaAcciones.addAll(SeekFoodAction.getListaAcciones(ants));
        //objListaAcciones.addAll(ExplorarMejorado3Accion.getListaAcciones(ants));
        
        //objListaAcciones.addAll(ExploracionRooms.getListaAcciones(ants));
        //objListaAcciones.addAll(ExplorarAccion.getListaAcciones(ants));
        objListaAcciones.addAll(AttackHill.getListaAcciones(ants));
        //objListaAcciones.addAll(AtacarAnt.getListaAcciones(ants));
        //objListaAcciones.addAll(DefenderHill.getListaAcciones(ants));
        objListaAcciones.addAll(CombatActions.getListaAcciones(ants));
        objListaAcciones.addAll(RandomAction.getListaAcciones(ants));
        //objListaAcciones.addAll(ExploracionEvitarHormigasAccion.getListaAcciones(ants));
        
        //objListaAcciones.addAll(ExplorarMejorado5Accion.getListaAcciones(ants));
        objListaAcciones.addAll(ExploreAction.getListaAcciones(ants));
        
        
        
        
        long lInicio=0;
        long lFin=0;
        while (!objListaAcciones.isEmpty()&&ants.getOrders().size()<ants.getMyAnts().size()){
        	if (isLevelEnabled(LogLevel.INFO)){
        		lInicio=System.currentTimeMillis();
        	}
        	AccionPrioridad objCurrent=objListaAcciones.pollFirst();
 

        		int iSigPrioridad=Integer.MAX_VALUE;
        		if (!objListaAcciones.isEmpty()){
        			iSigPrioridad=objListaAcciones.first().getPrioridad();
        		}
	        	ResultadoAccion objResultado;
	        			if (ants.isAntIssuedOrder(objCurrent.getTileObjetivo())){
	        				objResultado=objCurrent.getAccion().antIssuedOrder(objCurrent.getTileObjetivo(), objCurrent.getPrioridad(),iSigPrioridad, ants);
	        			}else{
	        				objResultado=objCurrent.getAccion().doAccion(objCurrent.getTileObjetivo(), objCurrent.getPrioridad(),iSigPrioridad, ants);
	        			}
	        	if (!objResultado.isAcabado()){
	        		objCurrent.setPrioridad(objResultado.getNuevaPrioridad());
	        		objListaAcciones.add(objCurrent);
	       
	        	}
        	
        	if (isLevelEnabled(LogLevel.INFO)){
        		lFin=System.currentTimeMillis();
        	}
        	if (isLevelEnabled(LogLevel.INFO)){
        		long lTiempoUltimaAccion=lFin-lInicio;
        		if (lTiempoUltimaAccion>50){
        			System.err.println("TIEMPO EXCESIVO");
        			log("TIEMPO EXCESIVO:Tiempo procesamiento:"+(lFin-lInicio), objCurrent.getTileObjetivo(), LogLevel.INFO);
        		}
        		
        	}

        	if (ants.getTimeRemaining()<=lTimeLimit){
        		if (isLevelEnabled(LogLevel.INFO)){
        			long lTiempoUltimaAccion=lFin-lInicio;
        			log("Saliendo por Timeout:"+ants.getTimeRemaining()+" Tam. lista acciones: "+objListaAcciones.size(), null, LogLevel.INFO);
        			log("Tiempo proceso ultima accion:"+lTiempoUltimaAccion, null, LogLevel.INFO);
        			if (lTiempoUltimaAccion>lTimeLimit){
        				log("TIEMPO EXCEDIDO", null, LogLevel.INFO);
        			}
        		}
        		return;
        	}
        	
        }
		if (isLevelEnabled(LogLevel.INFO)){
			log("Saliendo normal:"+ants.getTimeRemaining(), null, LogLevel.INFO);
			try {
				if (objTeeWriter!=null){
					objTeeWriter.flush();
				}
			} catch (IOException e) {
			}
		}
        
    }
    
    

	

	
    
    public static boolean isLevelEnabled(LogLevel loglevel){
    	return loglevel.ordinal()>=currentLoglevel.ordinal();
    }
    
	
	public static void log(String sMensaje,Tile objTile,LogLevel loglevel){
		if (loglevel.ordinal()>=currentLoglevel.ordinal()){
			String sMensajeFormateado=formatLog(sMensaje, objTile, loglevel);
			if (bErrLog){
				System.err.println(sMensajeFormateado);
			}
			if (objTeeWriter!=null){
				try {
					objTeeWriter.write(sMensajeFormateado);
					objTeeWriter.write('\n');
				} catch (IOException e) {
				}

			}
		}
	}
	private static String formatLog(String sMensaje,Tile objTile,LogLevel level){
		return String.format("[%1$tH:%1$tM:%1$tS.%1$tL][%2$5s][%3$8s]:%4$s" , new Date(),level,objTile!=null?objTile.toString():"NO TILE",sMensaje);
	}
	
}
