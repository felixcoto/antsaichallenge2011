import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.aichallenge.ants.Aim;
import org.aichallenge.ants.Ants;
import org.aichallenge.ants.Bot;
import org.aichallenge.ants.Tile;

import es.cccf.ants.battleplan.BattlePlan;
import es.cccf.ants.battleplan.TacticValoration;
import es.cccf.ants.battleplan.TileThreatInfo;
import es.cccf.ants.spacepartitioning.TileAimKey;


public class MyBotTest extends Bot {

    public static void main(String[] args) throws IOException {
    	
        new MyBotTest().readSystemInput();
    }

	
	@Override
	public void doTurn() {
		Ants ants=getAnts();
		if (ants.getCurrentTurn()>0){
			TileAimKey objTileAimKey=ants.getTileAimKey(new Tile(70,52), Aim.NORTH);
			long lInicio=System.currentTimeMillis();
			Set<TileAimKey> objSet=new HashSet<TileAimKey>();
			objSet.add(objTileAimKey);
			objSet.add(ants.getTileAimKey(new Tile(71,51), Aim.NORTH));
			objSet.add(ants.getTileAimKey(new Tile(71,52), Aim.NORTH));
			objSet.add(ants.getTileAimKey(new Tile(71,53), Aim.NORTH));
			objSet.add(ants.getTileAimKey(new Tile(70,53), Aim.NORTH));
			BattlePlan objBattlePlan=new BattlePlan(objSet,ants.getGeneral());
			objBattlePlan.calculateEnemyScenarios6();
			Tile[] objTestTiles=new Tile[]{new Tile(70,52),new Tile(69,52),new Tile(70,53),new Tile(69,53),new Tile(71,51), new Tile(70,51), new Tile(71,52),new Tile(71,53)};
			
			for (Tile objTile : objTestTiles) {
				TileThreatInfo objInfo=ants.getGeneral().getThreatInfo(objTile);
				if (objInfo!=null){
					TacticValoration objValoration=objInfo.getTacticValorationForPlayer();
					System.err.println(String.format("Tile %s:%s", objTile,objValoration));
				}
			}
			
			
//			BattlePlanComparator objComparator=new DeadSurviveRatioBattlePlanComparator(0.5);
//			List<BattlePlan> objResult=new ArrayList<BattlePlan>(ants.getGeneral().computeBattlePlanList(objTileAimKey, objComparator));
			long lFin=System.currentTimeMillis();
			System.err.println("TIEMPO:"+(lFin-lInicio));
//			Collections.sort(objResult,objComparator);
//			if (!objResult.isEmpty()){
//				
//				for (BattlePlan objBattlePlan : objResult) {
//					System.err.println(String.format("MinDeadRatio %5.2f, MinSurviveRatio %5.2f",objBattlePlan.getMinDeadRatio(),objBattlePlan.getMinSurviveRatio()));
//				}
//			}
		}
	}

}
